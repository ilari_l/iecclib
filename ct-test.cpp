#include <cstdlib>
#include "valgrind/memcheck.h"
#include "iecclib.h"

void test_field(const ecclib_field* f)
{
	uint8_t s1[f->element_size];
	uint8_t s2[f->element_size];
	uint8_t s3[f->element_size];
	uint8_t s4[f->storage_octets];
	uint8_t s5[f->storage_octets];
	uint8_t s6[f->element_size];
	uint8_t s7[f->element_size];
	uint8_t s8[f->element_size];
	uint32_t flag = s8[0];
	f->zero(s1);
	f->set_int(s2, 31337);
	f->set(s3, s4);
	f->get(s3, s5);
	f->add(s6, s7, s8);
	f->sub(s6, s7, s8);
	f->mul(s6, s7, s8);
	f->smul(s6, s7, 31337);
	f->neg(s6, s7);
	f->inv(s6, s7);
	f->sqr(s6, s7);
	f->sqrt(s6, s7);
	f->load_cond(s6, s7, flag);
	f->select(s6, s7, s8, flag);
	f->cswap(s6, s7, flag);
}

void test_curve1(const ecclib_curve* c)
{
	uint8_t s1[c->element_size];
	uint8_t s2[c->element_size];
	uint8_t s3[c->element_size];
	uint32_t s4[c->scalar_elems_single];
	uint32_t s5[c->scalar_elems_double];
	uint8_t s6[c->scalar_octets];
	uint8_t s7[c->field->element_size];
	uint8_t s8[c->field->element_size];
	uint8_t s9[c->field->storage_octets];
	c->point_add(s1, s2, s3);
	c->point_montgomery(s1, s7);
	c->reduce_partial(s4, s5);
	c->reduce_full(s6, s4);
	c->montgomery_mul(s8, s4, s7);
	c->point_mul_base(s1, s4);
	c->point_mul(s1, s4, s2);
	c->point_double(s1, s2);
	c->point_neg(s1, s2);
	c->point_copy(s1, s2);
	c->point_get_coords(s1, s7, s8);
	c->field->check(s9);
	ecclib_bigint_load(s4, c->scalar_elems_single, s6, c->scalar_octets);
}

void test_curve(const ecclib_curve* c)
{
	uint8_t s5[c->element_size];
	uint8_t s5x[c->storage_octets];
	uint8_t s5y[c->storage_octets];
	uint8_t s7[c->field->element_size];
	uint8_t s8[c->field->element_size];
	uint8_t s9[c->field->element_size];
	uint8_t one[c->field->element_size];
	c->field->set_int(one, 1);
	c->point_zero(s5);
	c->point_get(s5, s5x);
	VALGRIND_MAKE_MEM_UNDEFINED(s5x, sizeof(s5x));
	c->point_set(s5, s5x);
	c->point_base(s5);
	c->point_get_coords(s5, s7, s8);
	VALGRIND_MAKE_MEM_UNDEFINED(s7, sizeof(s7));
	VALGRIND_MAKE_MEM_UNDEFINED(s8, sizeof(s8));
	c->point_set_coords(s5, s7, s8);
	c->field->add(s9, s7, one);
	c->point_set_coords(s5, s9, s8);
	for(unsigned i = 0; i < 256; i++) {
		s5y[0] = i;
		c->point_set(s5, s5y);
	}
}

void test_misc()
{
	ecclib_set_thread_fns(NULL, NULL, NULL);
	uint8_t s5[65];
	uint32_t s4[16];
	ecclib_bigint_load(s4, 16, s5, 65);
	ecclib_zeroize(s5, sizeof(s5));
	uint8_t syndrome = 0;
	for(unsigned i = 0; i < sizeof(s5); i++) syndrome |= s5[i];
	if(syndrome != 0) {
		abort();
	}
}

int main()
{
	test_misc();
	const char* const * fields = ecclib_field_list();
	for(size_t i = 0; fields[i]; i++) {
		const struct ecclib_field* fld = ecclib_lookup_field_by_name(fields[i]);
		if(!fld) continue;
		test_field(fld);
	}
	ecclib_lookup_field_by_name("DOES_NOT_EXIST");

	const char* const * curves = ecclib_curve_list();
	for(size_t i = 0; curves[i]; i++) {
		const struct ecclib_curve* crv = ecclib_lookup_curve_by_name(curves[i]);
		if(!crv) continue;
		test_curve1(crv);
		test_curve(crv);
	}
	ecclib_lookup_curve_by_name("DOES_NOT_EXIST");
}