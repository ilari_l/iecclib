use std::process::Command;
use std::env;

fn compile(module: &str)
{
	let target = env::var("TARGET").unwrap();
	let is_64_bit = target.starts_with("x86_64-");
	let is_windows = target.contains("-pc-windows");
	let mut cmd = Command::new("gcc");
	cmd.args(&["-g", "-Werror", "-Wall", "-O3", "-std=c99", "-I.", "-c"]);
	if !is_windows {
		cmd.arg("-fPIC");
	}
	if is_64_bit {
		cmd.arg("-DUSE_64BIT_MATH");
	}
	cmd.arg("-o");
	cmd.arg(format!("{}.o", module));
	cmd.arg(format!("{}.c", module));
	println!("Compiling {}...", module);
	match cmd.status() {
		Ok(x) => match x.success() {
			true => (),
			false => panic!("GCC failed")
		},
		Err(_) => panic!("Failed to run GCC")
	};
}

fn link(modules: &[&str])
{
	let out_dir = env::var("OUT_DIR").unwrap();
	let mut cmd = Command::new("ld");
	cmd.args(&["-r", "-o", "iecclib-rust-1.o"]);
	for i in modules.iter() {
		cmd.arg(format!("{}.o", i));
	}
	println!("Partial linking...");
	match cmd.status() {
		Ok(x) => match x.success() {
			true => (),
			false => panic!("LD failed")
		},
		Err(_) => panic!("Failed to run LD")
	};
	let mut cmd = Command::new("ar");
	cmd.arg("cvrs");
	cmd.arg(format!("{}/libiecclib.a", out_dir));
	cmd.arg("iecclib-rust-1.o");
	println!("Archiving...");
	match cmd.status() {
		Ok(x) => match x.success() {
			true => (),
			false => panic!("AR failed")
		},
		Err(_) => panic!("Failed to run AR")
	};
}

fn main()
{
	let out_dir = env::var("OUT_DIR").unwrap();
	let filebase = [
		"curves/25519", "curves/448", "curves/3363", "curves/38921", "curves/41417", "curves/e521",
		"fields/25519", "fields/448", "fields/3363", "fields/38921", "fields/41417", "fields/5211",
		"fields/25519-64", "fields/448-64", "fields/3363-64", "fields/38921-64", "fields/41417-64",
		"fields/5211-64", "bigint", "curve", "field", "ecclib", "utils"
	];
	for i in filebase.iter() { compile(i) }
	link(&filebase);
	println!("cargo:rustc-link-search=native={}", out_dir);
	println!("cargo:rustc-link-lib=static=iecclib");
}
