#ifndef _iecclib_h_included_
#define _iecclib_h_included_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

/**
 * Add two bigints without carry.
 *
 * \param out The output bigint, base-2^32-little-endian, words elements.
 * \param in1 The first input bigint, base-2^32-little-endian, words elements.
 * \param in2 The second input bigint, base-2^32-little-endian, words elements.
 * \param words Number of elements in other bigints.
 */
void ecclib_bigint_add(uint32_t* out, const uint32_t* in1, const uint32_t* in2, size_t words);

/**
 * Substract two bigints without borrow.
 *
 * \param out The output bigint, base-2^32-little-endian, words elements.
 * \param in1 The first input bigint, base-2^32-little-endian, words elements.
 * \param in2 The second input bigint, base-2^32-little-endian, words elements.
 * \param words Number of elements in other bigints.
 */
void ecclib_bigint_sub(uint32_t* out, const uint32_t* in1, const uint32_t* in2, size_t words);

/**
 * Multiply two bigints.
 *
 * \param out The output bigint, base-2^32-little-endian, words1+words2 elements.
 * \param in1 The first input bigint, base-2^32-little-endian, words elements.
 * \param words1 Number of elements in in1.
 * \param in2 The second input bigint, base-2^32-little-endian, words elements.
 * \param words2 Number of elements in in2.
 * \note There is a more optimized special case when words2 = 1.
 */
void ecclib_bigint_mul(uint32_t* out, const uint32_t* in1, size_t words1, const uint32_t* in2, size_t words2);

/**
 * Create a bigint from bytes.
 *
 * \param out The output bigint base-2^32-little-endian, outwords elements.
 * \param outwords Number of words in output.
 * \param in The input bigint base-256-little-endian, inoctets elements.
 * \param inoctets Number of octets in input. Must be at most 4 * outwords.
 */
void ecclib_bigint_load(uint32_t* out, size_t outwords, const uint8_t* in, size_t inoctets);

/**
 * Store a bigint as bytes.
 *
 * \param out The output buffer to write bigint base-256-little-endian, outoctets elements.
 * \param outoctets Number of output octets.
 * \param in The input bigint base-2^32-little-endian. At least ceil(outoctets / 4) elements.
 */
void ecclib_bigint_store(uint8_t* out, size_t outoctets, const uint32_t* in);

/**
 * A field.
 */
struct ecclib_field
{
/**
 * Name of the field.
 */
	const char* name;
/**
 * The number of bytes field element takes in memory.
 */
	size_t element_size;
/**
 * Number of bits in field.
 */
	size_t field_bits;
/**
 * The number of octets storing field element takes.
 */
	size_t storage_octets;
/**
 * The size of the field (always prime power). Little-endian base-256, storage_octets octets.
 *
 * \note This value wraps around if field size is 256^storage_octets, but because such field must be a binary field
 *	with power multiple of 8 and thus not prime, those are not encountered in practice.
 */
	const uint8_t* field_element_count;
/**
 * The field characteristic (always prime). Little-endian base-256, storage_octets octets.
 */
	const uint8_t* field_characteristic;
/**
 * The field degree.
 */
	size_t degree;
/**
 * Stride between degrees.
 */
	size_t degree_stride;
/**
 * Bits in each degree.
 */
	size_t degree_bits;
/**
 * Initialize field element to zero.
 *
 * \param out The element.
 */
	void (*zero)(void* out);
/**
 * Initialize field element to a small non-negative value.
 *
 * \param out The element.
 * \param val The value to initialize to.
 * \note val can be maximum of 2^24-1.
 */
	void (*set_int)(void* out, uint32_t val);
/**
 * Initialize field element to given value.
 *
 * \param out The element.
 * \param val The value to initialize to. Given as base-256 little-endian integer. storage_octets elements.
 */
	void (*set)(void* out, const uint8_t* val);
/**
 * Store field element.
 *
 * \param in The element.
 * \param val The value to write. See parameter val of method set for format.
 */
	void (*get)(const void* in, uint8_t* val);
/**
 * Check if storage representation is smaller than modulus.
 *
 * \param val The value to check.
 * \returns 1 if smaller, 0 if not.
 */
	int (*check)(const uint8_t* val);
/**
 * Add two field elements.
 *
 * \param out The output value.
 * \param in1 The first input value.
 * \param in2 The second input value.
 */
	void (*add)(void* out, const void* in1, const void* in2);
/**
 * Substract two field elements.
 *
 * \param out The output value.
 * \param in1 The first input value.
 * \param in2 The second input value.
 */
	void (*sub)(void* out, const void* in1, const void* in2);
/**
 * Multiply two field elements.
 *
 * \param out The output value.
 * \param in1 The first input value.
 * \param in2 The second input value.
 */
	void (*mul)(void* out, const void* in1, const void* in2);
/**
 * Multiply field element by a small constant.
 *
 * \param out The output value.
 * \param in1 The first input value.
 * \param in2 The second input value.
 * \note in2 can be maximum of 2^24-1.
 */
	void (*smul)(void* out, const void* in1, uint32_t in2);
/**
 * Negate field element.
 *
 * \param out The output value.
 * \param in The input value.
 */
	void (*neg)(void* out, const void* in);
/**
 * Square field element.
 *
 * \param out The output value.
 * \param in The input value.
 */
	void (*sqr)(void* out, const void* in);
/**
 * Invert field element.
 *
 * \param out The output value.
 * \param in The input value.
 * \note Inverse of 0 is 0.
 */
	void (*inv)(void* out, const void* in);
/**
 * Square root field element.
 *
 * \param out The output value.
 * \param in The input value.
 * \returns 1 if square root exists, 0 if not.
 */
	int (*sqrt)(void* out, const void* in);
/**
 * Conditionally load field element.
 *
 * \param out The output value.
 * \param in The input value.
 * \param flag If 1, set output to input. Otherwise do nothing.
 * \note Constant time.
 */
	void (*load_cond)(void* out, const void* in, uint32_t flag);
/**
 * Select field element.
 *
 * \param out The output value.
 * \param in1 The input value to use if flag=1.
 * \param in0 The input value to use if flag=0.
 * \param flag Sets if in1 or in0 is loaded.
 * \note Constant time.
 */
	void (*select)(void* out, const void* in1, const void* in2, uint32_t flag);
/**
 * Conditionally swap field elements.
 *
 * \param x1 The first value.
 * \param x2 The second value.
 * \param flag If 1, swap values. Otherwise do nothing.
 * \note Constant time.
 */
	void (*cswap)(void* x1, void* x2, uint32_t flag);
/**
 * Copy field element.
 *
 * \param out The output value.
 * \param in The input value.
 */
	void (*copy)(void* out, const void* in);
};

/**
 * Look up supported field by name.
 *
 * \param name The name of the field, NUL-terminated.
 * \returns The field handle, or NULL if no such field is supported.
 */
const struct ecclib_field* ecclib_lookup_field_by_name(const char* name);

/**
 * List known fields.
 *
 * \returns The NULL terminated fields list.
 */
const char** ecclib_field_list();

/**
 * Return field->name
 */
const char* ecclib_field_name(const struct ecclib_field* field);
/**
 * Return field->element_size
 */
size_t ecclib_field_element_size(const struct ecclib_field* field);
/**
 * Return field->field_bits
 */
size_t ecclib_field_field_bits(const struct ecclib_field* field);
/**
 * Return field->storage_octets
 */
size_t ecclib_field_storage_octets(const struct ecclib_field* field);
/**
 * Return field->field_element_count
 */
const uint8_t* ecclib_field_field_element_count(const struct ecclib_field* field);
/**
 * Return field->field_characteristic
 */
const uint8_t* ecclib_field_field_characteristic(const struct ecclib_field* field);
/**
 * Return field->degree
 */
size_t ecclib_field_degree(const struct ecclib_field* field);
/**
 * Return field->degree_stride
 */
size_t ecclib_field_degree_stride(const struct ecclib_field* field);
/**
 * Return field->degree_bits
 */
size_t ecclib_field_degree_bits(const struct ecclib_field* field);
/**
 * Call field->zero
 */
void ecclib_field_zero(const struct ecclib_field* field, void* out);
/**
 * Call field->set_int
 */
void ecclib_field_set_int(const struct ecclib_field* field, void* out, uint32_t val);
/**
 * Call field->set
 */
void ecclib_field_set(const struct ecclib_field* field, void* out, const uint8_t* val);
/**
 * Call field->get
 */
void ecclib_field_get(const struct ecclib_field* field, const void* in, uint8_t* val);
/**
 * Call field->check
 */
int ecclib_field_check(const struct ecclib_field* field, const uint8_t* val);
/**
 * Call field->add
 */
void ecclib_field_add(const struct ecclib_field* field, void* out, const void* in1, const void* in2);
/**
 * Call field->sub
 */
void ecclib_field_sub(const struct ecclib_field* field, void* out, const void* in1, const void* in2);
/**
 * Call field->mul
 */
void ecclib_field_mul(const struct ecclib_field* field, void* out, const void* in1, const void* in2);
/**
 * Call field->smul
 */
void ecclib_field_smul(const struct ecclib_field* field, void* out, const void* in1, uint32_t in2);
/**
 * Call field->neg
 */
void ecclib_field_neg(const struct ecclib_field* field, void* out, const void* in);
/**
 * Call field->sqr
 */
void ecclib_field_sqr(const struct ecclib_field* field, void* out, const void* in);
/**
 * Call field->inv
 */
void ecclib_field_inv(const struct ecclib_field* field, void* out, const void* in);
/**
 * Call field->sqrt
 */
int ecclib_field_sqrt(const struct ecclib_field* field, void* out, const void* in);
/**
 * Call field->copy
 */
void ecclib_field_copy(const struct ecclib_field* field, void* out, const void* in);
/**
 * Call field->load_cond
 */
void ecclib_field_load_cond(const struct ecclib_field* field, void* out, const void* in, uint32_t flag);
/**
 * Call field->select
 */
void ecclib_field_select(const struct ecclib_field* field, void* out, const void* in1, const void* in2,
	uint32_t flag);
/**
 * Call field->cswap
 */
void ecclib_field_cswap(const struct ecclib_field* field, void* x1, void* x2, uint32_t flag);

/**
 * Flag: Curve has montgomery representation.
 */
#define ECCLIB_FLAG_HAS_MONTGOMERY 1

/**
 * Flag: Montgomery representation is slow.
 */
#define ECCLIB_FLAG_MONTGOMERY_SLOW 2

/**
 * An elliptic curve.
 */
struct ecclib_curve
{
/**
 * Name of the curve, NUL terminated string.
 */
	const char* name;
/**
 * The base field curve uses.
 */
	const struct ecclib_field* field;
/**
 * The number of octets storing elliptic curve point takes.
 */
	size_t storage_octets;
/**
 * The number of bytes field element takes in memory.
 */
	size_t element_size;
/**
 * Order of the group, as storage_octets octets.
 */
	const uint8_t* order;
/**
 * Ceiling of log2 of l.
 */
	size_t clog2l;
/**
 * Rounding of log2 of l
 */
	size_t rlog2l;
/**
 * Maximum number of bits partial reduction can handle.
 */
	size_t partial_max_bits;
/**
 * The size of single scalar in uint32_t elements.
 */
	size_t scalar_elems_single;
/**
 * The size of double scalar in uint32_t elements.
 */
	size_t scalar_elems_double;
/**
 * The size of storage represnetation of scalar in octets.
 */
	size_t scalar_octets;
/**
 * Cofactor of the curve.
 */
	uint32_t cofactor;
/**
 * Flags.
 */
	uint32_t flags;
/**
 * Standard montgomery basepoint. Field storage representation, only present if montgomery is supported.
 */
	const uint8_t* montgomery_base;
/**
 * Partially reduce number less than 4B^2 for some curve-specific constant B modulo order of the curve into number
 * less than B.
 *
 * \param output Output space. Base-2^32-little-endian, single scalar elements.
 * \param input Input space. Base-2^32-little-endian, double scalar elements.
 */
	void (*reduce_partial)(uint32_t* output, const uint32_t* input);
/**
 * Fully reduce number less than 2B for some curve-specific constant B modulo order of the curve.
 *
 * \param output Output space. Base-256-little-endian, scalar_octets octets.
 * \param input Input space. Base-2^32-little-endian, single scalar elements.
 */
	void (*reduce_full)(uint8_t* output, const uint32_t* input);
/**
 * Initialize point to neutral.
 *
 * \param point The point to initialize.
 */
	void (*point_zero)(void* point);
/**
 * Initialize point to standard basepoint.
 *
 * \param point The point to initialize.
 */
	void (*point_base)(void* point);
/**
 * Initialize a point from export representation.
 *
 * \param point The point to initialize
 * \param data The export presentation to initialize from, storage_octets octets.
 * \returns 0 on success, -1 if presentation is invalid.
 */
	int (*point_set)(void* point, const uint8_t* data);
/**
 * Export representation of point.
 *
 * \param output The export presentation to write, storage_octets octets.
 * \param point The point to serialize
 */
	void (*point_get)(const void* point, uint8_t* output);
/**
 * Add two points on the curve.
 *
 * \param out The output buffer.
 * \param in1 The first input.
 * \param in2 The second input.
 */
	void (*point_add)(void* out, const void* in1, const void* in2);
/**
 * Double a point on curve.
 *
 * \param out The output buffer.
 * \param in1 The input.
 * \note This is the same as adding point to itself, just faster.
 */
	void (*point_double)(void* out, const void* in);
/**
 * Scalar multiply standard basepoint.
 *
 * \param out The output buffer.
 * \param mul The multiplier. Base-2^32-little-endian, scalar_single_elements elements, less than B.
 */
	void (*point_mul_base)(void* out, const uint32_t* mul);
/**
 * Scalar multiply.
 *
 * \param out The output buffer.
 * \param mul The multiplier. Base-2^32-little-endian, scalar_single_elements elements, less than B.
 * \param in The input point
 */
	void (*point_mul)(void* out, const uint32_t* mul, const void* in);
/**
 * Negate a point.
 *
 * \param out The output buffer.
 * \param in The point to negate.
 */
	void (*point_neg)(void* out, const void* in);
/**
 * Copy a point.
 *
 * \param out The output buffer.
 * \param in The input buffer.
 */
	void (*point_copy)(void* out, const void* in);
/**
 * Get coodinates of point.
 *
 * \param point The point.
 * \param x The x coodinate (a field element of base field).
 * \param y The y coodinate (a field element of base field).
 */
	void (*point_get_coords)(const void* point, void* x, void* y);
/**
 * Set coordinates of point.
 *
 * \param point The point.
 * \param x The x coodinate (a field element of base field).
 * \param y The y coodinate (a field element of base field).
 * \returns 1 on success, 0 on failure (invalid coordinates).
 */
	int (*point_set_coords)(void* point, const void* x, const void* y);
/**
 * Export montgomery x representation of point.
 *
 * \param output The buffer to write the representation to (as field element).
 * \param point The point to export.
 * \returns 1 on success, 0 on failure (curve does not have montgomery representation).
 */
	int (*point_montgomery)(void* output, const void* point);
/**
 * Do scalar multiplication with montgomery coordinates.
 *
 * \param out The buffer to write the result to (as field element).
 * \param mul The multiplier.Base-2^32-little-endian, single scalar elements, at most B.
 * \param base The base point (as field element).
 * \returns 1 on success, 0 on failure (curve does not have montgomery representation).
 */
	int (*montgomery_mul)(void* output, const uint32_t* mul, const void* base);
};

/**
 * Look up supported curve by name.
 *
 * \param name The name of the curve, NUL-terminated.
 * \returns The ellptic curve handle, or NULL if no such curve is supported.
 */
const struct ecclib_curve* ecclib_lookup_curve_by_name(const char* name);

/**
 * List known curves.
 *
 * \returns The NULL terminated curves list.
 */
const char** ecclib_curve_list();

/**
 * Return curve->name
 */
const char* ecclib_curve_name(const struct ecclib_curve* curve);
/**
 * Return curve->field
 */
const struct ecclib_field* ecclib_curve_field(const struct ecclib_curve* curve);
/**
 * Return curve->storage_octets
 */
size_t ecclib_curve_storage_octets(const struct ecclib_curve* curve);
/**
 * Return curve->element_size
 */
size_t ecclib_curve_element_size(const struct ecclib_curve* curve);
/**
 * Return curve->order
 */
const uint8_t* ecclib_curve_order(const struct ecclib_curve* curve);
/**
 * Return curve->clog2l
 */
size_t ecclib_curve_clog2l(const struct ecclib_curve* curve);
/**
 * Return curve->rlog2l
 */
size_t ecclib_curve_rlog2l(const struct ecclib_curve* curve);
/**
 * Return curve->partial_max_bits
 */
size_t ecclib_curve_partial_max_bits(const struct ecclib_curve* curve);
/**
 * Return curve->scalar_elems_single
 */
size_t ecclib_curve_scalar_elems_single(const struct ecclib_curve* curve);
/**
 * Return curve->scalar_elems_double
 */
size_t ecclib_curve_scalar_elems_double(const struct ecclib_curve* curve);
/**
 * Return curve->scalar_octets
 */
size_t ecclib_curve_scalar_octets(const struct ecclib_curve* curve);
/**
 * Return curve->cofactor
 */
uint32_t ecclib_curve_cofactor(const struct ecclib_curve* curve);
/**
 * Return curve->flags
 */
uint32_t ecclib_curve_flags(const struct ecclib_curve* curve);
/**
 * Return curve->montgomery_base
 */
const uint8_t* ecclib_curve_montgomery_base(const struct ecclib_curve* curve);
/**
 * Call curve->reduce_partial
 */
void ecclib_curve_reduce_partial(const struct ecclib_curve* curve, uint32_t* output, const uint32_t* input);
/**
 * Call curve->reduce_full
 */
void ecclib_curve_reduce_full(const struct ecclib_curve* curve, uint8_t* output, const uint32_t* input);
/**
 * Call curve->point_zero
 */
void ecclib_curve_point_zero(const struct ecclib_curve* curve, void* point);
/**
 * Call curve->point_base
 */
void ecclib_curve_point_base(const struct ecclib_curve* curve, void* point);
/**
 * Call curve->point_set
 */
int ecclib_curve_point_set(const struct ecclib_curve* curve, void* point, const uint8_t* data);
/**
 * Call curve->point_get
 */
void ecclib_curve_point_get(const struct ecclib_curve* curve, const void* point, uint8_t* output);
/**
 * Call curve->point_add
 */
void ecclib_curve_point_add(const struct ecclib_curve* curve, void* out, const void* in1, const void* in2);
/**
 * Call curve->point_double
 */
void ecclib_curve_point_double(const struct ecclib_curve* curve, void* out, const void* in);
/**
 * Call curve->point_mul_base
 */
void ecclib_curve_point_mul_base(const struct ecclib_curve* curve, void* out, const uint32_t* mul);
/**
 * Call curve->point_mul
 */
void ecclib_curve_point_mul(const struct ecclib_curve* curve, void* out, const uint32_t* mul, const void* in);
/**
 * Call curve->point_neg
 */
void ecclib_curve_point_neg(const struct ecclib_curve* curve, void* out, const void* in);
/**
 * Call curve->point_copy
 */
void ecclib_curve_point_copy(const struct ecclib_curve* curve, void* out, const void* in);
/**
 * Call curve->point_get_coords
 */
void ecclib_curve_point_get_coords(const struct ecclib_curve* curve, const void* point, void* x, void* y);
/**
 * Call curve->point_set_coords
 */
int ecclib_curve_point_set_coords(const struct ecclib_curve* curve, void* point, const void* x, const void* y);
/**
 * Call curve->point_montgomery
 */
int ecclib_curve_point_montgomery(const struct ecclib_curve* curve, void* output, const void* point);
/**
 * Call curve->montgomery_mul
 */
int ecclib_curve_montgomery_mul(const struct ecclib_curve* curve, void* output, const uint32_t* mul,
	const void* base);

/**
 * Compute Xcurve function with standard base point
 *
 * \param curve The base curve to use.
 * \param pubkey The public key output buffer (curve->field->storage_octets octets).
 * \param secret The secret key buffer (curve->field->storage_octets octets).
 * \return 1 on success, 0 if curve does not have montgomery form.
 *
 * \note If curve is "curve25519", this is X25519.
 * \note If curve is "curve448", this is X448.
 */
int ecclib_xcurve_base(const struct ecclib_curve* curve, uint8_t* pubkey, const uint8_t* secret);

/**
 * Compute Xcurve function
 *
 * \param curve The base curve to use.
 * \param shared The shared secret output buffer (curve->field->storage_octets octets).
 * \param secret The secret key buffer (curve->field->storage_octets octets).
 * \param ppubkey The peer public key buffer (curve->field->storage_octets octets).
 * \return 1 on success, 0 if curve does not have montgomery form.
 */
int ecclib_xcurve(const struct ecclib_curve* curve, uint8_t* shared, const uint8_t* secret, const uint8_t* ppubkey);




/**
 * Set threading lock functions.
 *
 * \param lock The locking function.
 * \param unlock The unlocking function.
 * \param ctx The context to pass to lock/unlock.
 * \note Default functions do nothing.
 * \note Not thread safe versus anything else.
 */
void ecclib_set_thread_fns(void(*lock)(void* ctx), void(*unlock)(void* ctx), void* ctx);

/**
 * Reliably zeroize block of memory.
 *
 * \param buf The buffer to zeroize.
 * \param buflen Number of bytes in buffer.
 */
void ecclib_zeroize(uint8_t* buf, size_t buflen);

#ifdef __cplusplus
}
#endif

#endif
