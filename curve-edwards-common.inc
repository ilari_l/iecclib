#define FLAGS ECCLIB_FLAG_HAS_MONTGOMERY

static int _point_set(point_t* point, const uint8_t* data);
static void _point_get_coords(const point_t* point, field_t* x, field_t* y);
static int _point_set_coords(point_t* point, const field_t* x, const field_t* y);
static inline int _point_montgomery(field_t* output, const point_t* point);
static inline void _point_neg(point_t* out, const point_t* in);
static inline void _point_double(point_t* out, const point_t* in);
static inline void _point_add(point_t* out, const point_t* in1, const point_t* in2);
static inline void _point_base(point_t* point);
static inline void _point_zero(point_t* point);
static inline void _point_load_cond(point_t* out, const point_t* in, uint32_t flag);

static void _point_get(const point_t* point, uint8_t* output)
{
	uint8_t tmp[FIELD_STORE_SIZE];
	field_t x, y, iz;
	FIELD_INV(&iz, &point->z);
	FIELD_MUL(&x, &point->x, &iz);
	FIELD_MUL(&y, &point->y, &iz);
	FIELD_STORE(tmp, &x);
	FIELD_STORE(output, &y);
	//Clear any extra octets.
	for(size_t i = FIELD_STORE_SIZE; i < POINT_STORE_SIZE; i++) output[i] = 0;
	output[POINT_STORE_SIZE - 1] |= tmp[0] << 7;	//Sign of X.
}

static void _point_get_coords(const point_t* point, field_t* x, field_t* y)
{
	field_t iz;
	FIELD_INV(&iz, &point->z);
	FIELD_MUL(x, &point->x, &iz);
	FIELD_MUL(y, &point->y, &iz);
}

static int _point_set_coords(point_t* point, const field_t* x, const field_t* y)
{
	do_global_init();
	uint8_t _check[FIELD_STORE_SIZE];
	field_t A, B, C, D;
	FIELD_SQR(&A, x);			//y^2
	FIELD_SQR(&B, y);			//y^2
	//FIXME: This isn't entierely correct (assumes that all curves having T are twisted.
#ifdef HAS_COORDINATE_T
	FIELD_SUB(&C, &B, &A);			//y^2 - x^2
#else
	FIELD_ADD(&C, &B, &A);			//y^2 + x^2
#endif
	FIELD_MUL(&D, &A, &B);			//x^2*y^2
#ifdef EDWARDS_CONST
#if EDWARDS_CONST < 0
	FIELD_SMUL(&A, &D, -EDWARDS_CONST);	//-d*x^2*y^2
	FIELD_SUB(&B, &const_one, &A);		//1+d*x^2*y^2
#else
	FIELD_SMUL(&A, &D, EDWARDS_CONST);	//d*x^2*y^2
	FIELD_ADD(&B, &const_one, &A);		//1+d*x^2*y^2
#endif
#else
	FIELD_MUL(&A, &D, &const_d);		//d*x^2*y^2
	FIELD_ADD(&B, &const_one, &A);		//1+d*x^2*y^2
#endif
	FIELD_SUB(&A, &C, &B);			//(y^2 +- x^2)-(1+d*x^2*y^2)
	FIELD_STORE(_check, &A);
	uint8_t syndrome = 0;
	for(size_t i = 0; i < FIELD_STORE_SIZE; i++)
		syndrome |= _check[i];
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	syndrome ^= 1;
	FIELD_LOAD_COND(&point->x, x, syndrome);
	FIELD_LOAD_COND(&point->y, y, syndrome);
	FIELD_LOAD_COND(&point->z, &const_one, syndrome);
#ifdef HAS_COORDINATE_T
	FIELD_MUL(&A, x, y);
	FIELD_LOAD_COND(&point->t, &A, syndrome);
#endif
	return syndrome;
}

static void _montgomery(field_t* dblx, field_t* dblz, field_t* sumx, field_t* sumz,
	const field_t* ax, const field_t* az, const field_t* bx, const field_t* bz,
	const field_t* diff)
{
	field_t A, B, C, D, E, F, G, H;
	FIELD_ADD(&A, ax, az);
	FIELD_SQR(&F, &A);
	FIELD_SUB(&B, ax, az);
	FIELD_SQR(&G, &B);
	FIELD_SUB(&E, &F, &G);
	FIELD_ADD(&C, bx, bz);
	FIELD_SUB(&D, bx, bz);
	FIELD_MUL(&H, &D, &A);
	FIELD_MUL(&A, &C, &B);
	FIELD_ADD(&B, &H, &A);
	FIELD_SQR(sumx, &B);
	FIELD_SUB(&B, &H, &A);
	FIELD_SQR(&A, &B);
	FIELD_MUL(sumz, diff, &A);
	FIELD_MUL(dblx, &F, &G);
	//Absorb possible negative montgomery constant into next addition, turning it into substration.
	if(MONTGOMERY_CONSTANT > 0) {
		FIELD_SMUL(&A, &E, MONTGOMERY_CONSTANT);
		FIELD_ADD(&B, &G, &A);
	} else {
		FIELD_SMUL(&A, &E, -MONTGOMERY_CONSTANT);
		FIELD_SUB(&B, &G, &A);
	}
	FIELD_MUL(dblz, &E, &B);
}

static inline int _montgomery_mul(field_t* output, const uint32_t* mul, const field_t* base)
{
	field_t x1a, z1a, x2a, z2a, x1b, z1b, x2b, z2b;
	FIELD_LOAD_SMALL(&x1a, 1);
	FIELD_ZERO(&z1a);
	x2a = *base;
	FIELD_LOAD_SMALL(&z2a, 1);
	FIELD_ZERO(&x1b);
	FIELD_LOAD_SMALL(&z1b, 1);
	FIELD_ZERO(&x2b);
	FIELD_LOAD_SMALL(&z2b, 1);

	uint32_t lbit = 0;
	for(unsigned i = SCALAR_WORDS - 1; i < SCALAR_WORDS; i--) {
		uint32_t x = mul[i];
		for(unsigned j = 0; j < 16; j++) {
			uint32_t bit = (x >> 31);
			FIELD_CSWAP(&x1a, &x2a, bit ^ lbit);
			FIELD_CSWAP(&z1a, &z2a, bit ^ lbit);
			_montgomery(&x1b, &z1b, &x2b, &z2b, &x1a, &z1a, &x2a, &z2a, base);
			lbit = bit;
			x <<= 1;
			bit = (x >> 31);
			FIELD_CSWAP(&x1b, &x2b, bit ^ lbit);
			FIELD_CSWAP(&z1b, &z2b, bit ^ lbit);
			_montgomery(&x1a, &z1a, &x2a, &z2a, &x1b, &z1b, &x2b, &z2b, base);
			x <<= 1;
			lbit = bit;
		}
	}
	FIELD_CSWAP(&x1a, &x2a, lbit);
	FIELD_CSWAP(&z1a, &z2a, lbit);
	FIELD_INV(&z2a, &z1a);
	FIELD_MUL(output, &x1a, &z2a);
	return 1;
}

static inline void _init_window(point_t win[16], const point_t* base)
{
	_point_zero(&win[0]);
	win[1] = *base;
	//Fill the rest.
	for(unsigned i = 2; i < 16; i += 2) {
		_point_double(win + i, win + (i >> 1));
		_point_add(win + i + 1, win + i, base);
	}
}

static inline void _load_window(point_t* res, point_t win[16], uint32_t index)
{
	for(unsigned i = 0; i < 16; i++) {
		uint32_t flag = i - index;
		//flag = (flag | (flag >> 4)) & 15;	//window has 16 elements.
		flag = (flag | (flag >> 2)) & 3;
		flag = (flag | (flag >> 1)) & 1;
		_point_load_cond(res, win + i, 1  - flag);
	}
}

static void _point_mul_base(point_t* out, const uint32_t* mul)
{
	static point_t points[SCALAR_WORDS * 8 * 16];
	point_t accum, tmp2;
	static volatile unsigned init = 0;
	if(!init) {
		call_thread_lock();
		point_t base;
		_point_base(&base);
		for(unsigned i = 0; i < SCALAR_WORDS * 8; i++) {
			_init_window(points + i * 16, &base);
			_point_double(&base, &base);
			_point_double(&base, &base);
			_point_double(&base, &base);
			_point_double(&base, &base);
		}
		init = 1;
		call_thread_unlock();
	}
	_point_zero(&accum);
	_point_zero(&tmp2);	//Avoid false warning from Valgrind.
	unsigned k = 0;
	for(unsigned i = 0; i < SCALAR_WORDS; i++) {
		uint32_t word = mul[i];
		//Note: This assumes window is 4 bits.
		for(unsigned j = 0; j < 8; j++, k++) {
			_load_window(&tmp2, points + k * 16, word & 15);
			_point_add(&accum, &accum, &tmp2);
			word >>= 4;
		}
	}
	*out = accum;
}

static void _point_mul(point_t* out, const uint32_t* mul, const point_t* in)
{
	point_t win[16], accum, tmp2;
	_init_window(win, in);
	_point_zero(&accum);
	_point_zero(&tmp2);	//Avoid false warning from Valgrind.
	for(unsigned i = SCALAR_WORDS - 1; i < SCALAR_WORDS; i--) {
		uint32_t word = mul[i];
		//Note: This assumes window is 4 bits.
		for(unsigned j = 0; j < 8; j++) {
			_load_window(&tmp2, win, word >> 28);
			_point_double(&accum, &accum);
			_point_double(&accum, &accum);
			_point_double(&accum, &accum);
			_point_double(&accum, &accum);
			_point_add(&accum, &accum, &tmp2);
			word <<= 4;
		}
	}
	*out = accum;
}

#include "curve-fn-wrappers.inc"
