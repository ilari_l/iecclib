static inline void _point_load_cond(point_t* out, const point_t* in, uint32_t flag)
{
	FIELD_LOAD_COND(&out->x, &in->x, flag);
	FIELD_LOAD_COND(&out->y, &in->y, flag);
	FIELD_LOAD_COND(&out->z, &in->z, flag);
}

static inline void _point_zero(point_t* point)
{
	FIELD_ZERO(&point->x);
	FIELD_LOAD_SMALL(&point->y, 1);
	FIELD_LOAD_SMALL(&point->z, 1);
}

static inline void _point_base(point_t* point)
{
	do_global_init();
	point->x = base_x;
	point->y = base_y;
	FIELD_LOAD_SMALL(&point->z, 1);
}

static inline void _point_double(point_t* out, const point_t* in)
{
	field_t A, B, C, D, E, F;
	FIELD_ADD(&A, &in->x, &in->y);
	FIELD_SQR(&B, &A);
	FIELD_SQR(&A, &in->x);
	FIELD_SQR(&D, &in->y);
	FIELD_ADD(&C, &A, &D);
	FIELD_SQR(&E, &in->z);
	FIELD_ADD(&F, &E, &E);
	FIELD_SUB(&E, &C, &F);
	FIELD_SUB(&F, &B, &C);
	FIELD_MUL(&out->x, &F, &E);
	FIELD_SUB(&B, &A, &D);
	FIELD_MUL(&out->y, &C, &B);
	FIELD_MUL(&out->z, &C, &E);
}

static inline void _point_neg(point_t* out, const point_t* in)
{
	FIELD_NEG(&out->x, &in->x);
	out->y = in->y;
	out->z = in->z;
}
