#ifndef _ecclib_utils_h_included_
#define _ecclib_utils_h_included_

#include <stdint.h>
#include <stdlib.h>
#include "libpfx.h"

#define streq FAKE_NOEXPORT(streq)
#define call_thread_lock FAKE_NOEXPORT(call_thread_lock)
#define call_thread_unlock FAKE_NOEXPORT(call_thread_unlock)

/**
 * Compare two NUL-terminated strings for equality.
 *
 * \param left The first string.
 * \param right The second string.
 * \returns 1 if strings are equal, 0 if not.
 */
int streq(const char* left, const char* right);

/**
 * Call thread lock function.
 */
void call_thread_lock();

/**
 * Call thread unlock function.
 */
void call_thread_unlock();

/**
 * Copy octets from buffer to buffer.
 *
 * \param out The output
 * \param in The input
 * \param size Number of octets to copy.
 */
static inline void copy_octets(uint8_t* out, const uint8_t* in, size_t size)
{
	for(size_t i = 0; i < size; i++)
		out[i] = in[i];
}

#define DO_ALIGN(TYPE, PTR,POWER) (TYPE*)(((size_t)(PTR) + ((1 << (POWER)) - 1)) >> (POWER) << (POWER))
#define ELEMENTS(X) (sizeof(X) / sizeof((X)[0]))

#endif
