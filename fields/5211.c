#include "5211defs.inc"
#include "iecclib.h"

#ifdef DEFAULT_F5211
//in is assumed to be <2^1088
static void reduce_after_mul(uint32_t out[17], const uint32_t in[34])
{
	uint32_t tmp[18];
	uint32_t tmpl[18];
	uint32_t tmph[18];
	uint32_t hmul = 1UL << 23;

	//Multiply high part of in by 2^23 and add to low part.
	for(unsigned i = 0; i < 17; i++) tmpl[i] = in[i];
	tmpl[17] = 0;	//This is needed for padding.
	ecclib_bigint_mul(tmph, in + 17, 17, &hmul, 1);
	ecclib_bigint_add(tmp, tmpl, tmph, 18);
	//Now tmp is at most 2^24*2^521, so carry will fit to 32 bits.
	uint64_t carry = ((tmp[16] >> 9) | (tmp[17] << 23));
	for(unsigned i = 0; i < 16; i++) {
		carry += tmp[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	out[16] = carry + (tmp[16] & 0x1FF);
}

//in is assumed to be <2^31*2^521
static void reduce_after_add(uint32_t out[17], const uint32_t in[18])
{
	uint64_t carry = ((in[16] >> 9) | (in[17] << 23));
	for(unsigned i = 0; i < 16; i++) {
		carry += in[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	out[16] = carry + (in[16] & 0x1FF);
}

//65536 * modulus
static const uint32_t sub_bias[17] = {
	0xFFFF0000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0x01FFFFFF
};

//The modulus as words
static const uint32_t modulus32[17] = {
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0x000001FF
};

void fe5211_load(struct fe5211* out, const uint8_t* num)
{
	for(unsigned i = 0; i < 17; i++)
		out->x[i] = 0;
	for(unsigned i = 0; i < 66; i++)
		out->x[i>>2] |= num[i] << ((i&3)<<3);
	out->x[16] &= 0x1FF;		//Mask high bits.
}

void fe5211_store(uint8_t* out, const struct fe5211* elem)
{
	uint32_t tmp[17];
	uint32_t tmp3[17];
	uint32_t sub[17];
	uint64_t carry = elem->x[16] >> 9;
	for(unsigned i = 0; i < 16; i++) {
		carry += elem->x[i];
		tmp[i] = carry;
		carry >>= 32;
	}
	//The high bits of last word is masked off.
	tmp[16] = carry + (elem->x[16] & 0x1FF);

	//Compare against modulus.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 16; i < 17; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (tmp[i] < modulus32[i]);
		gt |= neutral & (tmp[i] > modulus32[i]);
	}
	//Load substract.
	uint32_t mask = -(1^lt);
	for(unsigned i = 0; i < 17; i++) sub[i] = modulus32[i] & mask;
	//Substract the modulus if needed.
	ecclib_bigint_sub(tmp3, tmp, sub, 17);
	//Now, write this to bytes
	for(unsigned i = 0; i < 66; i++)
		out[i] = tmp3[i>>2] >> ((i&3)<<3);
}

void fe5211_zero(struct fe5211* out)
{
	for(unsigned i = 0; i < 17; i++) out->x[i] = 0;
}

void fe5211_load_small(struct fe5211* out, uint32_t num)
{
	for(unsigned i = 0; i < 17; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe5211_neg(struct fe5211* out, const struct fe5211* in)
{
	uint32_t result[18];
	ecclib_bigint_sub(result, sub_bias, in->x, 17);
	result[17] = 0;
	reduce_after_add(out->x, result);
}

void fe5211_sub(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in2)
{
	uint32_t tmp[17];
	uint32_t result[18];
	ecclib_bigint_add(tmp, in1->x, sub_bias, 17);
	ecclib_bigint_sub(result, tmp, in2->x, 17);
	result[17] = 0;
	reduce_after_add(out->x, result);
}

void fe5211_add(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in2)
{
	uint32_t result[18];
	ecclib_bigint_add(result, in1->x, in2->x, 17);
	result[17] = 0;
	reduce_after_add(out->x, result);
}

void fe5211_smul(struct fe5211* out, const struct fe5211* in1, uint32_t in2)
{
	uint32_t result[18];
	ecclib_bigint_mul(result, in1->x, 17, &in2, 1);
	//This is so small reduce_after_add can do it.
	reduce_after_add(out->x, result);
}

void fe5211_mul(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in2)
{
	uint32_t result[34];
	ecclib_bigint_mul(result, in1->x, 17, in2->x, 17);
	reduce_after_mul(out->x, result);
}

void fe5211_sqr(struct fe5211* out, const struct fe5211* in)
{
	uint32_t result[34];
	ecclib_bigint_mul(result, in->x, 17, in->x, 17);
	reduce_after_mul(out->x, result);
}

void fe5211_load_cond(struct fe5211* out, const struct fe5211* in, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 17; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe5211_select(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in0, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 17; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe5211_cswap(struct fe5211* x0, struct fe5211* x1, uint32_t flag)
{
	uint32_t rflag = -flag;
	for(unsigned i = 0; i < 17; i++) {
		uint32_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif

#include "5211-sqrt-inv.inc"
#define FIELD_ELEMENT_COUNT modulus
#define FIELD_CHARACTERISTIC modulus
#include "field.inc"
