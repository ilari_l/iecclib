#include "5211.h"
#include "utils2.h"
typedef struct fe5211 field_t;
#define FIELD_NAME field_5211
#define FIELD_REAL_NAME NAME_field_5211
#define FIELD_BITS FE5211_BITS
#define FIELD_DEGREE 1
#define STORAGE_OCTETS FE5211_STORE_SIZE
#define FIELD_DEGREE_STRIDE STORAGE_OCTETS
#define FIELD_DEGREE_BITS FIELD_BITS
#define FIELD_ALIGN_POWER FE5211_ALIGN_POWER
#define ELEMENT_SIZE (sizeof(struct fe5211) + (1 << FIELD_ALIGN_POWER))
#define FIELD_ZERO(X) fe5211_zero(X)
#define FIELD_CHECK(X) fe5211_check(X)
#define FIELD_LOAD(X, Y) fe5211_load(X, Y)
#define FIELD_STORE(X, Y) fe5211_store(X, Y)
#define FIELD_SQR(X, Y) fe5211_sqr(X, Y)
#define FIELD_INV(X, Y) fe5211_inv(X, Y)
#define FIELD_NEG(X, Y) fe5211_neg(X, Y)
#define FIELD_SQRT(X, Y) fe5211_sqrt(X, Y)
#define FIELD_LOAD_SMALL(X, Y) fe5211_load_small(X, Y)
#define FIELD_ADD(X, Y, Z) fe5211_add(X, Y, Z)
#define FIELD_SUB(X, Y, Z) fe5211_sub(X, Y, Z)
#define FIELD_MUL(X, Y, Z) fe5211_mul(X, Y, Z)
#define FIELD_SMUL(X, Y, Z) fe5211_smul(X, Y, Z)
#define FIELD_CSWAP(X, Y, Z) fe5211_cswap(X, Y, Z)
#define FIELD_LOAD_COND(X, Y, Z) fe5211_load_cond(X, Y, Z)
#define FIELD_SELECT(X, Y, Z, W) fe5211_select(X, Y, Z, W)
#define FIELD_STORE_SIZE BITS_TO_OCTETS(FIELD_BITS)
