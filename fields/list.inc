#ifdef DO_CC_INCLUDES
#include "25519.h"
#include "3363.h"
#include "38921.h"
#include "41417.h"
#include "448.h"
#include "5211.h"
#else
CANDIDATE(field_25519)
CANDIDATE(field_3363)
CANDIDATE(field_38921)
CANDIDATE(field_41417)
CANDIDATE(field_448)
CANDIDATE(field_5211)
#endif
