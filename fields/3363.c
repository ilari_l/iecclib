#include "3363defs.inc"
#include "iecclib.h"

#ifdef DEFAULT_F3363
//in is assumed to be <2^700
static void reduce_after_mul(uint32_t out[11], const uint32_t in[22])
{
	uint32_t tmp[12];
	uint32_t tmpl[12];
	uint32_t tmph[12];
	uint32_t hmul = 196608;

	//Multiply high part of in by 196608 and add to low part.
	for(unsigned i = 0; i < 11; i++) tmpl[i] = in[i];
	tmpl[11] = 0;	//This is needed for padding.
	ecclib_bigint_mul(tmph, in + 11, 11, &hmul, 1);
	ecclib_bigint_add(tmp, tmpl, tmph, 12);

	//Now tmp is at most 1431655765*2^336, so carry will fit to 32 bits.
	uint64_t carry = ((tmp[10] >> 16) | (tmp[11] << 16)) * 3;
	for(unsigned i = 0; i < 10; i++) {
		carry += tmp[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bits of last word are masked off.
	out[10] = carry + (tmp[10] & 0xFFFF);
}

//in is assumed to be <2^366
static void reduce_after_add(uint32_t out[11], const uint32_t in[12])
{
	uint64_t carry = ((in[10] >> 16) | (in[11] << 16)) * 3;
	for(unsigned i = 0; i < 10; i++) {
		carry += in[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bits of last word is masked off.
	out[10] = carry + (in[10] & 0xFFFF);
}

//32768 * modulus
static const uint32_t sub_bias[12] = {
	0xFFFE8000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF, 0x00000000
};

//The modulus as words
static const uint32_t modulus32[11] = {
	0xFFFFFFFD, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0x0000FFFF
};

void fe3363_load(struct fe3363* out, const uint8_t* num)
{
	for(unsigned i = 0; i < 11; i++)
		out->x[i] = 0;
	for(unsigned i = 0; i < 42; i++)
		out->x[i>>2] |= num[i] << ((i&3)<<3);
	out->x[10] &= 0xFFFF;	//Mask high bits.
}

void fe3363_store(uint8_t* out, const struct fe3363* elem)
{
	uint32_t tmp[11];
	uint32_t tmp3[11];
	uint32_t sub[11];
	uint64_t carry = (elem->x[10] >> 16) * 3;
	for(unsigned i = 0; i < 10; i++) {
		carry += elem->x[i];
		tmp[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	tmp[10] = carry + (elem->x[10] & 0xFFFF);

	//Compare against modulus.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 10; i < 11; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (tmp[i] < modulus32[i]);
		gt |= neutral & (tmp[i] > modulus32[i]);
	}
	//Load substract.
	uint32_t mask = -(1^lt);
	for(unsigned i = 0; i < 11; i++) sub[i] = modulus32[i] & mask;
	//Substract the modulus if needed.
	ecclib_bigint_sub(tmp3, tmp, sub, 11);
	//Now, write this to bytes
	for(unsigned i = 0; i < 42; i++)
		out[i] = tmp3[i>>2] >> ((i&3)<<3);
}

void fe3363_zero(struct fe3363* out)
{
	for(unsigned i = 0; i < 11; i++) out->x[i] = 0;
}

void fe3363_load_small(struct fe3363* out, uint32_t num)
{
	for(unsigned i = 0; i < 11; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe3363_neg(struct fe3363* out, const struct fe3363* in)
{
	uint32_t result[12];
	ecclib_bigint_sub(result, sub_bias, in->x, 11);
	result[11] = 0;
	reduce_after_add(out->x, result);
}

void fe3363_sub(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in2)
{
	uint32_t tmp[12];
	uint32_t result[12];

	ecclib_bigint_add(tmp, in1->x, sub_bias, 11);
	ecclib_bigint_sub(result, tmp, in2->x, 11);
	result[11] = 0;
	reduce_after_add(out->x, result);
}

void fe3363_add(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in2)
{
	uint32_t result[12];
	ecclib_bigint_add(result, in1->x, in2->x, 11);
	result[11] = 0;
	reduce_after_add(out->x, result);
}

void fe3363_smul(struct fe3363* out, const struct fe3363* in1, uint32_t in2)
{
	uint32_t result[12];
	ecclib_bigint_mul(result, in1->x, 11, &in2, 1);
	//This is so small reduce_after_add can do it.
	reduce_after_add(out->x, result);
}

void fe3363_mul(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in2)
{
	uint32_t result[22];
	ecclib_bigint_mul(result, in1->x, 11, in2->x, 11);
	reduce_after_mul(out->x, result);
}

void fe3363_sqr(struct fe3363* out, const struct fe3363* in)
{
	uint32_t result[22];
	ecclib_bigint_mul(result, in->x, 11, in->x, 11);
	reduce_after_mul(out->x, result);
}

void fe3363_load_cond(struct fe3363* out, const struct fe3363* in, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 11; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe3363_select(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in0, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 11; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe3363_cswap(struct fe3363* x0, struct fe3363* x1, uint32_t flag)
{
	uint32_t rflag = -flag;
	for(unsigned i = 0; i < 11; i++) {
		uint32_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif

#include "3363-sqrt-inv.inc"
#define FIELD_ELEMENT_COUNT modulus
#define FIELD_CHARACTERISTIC modulus
#include "field.inc"
