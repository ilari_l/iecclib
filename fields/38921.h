#ifndef _ecclib_field_38921_h_included_
#define _ecclib_field_38921_h_included_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include "utils2.h"
#include "libpfx.h"

struct fe38921
{
#ifdef USE_64BIT_MATH
	uint64_t x[7];
#else
#define DEFAULT_F38921
	uint32_t x[13];
#endif
};

#define NAME_field_38921 "2^389-21"
#define FE38921_BITS 389
#define FE38921_STORE_SIZE BITS_TO_OCTETS(FE38921_BITS)
#define FE38921_ALIGN_POWER 4

#define field_38921 FAKE_NOEXPORT(field_38921)
#define fe38921_add FAKE_NOEXPORT(fe38921_add)
#define fe38921_check FAKE_NOEXPORT(fe38921_check)
#define fe38921_cswap FAKE_NOEXPORT(fe38921_cswap)
#define fe38921_inv FAKE_NOEXPORT(fe38921_inv)
#define fe38921_load FAKE_NOEXPORT(fe38921_load)
#define fe38921_load_cond FAKE_NOEXPORT(fe38921_load_cond)
#define fe38921_load_small FAKE_NOEXPORT(fe38921_load_small)
#define fe38921_mul FAKE_NOEXPORT(fe38921_mul)
#define fe38921_neg FAKE_NOEXPORT(fe38921_neg)
#define fe38921_select FAKE_NOEXPORT(fe38921_select)
#define fe38921_smul FAKE_NOEXPORT(fe38921_smul)
#define fe38921_sqr FAKE_NOEXPORT(fe38921_sqr)
#define fe38921_sqrt FAKE_NOEXPORT(fe38921_sqrt)
#define fe38921_store FAKE_NOEXPORT(fe38921_store)
#define fe38921_sub FAKE_NOEXPORT(fe38921_sub)
#define fe38921_zero FAKE_NOEXPORT(fe38921_zero)

/**
 * Initialize field element to zero.
 *
 * \param out The element.
 */
void fe38921_zero(struct fe38921* out);

/**
 * Initialize field element to a small value.
 *
 * \param out The element.
 * \param num The value.
 */
void fe38921_load_small(struct fe38921* out, uint32_t num);

/**
 * Initialize field element to given value.
 *
 * \param out The element.
 * \param num The value. Given as base-256-little-endian, 32 octets.
 */
void fe38921_load(struct fe38921* out, const uint8_t* num);

/**
 * Check if value is smaller than 2^255-19.
 *
 * \param num The number to check. Given as base-256-littlen-endian, 32 octets.
 * \returns 0 if smaller, -1 if not.
 */
int fe38921_check(const uint8_t* num);						//-1 if outside range.

/**
 * Store field element into octet array.
 *
 * \param out The buffer to store to, 32 octets.
 * \param elem The element to store.
 */
void fe38921_store(uint8_t* out, const struct fe38921* elem);

/**
 * Add two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to add.
 * \param in2 The second element to add.
 */
void fe38921_add(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in2);

/**
 * Substract two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to substract.
 * \param in2 The second element to substract.
 */
void fe38921_sub(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in2);

/**
 * Multiply two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to multiply.
 * \param in2 The second element to multiply.
 */
void fe38921_mul(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in2);

/**
 * Multiply field element by small constant.
 *
 * \param out The element to write the result to.
 * \param in1 The element to multiply.
 * \param in2 The multiplier.
 */
void fe38921_smul(struct fe38921* out, const struct fe38921* in1, uint32_t in2);

/**
 * Negate a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to negate.
 */
void fe38921_neg(struct fe38921* out, const struct fe38921* in);

/**
 * Square a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to square.
 * \note This is the same as multiplying element by itself, but faster.
 */
void fe38921_sqr(struct fe38921* out, const struct fe38921* in);

/**
 * Square root of a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to take square root of.
 * \returns 0 on success, -1 if no square root exists.
 */
int fe38921_sqrt(struct fe38921* out, const struct fe38921* in);		//-1 on failure.

/**
 * Invert a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to invert.
 */
void fe38921_inv(struct fe38921* out, const struct fe38921* in);

/**
 * Conditionally load a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to read.
 * \param flag If 1, the copy is done, if 0 nothing is done.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe38921_load_cond(struct fe38921* out, const struct fe38921* in, uint32_t flag);

/**
 * Select field element.
 *
 * \param out The element to write the result to.
 * \param in1 The element to read if flag is 1
 * \param in0 The element to read if flag is 0
 * \param flag The elment to read.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe38921_select(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in0, uint32_t flag);

/**
 * Conditional swap.
 *
 * \param x0 The elements to swap
 * \param x1 The elements to swap
 * \param flag If 1, swap, if 0 do nothing.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe38921_cswap(struct fe38921* x0, struct fe38921* x1, uint32_t flag);

extern const struct ecclib_field field_38921;

#ifdef __cplusplus
}
#endif


#endif
