#include "38921defs.inc"
#include "iecclib.h"
#include <stdio.h>
#include <stdlib.h>

#ifdef DEFAULT_F38921
//in is assumed to be <2^800
static void reduce_after_mul(uint32_t out[13], const uint32_t in[26])
{
	uint32_t tmp[14];
	uint32_t tmpl[14];
	uint32_t tmph[14];
	uint32_t hmul = 2818572288;

	//Multiply high part of in by 2818572288 and add to low part.
	for(unsigned i = 0; i < 13; i++) tmpl[i] = in[i];
	tmpl[13] = 0;	//This is needed for padding.
	ecclib_bigint_mul(tmph, in + 13, 13, &hmul, 1);
	ecclib_bigint_add(tmp, tmpl, tmph, 14);
	//Now tmp is at most 2^417.
	//The carry in next one can be up to 33 bits.
	uint64_t carry = (uint64_t)((tmp[12] >> 5) | (tmp[13] << 27)) * 21;
	for(unsigned i = 0; i < 12; i++) {
		carry += tmp[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	out[12] = carry + (tmp[12] & 0x1F);
}

//in is assumed to be <204522252*2^389
static void reduce_after_add(uint32_t out[13], const uint32_t in[14])
{
	uint64_t carry = (in[12] >> 5) * 21;
	for(unsigned i = 0; i < 12; i++) {
		carry += in[i];
		out[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	out[12] = carry + (in[12] & 0x1F);
}

//65536 * modulus
static const uint32_t sub_bias[13] = {
	0xFFEB0000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x001FFFFF
};

//The modulus as words
static const uint32_t modulus32[13] = {
	0xFFFFFFEB, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x0000001F
};


void fe38921_load(struct fe38921* out, const uint8_t* num)
{
	for(unsigned i = 0; i < 13; i++)
		out->x[i] = 0;
	for(unsigned i = 0; i < 49; i++)
		out->x[i>>2] |= num[i] << ((i&3)<<3);
	out->x[12] &= 0x1F;		//Mask high bit.
}

void fe38921_store(uint8_t* out, const struct fe38921* elem)
{
	uint32_t tmp[13];
	uint32_t tmp3[13];
	uint32_t sub[13];
	uint64_t carry = (elem->x[12] >> 5) * 21;
	for(unsigned i = 0; i < 13; i++) {
		carry += elem->x[i];
		tmp[i] = carry;
		carry >>= 32;
	}
	//The high bit of last word is masked off.
	tmp[12] = carry + (elem->x[12] & 0x1F);

	//Compare against modulus.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 12; i < 13; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (tmp[i] < modulus32[i]);
		gt |= neutral & (tmp[i] > modulus32[i]);
	}
	//Load substract.
	uint32_t mask = -(1^lt);
	for(unsigned i = 0; i < 13; i++) sub[i] = modulus32[i] & mask;
	//Substract the modulus if needed.
	ecclib_bigint_sub(tmp3, tmp, sub, 13);
	//Now, write this to bytes
	for(unsigned i = 0; i < 49; i++)
		out[i] = tmp3[i>>2] >> ((i&3)<<3);
}

void fe38921_zero(struct fe38921* out)
{
	for(unsigned i = 0; i < 13; i++) out->x[i] = 0;
}

void fe38921_load_small(struct fe38921* out, uint32_t num)
{
	for(unsigned i = 0; i < 13; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe38921_neg(struct fe38921* out, const struct fe38921* in)
{
	uint32_t result[14];
	ecclib_bigint_sub(result, sub_bias, in->x, 13);
	result[13] = 0;
	reduce_after_add(out->x, result);
}

void fe38921_sub(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in2)
{
	uint32_t tmp[14];
	uint32_t result[14];
	ecclib_bigint_add(tmp, in1->x, sub_bias, 13);
	ecclib_bigint_sub(result, tmp, in2->x, 13);
	result[13] = 0;
	reduce_after_add(out->x, result);
}

void fe38921_add(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in2)
{
	uint32_t result[14];
	ecclib_bigint_add(result, in1->x, in2->x, 13);
	result[13] = 0;
	reduce_after_add(out->x, result);
}

void fe38921_smul(struct fe38921* out, const struct fe38921* in1, uint32_t in2)
{
	uint32_t result[14];
	ecclib_bigint_mul(result, in1->x, 13, &in2, 1);
	//This is so small reduce_after_add can do it.
	reduce_after_add(out->x, result);
}

void fe38921_mul(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in2)
{
	uint32_t result[26];
	ecclib_bigint_mul(result, in1->x, 13, in2->x, 13);
	reduce_after_mul(out->x, result);
}

void fe38921_sqr(struct fe38921* out, const struct fe38921* in)
{
	uint32_t result[26];
	ecclib_bigint_mul(result, in->x, 13, in->x, 13);
	reduce_after_mul(out->x, result);
}

void fe38921_load_cond(struct fe38921* out, const struct fe38921* in, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 13; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe38921_select(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in0, uint32_t flag)
{
	uint32_t rflag = -flag;
	uint32_t iflag = ~rflag;
	for(unsigned i = 0; i < 13; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe38921_cswap(struct fe38921* x0, struct fe38921* x1, uint32_t flag)
{
	uint32_t rflag = -flag;
	for(unsigned i = 0; i < 13; i++) {
		uint32_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif

#include "38921-sqrt-inv.inc"
#define FIELD_ELEMENT_COUNT modulus
#define FIELD_CHARACTERISTIC modulus
#include "field.inc"
