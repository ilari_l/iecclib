#ifndef _ecclib_field_3363_h_included_
#define _ecclib_field_3363_h_included_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include "utils2.h"
#include "libpfx.h"

struct fe3363
{
#ifdef USE_64BIT_MATH
	uint64_t x[6];
#else
#define DEFAULT_F3363
	uint32_t x[11];
#endif
};

#define NAME_field_3363 "2^336-3"
#define FE3363_BITS 336
#define FE3363_STORE_SIZE BITS_TO_OCTETS(FE3363_BITS)
#define FE3363_ALIGN_POWER 3

#define field_3363 FAKE_NOEXPORT(field_3363)
#define fe3363_add FAKE_NOEXPORT(fe3363_add)
#define fe3363_check FAKE_NOEXPORT(fe3363_check)
#define fe3363_cswap FAKE_NOEXPORT(fe3363_cswap)
#define fe3363_inv FAKE_NOEXPORT(fe3363_inv)
#define fe3363_load FAKE_NOEXPORT(fe3363_load)
#define fe3363_load_cond FAKE_NOEXPORT(fe3363_load_cond)
#define fe3363_load_small FAKE_NOEXPORT(fe3363_load_small)
#define fe3363_mul FAKE_NOEXPORT(fe3363_mul)
#define fe3363_neg FAKE_NOEXPORT(fe3363_neg)
#define fe3363_select FAKE_NOEXPORT(fe3363_select)
#define fe3363_smul FAKE_NOEXPORT(fe3363_smul)
#define fe3363_sqr FAKE_NOEXPORT(fe3363_sqr)
#define fe3363_sqrt FAKE_NOEXPORT(fe3363_sqrt)
#define fe3363_store FAKE_NOEXPORT(fe3363_store)
#define fe3363_sub FAKE_NOEXPORT(fe3363_sub)
#define fe3363_zero FAKE_NOEXPORT(fe3363_zero)

/**
 * Initialize field element to zero.
 *
 * \param out The element.
 */
void fe3363_zero(struct fe3363* out);

/**
 * Initialize field element to a small value.
 *
 * \param out The element.
 * \param num The value.
 */
void fe3363_load_small(struct fe3363* out, uint32_t num);

/**
 * Initialize field element to given value.
 *
 * \param out The element.
 * \param num The value. Given as base-256-little-endian, 32 octets.
 */
void fe3363_load(struct fe3363* out, const uint8_t* num);

/**
 * Check if value is smaller than 2^255-19.
 *
 * \param num The number to check. Given as base-256-littlen-endian, 32 octets.
 * \returns 0 if smaller, -1 if not.
 */
int fe3363_check(const uint8_t* num);						//-1 if outside range.

/**
 * Store field element into octet array.
 *
 * \param out The buffer to store to, 32 octets.
 * \param elem The element to store.
 */
void fe3363_store(uint8_t* out, const struct fe3363* elem);

/**
 * Add two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to add.
 * \param in2 The second element to add.
 */
void fe3363_add(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in2);

/**
 * Substract two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to substract.
 * \param in2 The second element to substract.
 */
void fe3363_sub(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in2);

/**
 * Multiply two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to multiply.
 * \param in2 The second element to multiply.
 */
void fe3363_mul(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in2);

/**
 * Multiply field element by small constant.
 *
 * \param out The element to write the result to.
 * \param in1 The element to multiply.
 * \param in2 The multiplier.
 */
void fe3363_smul(struct fe3363* out, const struct fe3363* in1, uint32_t in2);

/**
 * Negate a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to negate.
 */
void fe3363_neg(struct fe3363* out, const struct fe3363* in);

/**
 * Square a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to square.
 * \note This is the same as multiplying element by itself, but faster.
 */
void fe3363_sqr(struct fe3363* out, const struct fe3363* in);

/**
 * Square root of a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to take square root of.
 * \returns 0 on success, -1 if no square root exists.
 */
int fe3363_sqrt(struct fe3363* out, const struct fe3363* in);		//-1 on failure.

/**
 * Invert a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to invert.
 */
void fe3363_inv(struct fe3363* out, const struct fe3363* in);

/**
 * Conditionally load a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to read.
 * \param flag If 1, the copy is done, if 0 nothing is done.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe3363_load_cond(struct fe3363* out, const struct fe3363* in, uint32_t flag);

/**
 * Select field element.
 *
 * \param out The element to write the result to.
 * \param in1 The element to read if flag is 1
 * \param in0 The element to read if flag is 0
 * \param flag The elment to read.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe3363_select(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in0, uint32_t flag);

/**
 * Conditional swap.
 *
 * \param x0 The elements to swap
 * \param x1 The elements to swap
 * \param flag If 1, swap, if 0 do nothing.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe3363_cswap(struct fe3363* x0, struct fe3363* x1, uint32_t flag);

extern const struct ecclib_field field_3363;

#ifdef __cplusplus
}
#endif


#endif
