//The modulus as bytes
static const uint8_t modulus[32] = {
	0xED, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F
};

int fe25519_check(const uint8_t* num)
{
	uint8_t lt = 0, gt = 0;
	for(unsigned i = 31; i < 32; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (num[i] < modulus[i]);
		gt |= neutral & (num[i] > modulus[i]);
	}
	return (int)lt;
}

static void fe25519_sqrn(struct fe25519* out, const struct fe25519* in, unsigned count)
{
	struct fe25519 tmp1, tmp2;
	if(count % 2 == 0) {
		fe25519_sqr(&tmp1, in);
		for(unsigned i = 0; i < (count-2)/2; i++) {
			fe25519_sqr(&tmp2, &tmp1);
			fe25519_sqr(&tmp1, &tmp2);
		}
		fe25519_sqr(out, &tmp1);
	} else {
		fe25519_sqr(&tmp1, in);
		fe25519_sqr(&tmp2, &tmp1);
		for(unsigned i = 0; i < (count-3)/2; i++) {
			fe25519_sqr(&tmp1, &tmp2);
			fe25519_sqr(&tmp2, &tmp1);
		}
		fe25519_sqr(out, &tmp2);
	}
}

int fe25519_sqrt(struct fe25519* out, const struct fe25519* in)
{
	//v=(2a)^(2^252-3)
	//i=(2a)v^2
	//r=av(i-1)
	//2^252-3=2^252-4+1=4*(2^250-1)+1
	struct fe25519 r, y, g, b, c, d, o;
	uint8_t check1[32];
	uint8_t check2[32];
	fe25519_load_small(&o, 1);
	fe25519_add(&d, in, in);	//2a
	fe25519_sqr(&y, &d);		//2
	fe25519_sqrn(&g, &y, 2);	//8
	fe25519_mul(&b, &g, &d);	//9
	fe25519_mul(&r, &b, &y);	//11
	fe25519_sqr(&y, &r);		//22
	fe25519_mul(&g, &y, &b);	//31
	fe25519_sqrn(&y, &g, 5);	//2^10-2^5
	fe25519_mul(&b, &y, &g);	//2^10-1
	fe25519_sqrn(&y, &b, 10);	//2^20-2^10
	fe25519_mul(&g, &y, &b);	//2^20-1
	fe25519_sqrn(&y, &g, 20);	//2^40-2^20
	fe25519_mul(&c, &y, &g);	//2^40-1
	fe25519_sqrn(&g, &c, 10);	//2^50-2^10
	fe25519_mul(&y, &g, &b);	//2^50-1
	fe25519_sqrn(&b, &y, 50);	//2^100-2^50
	fe25519_mul(&g, &y, &b);	//2^100-1
	fe25519_sqrn(&b, &g, 100);	//2^200-2^100
	fe25519_mul(&c, &g, &b);	//2^200-1
	fe25519_sqrn(&g, &c, 50);	//2^250-2^50
	fe25519_mul(&b, &y, &g);	//2^250-1
	fe25519_sqrn(&r, &b, 2);	//2^252-4
	fe25519_mul(&y, &r, &d);	//2^252-3 => y is v.
	fe25519_sqr(&g, &y);		//v^2
	fe25519_mul(&c, &d, &g);	//(2a)v^2 => c is i
	fe25519_mul(&b, in, &y);	//av
	fe25519_sub(&r, &c, &o);	//i-1
	fe25519_mul(&g, &b, &r);	//candidate root.
	fe25519_sqr(&r, &g);		//check value.
	fe25519_store(check1, in);
	fe25519_store(check2, &r);
	uint8_t syndrome = 0;
	for(unsigned i = 0; i < 32; i++)
		syndrome |= (check1[i] ^ check2[i]);
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	fe25519_load_cond(out, &g, syndrome ^ 1);
	return 1-(int)syndrome;
}

void fe25519_inv(struct fe25519* out, const struct fe25519* in)
{
	//Raise to power of 2^255-21
	struct fe25519 r, y, g, b, c;
	fe25519_sqr(&y, in);		//2
	fe25519_sqrn(&g, &y, 2);	//8
	fe25519_mul(&b, &g, in);	//9
	fe25519_mul(&r, &b, &y);	//11
	fe25519_sqr(&y, &r);		//22
	fe25519_mul(&g, &y, &b);	//31
	fe25519_sqrn(&y, &g, 5);	//2^10-2^5
	fe25519_mul(&b, &y, &g);	//2^10-1
	fe25519_sqrn(&y, &b, 10);	//2^20-2^10
	fe25519_mul(&g, &y, &b);	//2^20-1
	fe25519_sqrn(&y, &g, 20);	//2^40-2^20
	fe25519_mul(&c, &y, &g);	//2^40-1
	fe25519_sqrn(&g, &c, 10);	//2^50-2^10
	fe25519_mul(&y, &g, &b);	//2^50-1
	fe25519_sqrn(&b, &y, 50);	//2^100-2^50
	fe25519_mul(&g, &y, &b);	//2^100-1
	fe25519_sqrn(&b, &g, 100);	//2^200-2^100
	fe25519_mul(&c, &g, &b);	//2^200-1
	fe25519_sqrn(&g, &c, 50);	//2^250-2^50
	fe25519_mul(&b, &y, &g);	//2^250-1
	fe25519_sqrn(&y, &b, 5);	//2^255-32
	fe25519_mul(out, &r, &y);	//2^255-21
}
