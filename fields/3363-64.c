#include "3363.h"
#include "iecclib.h"
#ifdef USE_64BIT_MATH

static const uint64_t emask = (1ULL << 56) - 1;
static const unsigned eshift = 56;
static const uint64_t fconst = 3;

//The modulus as words
static const uint64_t modulus64[6] = {
	0xFFFFFFFFFFFFFD, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF
};

void fe3363_load(struct fe3363* out, const uint8_t* num)
{
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 6; i++) {
		while(shift < eshift) {
			if(idx == 42) break;
			uint64_t byte = num[idx];
			tmp = tmp + (byte << shift);
			idx++;
			shift += 8;
		}
		out->x[i] = tmp & emask;
		shift -= eshift;
		tmp >>= eshift;
	}
}

static void reduce(uint64_t out[6], const uint64_t in[6])
{
	uint64_t carry = fconst * (in[5] >> eshift);
	for(unsigned i = 0; i < 6; i++) out[i] = in[i];
	out[5] = in[5] & emask;
	for(unsigned i = 0; i < 6; i++) {
		carry = out[i] + carry;
		out[i] = carry & emask;
		carry >>= eshift;
	}
	out[5] = out[5] + (carry << eshift);
	uint64_t gt = 0;
	uint64_t lt = 0;
	uint64_t s[6] = {0};
	for(unsigned i = 5; i < 6; i--) {
		uint64_t neutral = (gt ^ 1) & (lt ^ 1);
		gt |= neutral & (out[i] > modulus64[i]);
		lt |= neutral & (out[i] < modulus64[i]);
	}
	uint64_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 6; i++) s[i] = modulus64[i] & mask;
	uint64_t borrow = 0;
	for(unsigned i = 0; i < 6; i++) {
		out[i] = out[i] - s[i] - borrow;
		borrow = out[i] >> 63;
		out[i] &= emask;
	}
}

void fe3363_store(uint8_t* out, const struct fe3363* elem)
{
	uint64_t y[6];
	reduce(y, elem->x);
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 6; i++) {
		tmp = tmp + (y[i] << shift);
		shift = shift + eshift;
		while(shift >= 8) {
			if(idx >= 42) return;
			out[idx] = tmp;
			idx++;
			tmp >>= 8;
			shift -= 8;
		}
	}
}

void fe3363_zero(struct fe3363* out)
{
	for(unsigned i = 0; i < 6; i++) out->x[i] = 0;
}

void fe3363_load_small(struct fe3363* out, uint32_t num)
{
	for(unsigned i = 0; i < 6; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe3363_neg(struct fe3363* out, const struct fe3363* in)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) - in->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 6; i++) {
		uint64_t r = 4 * emask - in->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe3363_sub(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in2)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) + in1->x[0] - in2->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 6; i++) {
		uint64_t r = 4 * emask + in1->x[i] - in2->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe3363_add(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in2)
{
	uint64_t carry = 0;
	for(unsigned i = 0; i < 6; i++) {
		carry = in1->x[i] + in2->x[i] + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	out->x[0] += fconst * carry;
}

#define WM(X, Y) ((__uint128_t)(X) * (__uint128_t)(Y))

void fe3363_smul(struct fe3363* out, const struct fe3363* in1, uint32_t in2)
{
	__uint128_t carry = 0, carry2 = 0;
	for(unsigned i = 0; i < 6; i++) {
		carry = WM(in1->x[i], in2) + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	carry2 = out->x[0] + fconst * carry;
	out->x[0] = carry2 & emask;
	carry2 >>= eshift;
	out->x[1] += carry2;
}

void fe3363_mul(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in2)
{
	__uint128_t W=0;
	uint64_t a0 = in1->x[0], a1 = in1->x[1], a2 = in1->x[2], a3 = in1->x[3], a4 = in1->x[4], a5 = in1->x[5];
	uint64_t b0 = in2->x[0], b1 = in2->x[1], b2 = in2->x[2], b3 = in2->x[3], b4 = in2->x[4], b5 = in2->x[5];
	uint64_t c1 = fconst * b1, c2 = fconst * b2, c3 = fconst * b3, c4 = fconst * b4, c5 = fconst * b5;

	W = W + WM(a0, b0) + WM(a1, c5) + WM(a2, c4) + WM(a3, c3) + WM(a4, c2) + WM(a5, c1);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(a0, b1) + WM(a1, b0) + WM(a2, c5) + WM(a3, c4) + WM(a4, c3) + WM(a5, c2);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(a0, b2) + WM(a1, b1) + WM(a2, b0) + WM(a3, c5) + WM(a4, c4) + WM(a5, c3);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(a0, b3) + WM(a1, b2) + WM(a2, b1) + WM(a3, b0) + WM(a4, c5) + WM(a5, c4);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(a0, b4) + WM(a1, b3) + WM(a2, b2) + WM(a3, b1) + WM(a4, b0) + WM(a5, c5);
	out->x[4] = W & emask; W >>= eshift;
	W = W + WM(a0, b5) + WM(a1, b4) + WM(a2, b3) + WM(a3, b2) + WM(a4, b1) + WM(a5, b0);
	out->x[5] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask; W >>= eshift;
	out->x[1] += W;
}

void fe3363_sqr(struct fe3363* out, const struct fe3363* in)
{
	//C1=C*X1
	//C2=C*X2
	//C3=C*X3
	//C4=C*X4
	//C5=C*X5
	//Y0=X0*X0  + C1*2X5 +C2*2X4+C3*X3
	//Y1=X0*2X1 + C2*2X5 +C3*2X4
	//Y2=X0*2X2 + X1*X1  +C3*2X5+C4*X4
	//Y3=X0*2X3 + X1*2X2 +C4*2X5
	//Y4=X0*2X4 + X1*2X3 +X2*X2+C5*X5
	//Y5=X0*2X5 + X1*2X4 +X2*2X3
	//
	//
	__uint128_t W=0;
	uint64_t a0 = in->x[0], a1 = in->x[1], a2 = in->x[2], a3 = in->x[3], a4 = in->x[4], a5 = in->x[5];
	uint64_t b1 = fconst * a1, b2 = fconst * a2, b3 = fconst * a3, b4 = fconst * a4, b5 = fconst * a5;
	uint64_t c1 = a1 << 1, c2 = a2 << 1, c3 = a3 << 1, c4 = a4 << 1, c5 = a5 << 1;

	W = W + WM(a0, a0) + WM(b1, c5) + WM(b2, c4) + WM(b3, a3);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(a0, c1) + WM(b2, c5) + WM(b3, c4);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(a0, c2) + WM(a1, a1) + WM(b3, c5) + WM(b4, a4);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(a0, c3) + WM(a1, c2) + WM(b4, c5);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(a0, c4) + WM(a1, c3) + WM(a2, a2) + WM(b5, a5);
	out->x[4] = W & emask; W >>= eshift;
	W = W + WM(a0, c5) + WM(a1, c4) + WM(a2, c3);
	out->x[5] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask; W >>= eshift;
	out->x[1] += W;
}

void fe3363_load_cond(struct fe3363* out, const struct fe3363* in, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 6; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe3363_select(struct fe3363* out, const struct fe3363* in1, const struct fe3363* in0, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 6; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe3363_cswap(struct fe3363* x0, struct fe3363* x1, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	for(unsigned i = 0; i < 6; i++) {
		uint64_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif
