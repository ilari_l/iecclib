#ifndef _ecclib_field_25519_h_included_
#define _ecclib_field_25519_h_included_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include "utils2.h"
#include "libpfx.h"

struct fe25519
{
#ifdef USE_64BIT_MATH
	uint64_t x[5];
#else
#define DEFAULT_F25519
	uint32_t x[9];	//+1 extra word for overflows
#endif
};

#define NAME_field_25519 "2^255-19"
#define FE25519_BITS 255
#define FE25519_STORE_SIZE BITS_TO_OCTETS(FE25519_BITS)
#define FE25519_ALIGN_POWER 4

#define field_25519 FAKE_NOEXPORT(field_25519)
#define fe25519_add FAKE_NOEXPORT(fe25519_add)
#define fe25519_check FAKE_NOEXPORT(fe25519_check)
#define fe25519_cswap FAKE_NOEXPORT(fe25519_cswap)
#define fe25519_inv FAKE_NOEXPORT(fe25519_inv)
#define fe25519_load FAKE_NOEXPORT(fe25519_load)
#define fe25519_load_cond FAKE_NOEXPORT(fe25519_load_cond)
#define fe25519_load_small FAKE_NOEXPORT(fe25519_load_small)
#define fe25519_mul FAKE_NOEXPORT(fe25519_mul)
#define fe25519_neg FAKE_NOEXPORT(fe25519_neg)
#define fe25519_select FAKE_NOEXPORT(fe25519_select)
#define fe25519_smul FAKE_NOEXPORT(fe25519_smul)
#define fe25519_sqr FAKE_NOEXPORT(fe25519_sqr)
#define fe25519_sqrt FAKE_NOEXPORT(fe25519_sqrt)
#define fe25519_store FAKE_NOEXPORT(fe25519_store)
#define fe25519_sub FAKE_NOEXPORT(fe25519_sub)
#define fe25519_zero FAKE_NOEXPORT(fe25519_zero)

/**
 * Initialize field element to zero.
 *
 * \param out The element.
 */
void fe25519_zero(struct fe25519* out);

/**
 * Initialize field element to a small value.
 *
 * \param out The element.
 * \param num The value.
 */
void fe25519_load_small(struct fe25519* out, uint32_t num);

/**
 * Initialize field element to given value.
 *
 * \param out The element.
 * \param num The value. Given as base-256-little-endian, 32 octets.
 */
void fe25519_load(struct fe25519* out, const uint8_t* num);

/**
 * Check if value is smaller than 2^255-19.
 *
 * \param num The number to check. Given as base-256-littlen-endian, 32 octets.
 * \returns 0 if smaller, -1 if not.
 */
int fe25519_check(const uint8_t* num);						//-1 if outside range.

/**
 * Store field element into octet array.
 *
 * \param out The buffer to store to, 32 octets.
 * \param elem The element to store.
 */
void fe25519_store(uint8_t* out, const struct fe25519* elem);

/**
 * Add two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to add.
 * \param in2 The second element to add.
 */
void fe25519_add(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in2);

/**
 * Substract two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to substract.
 * \param in2 The second element to substract.
 */
void fe25519_sub(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in2);

/**
 * Multiply two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to multiply.
 * \param in2 The second element to multiply.
 */
void fe25519_mul(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in2);

/**
 * Multiply field element by small constant.
 *
 * \param out The element to write the result to.
 * \param in1 The element to multiply.
 * \param in2 The multiplier.
 */
void fe25519_smul(struct fe25519* out, const struct fe25519* in1, uint32_t in2);

/**
 * Negate a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to negate.
 */
void fe25519_neg(struct fe25519* out, const struct fe25519* in);

/**
 * Square a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to square.
 * \note This is the same as multiplying element by itself, but faster.
 */
void fe25519_sqr(struct fe25519* out, const struct fe25519* in);

/**
 * Square root of a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to take square root of.
 * \returns 0 on success, -1 if no square root exists.
 */
int fe25519_sqrt(struct fe25519* out, const struct fe25519* in);		//-1 on failure.

/**
 * Invert a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to invert.
 */
void fe25519_inv(struct fe25519* out, const struct fe25519* in);

/**
 * Conditionally load a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to read.
 * \param flag If 1, the copy is done, if 0 nothing is done.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe25519_load_cond(struct fe25519* out, const struct fe25519* in, uint32_t flag);

/**
 * Select field element.
 *
 * \param out The element to write the result to.
 * \param in1 The element to read if flag is 1
 * \param in0 The element to read if flag is 0
 * \param flag The elment to read.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe25519_select(struct fe25519* out, const struct fe25519* in1, const struct fe25519* in0, uint32_t flag);

/**
 * Conditional swap.
 *
 * \param x0 The elements to swap
 * \param x1 The elements to swap
 * \param flag If 1, swap, if 0 do nothing.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe25519_cswap(struct fe25519* x0, struct fe25519* x1, uint32_t flag);

extern const struct ecclib_field field_25519;

#ifdef __cplusplus
}
#endif


#endif
