//The modulus as bytes
static const uint8_t modulus[52] = {
	0xEF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0x3F
};

int fe41417_check(const uint8_t* num)
{
	uint8_t lt = 0, gt = 0;
	for(unsigned i = 51; i < 52; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (num[i] < modulus[i]);
		gt |= neutral & (num[i] > modulus[i]);
	}
	return (int)lt;
}

static void fe41417_sqrn(struct fe41417* out, const struct fe41417* in, unsigned count)
{
	struct fe41417 tmp1, tmp2;
	if(count % 2 == 0) {
		fe41417_sqr(&tmp1, in);
		for(unsigned i = 0; i < (count-2)/2; i++) {
			fe41417_sqr(&tmp2, &tmp1);
			fe41417_sqr(&tmp1, &tmp2);
		}
		fe41417_sqr(out, &tmp1);
	} else {
		fe41417_sqr(&tmp1, in);
		fe41417_sqr(&tmp2, &tmp1);
		for(unsigned i = 0; i < (count-3)/2; i++) {
			fe41417_sqr(&tmp1, &tmp2);
			fe41417_sqr(&tmp2, &tmp1);
		}
		fe41417_sqr(out, &tmp2);
	}
}

int fe41417_sqrt(struct fe41417* out, const struct fe41417* in)
{
	//Raise to power of 2^410-1 (r)
	struct fe41417 r, g, b;
	fe41417_sqr(&g, in);		//2
	fe41417_mul(&b, &g, in);	//3
	fe41417_sqr(&g, &b);		//6
	fe41417_mul(&b, &g, in);	//7
	fe41417_sqrn(&r, &b, 3);	//2^6-2^3
	fe41417_mul(&g, &r, &b);	//2^6-1
	fe41417_sqrn(&r, &g, 6);	//2^12-2^6
	fe41417_mul(&b, &r, &g);	//2^12-1
	fe41417_sqrn(&g, &b, 12);	//2^24-2^12
	fe41417_mul(&r, &g, &b);	//2^24-1
	fe41417_sqr(&g, &r);		//2^25-2
	fe41417_mul(&b, &g, in);	//2^25-1
	fe41417_sqrn(&g, &b, 25);	//2^50-2^25
	fe41417_mul(&r, &g, &b);	//2^50-1
	fe41417_sqr(&g, &r);		//2^51-2
	fe41417_mul(&b, &g, in);	//2^51-1
	fe41417_sqrn(&g, &b, 51);	//2^102-2^51
	fe41417_mul(&r, &g, &b);	//2^102-1
	fe41417_sqrn(&g, &r, 102);	//2^204-2^102
	fe41417_mul(&b, &g, &r);	//2^204-1
	fe41417_sqr(&g, &b);		//2^205-2
	fe41417_mul(&b, &g, in);	//2^205-1
	fe41417_sqrn(&g, &b, 205);	//2^410-2^205
	fe41417_mul(&r, &g, &b);	//2^410-1
	fe41417_sqrn(&g, &r, 2);	//candidate root.
	uint8_t check1[52];
	uint8_t check2[52];
	fe41417_sqr(&r, &g);		//check value.
	fe41417_store(check1, in);
	fe41417_store(check2, &r);
	uint8_t syndrome = 0;
	for(unsigned i = 0; i < 52; i++)
		syndrome |= (check1[i] ^ check2[i]);
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	fe41417_load_cond(out, &g, syndrome ^ 1);
	return 1-(int)syndrome;
}

void fe41417_inv(struct fe41417* out, const struct fe41417* in)
{
	//2^414-19=(2^414-32)+7+6
	//Raise to power of 2^414-19
	struct fe41417 r, y, g, b, c;
	fe41417_sqr(&r, in);		//2
	fe41417_mul(&g, &r, in);	//3
	fe41417_sqr(&c, &g);		//6
	fe41417_mul(&b, &c, in);	//7
	fe41417_sqrn(&r, &b, 3);	//2^6-2^3
	fe41417_mul(&y, &r, &b);	//2^6-1
	fe41417_sqrn(&r, &y, 6);	//2^12-2^6
	fe41417_mul(&g, &r, &y);	//2^12-1
	fe41417_sqrn(&y, &g, 12);	//2^24-2^12
	fe41417_mul(&r, &y, &g);	//2^24-1
	fe41417_sqr(&y, &r);		//2^25-2
	fe41417_mul(&r, &y, in);	//2^25-2
	fe41417_sqrn(&g, &r, 25);	//2^50-2^25
	fe41417_mul(&y, &g, &r);	//2^50-1
	fe41417_sqr(&r, &y);		//2^51-2
	fe41417_mul(&y, &r, in);	//2^51-1
	fe41417_sqrn(&r, &y, 51);	//2^102-2^51
	fe41417_mul(&g, &r, &y);	//2^102-1
	fe41417_sqrn(&r, &g, 102);	//2^204-2^102
	fe41417_mul(&y, &g, &r);	//2^204-1
	fe41417_sqrn(&g, &y, 204);	//2^408-2^204
	fe41417_mul(&r, &g, &y);	//2^408-1
	fe41417_sqr(&g, &r);		//2^409-2
	fe41417_mul(&r, &g, in);	//2^409-1
	fe41417_sqrn(&g, &r, 5);	//2^414-32
	fe41417_mul(&r, &g, &c);	//2^414-25
	fe41417_mul(out, &r, &b);	//2^414-19
}
