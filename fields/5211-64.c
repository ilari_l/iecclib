#include "5211.h"
#include "iecclib.h"
#ifdef USE_64BIT_MATH

static const uint64_t emask = (1ULL << 58) - 1;
static const unsigned eshift = 58;
static const uint64_t fconst = 2;
static const uint64_t rfconst = 1;

//The modulus as words
static const uint64_t modulus64[9] = {
	0x3FFFFFFFFFFFFFF, 0x3FFFFFFFFFFFFFF, 0x3FFFFFFFFFFFFFF, 0x3FFFFFFFFFFFFFF,
	0x3FFFFFFFFFFFFFF, 0x3FFFFFFFFFFFFFF, 0x3FFFFFFFFFFFFFF, 0x3FFFFFFFFFFFFFF,
	0x1FFFFFFFFFFFFFF
};

void fe5211_load(struct fe5211* out, const uint8_t* num)
{
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 9; i++) {
		while(shift < eshift) {
			if(idx == 66) break;
			uint64_t byte = num[idx];
			tmp = tmp + (byte << shift);
			idx++;
			shift += 8;
		}
		out->x[i] = tmp & emask;
		shift -= eshift;
		tmp >>= eshift;
	}
	out->x[8] &= (emask >> 1);
}

static void reduce(uint64_t out[9], const uint64_t in[9])
{
	uint64_t carry = rfconst * (in[8] >> (eshift - 1));
	for(unsigned i = 0; i < 9; i++) out[i] = in[i];
	out[8] = in[8] & (emask >> 1);
	for(unsigned i = 0; i < 9; i++) {
		carry = out[i] + carry;
		out[i] = carry & emask;
		carry >>= eshift;
	}
	out[8] = out[8] + (carry << eshift);
	uint64_t gt = 0;
	uint64_t lt = 0;
	uint64_t s[9] = {0};
	for(unsigned i = 8; i < 9; i--) {
		uint64_t neutral = (gt ^ 1) & (lt ^ 1);
		gt |= neutral & (out[i] > modulus64[i]);
		lt |= neutral & (out[i] < modulus64[i]);
	}
	uint64_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 9; i++) s[i] = modulus64[i] & mask;
	uint64_t borrow = 0;
	for(unsigned i = 0; i < 9; i++) {
		out[i] = out[i] - s[i] - borrow;
		borrow = out[i] >> 63;
		out[i] &= emask;
	}
}

void fe5211_store(uint8_t* out, const struct fe5211* elem)
{
	uint64_t y[9];
	reduce(y, elem->x);
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 9; i++) {
		tmp = tmp + (y[i] << shift);
		shift = shift + eshift;
		while(shift >= 8) {
			out[idx] = tmp;
			idx++;
			tmp >>= 8;
			shift -= 8;
		}
	}
	out[65] = tmp;
}

void fe5211_zero(struct fe5211* out)
{
	for(unsigned i = 0; i < 9; i++) out->x[i] = 0;
}

void fe5211_load_small(struct fe5211* out, uint32_t num)
{
	for(unsigned i = 0; i < 9; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe5211_neg(struct fe5211* out, const struct fe5211* in)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) - in->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 9; i++) {
		uint64_t r = 4 * emask - in->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe5211_sub(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in2)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) + in1->x[0] - in2->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 9; i++) {
		uint64_t r = 4 * emask + in1->x[i] - in2->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe5211_add(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in2)
{
	uint64_t carry = 0;
	for(unsigned i = 0; i < 9; i++) {
		carry = in1->x[i] + in2->x[i] + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	out->x[0] += fconst * carry;
}

#define WM(X, Y) ((__uint128_t)(X) * (__uint128_t)(Y))

void fe5211_smul(struct fe5211* out, const struct fe5211* in1, uint32_t in2)
{
	__uint128_t carry = 0, carry2 = 0;
	for(unsigned i = 0; i < 9; i++) {
		carry = WM(in1->x[i], in2) + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	carry2 = out->x[0] + fconst * carry;
	out->x[0] = carry2 & emask;
	carry2 >>= eshift;
	out->x[1] += carry2;
}

void fe5211_mul(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in2)
{
	__uint128_t W=0;
	uint64_t a0 = in1->x[0], a1 = in1->x[1], a2 = in1->x[2], a3 = in1->x[3], a4 = in1->x[4], a5 = in1->x[5],
		a6 = in1->x[6], a7 = in1->x[7], a8 = in1->x[8];
	uint64_t b0 = in2->x[0], b1 = in2->x[1], b2 = in2->x[2], b3 = in2->x[3], b4 = in2->x[4], b5 = in2->x[5],
		b6 = in2->x[6], b7 = in2->x[7], b8 = in2->x[8];
	uint64_t c1 = (b1 << 1), c2 = (b2 << 1), c3 = (b3 << 1), c4 = (b4 << 1), c5 = (b5 << 1), c6 = (b6 << 1),
		c7 = (b7 << 1), c8 = (b8 << 1);

	W = W + WM(a0, b0) + WM(a1, c8) + WM(a2, c7) + WM(a3, c6) + WM(a4, c5) + WM(a5, c4) + WM(a6, c3) + WM(a7, c2)
		+ WM(a8, c1);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(a0, b1) + WM(a1, b0) + WM(a2, c8) + WM(a3, c7) + WM(a4, c6) + WM(a5, c5) + WM(a6, c4) + WM(a7, c3)
		+ WM(a8, c2);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(a0, b2) + WM(a1, b1) + WM(a2, b0) + WM(a3, c8) + WM(a4, c7) + WM(a5, c6) + WM(a6, c5) + WM(a7, c4)
		+ WM(a8, c3);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(a0, b3) + WM(a1, b2) + WM(a2, b1) + WM(a3, b0) + WM(a4, c8) + WM(a5, c7) + WM(a6, c6) + WM(a7, c5)
		+ WM(a8, c4);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(a0, b4) + WM(a1, b3) + WM(a2, b2) + WM(a3, b1) + WM(a4, b0) + WM(a5, c8) + WM(a6, c7) + WM(a7, c6)
		+ WM(a8, c5);
	out->x[4] = W & emask; W >>= eshift;
	W = W + WM(a0, b5) + WM(a1, b4) + WM(a2, b3) + WM(a3, b2) + WM(a4, b1) + WM(a5, b0) + WM(a6, c8) + WM(a7, c7)
		+ WM(a8, c6);
	out->x[5] = W & emask; W >>= eshift;
	W = W + WM(a0, b6) + WM(a1, b5) + WM(a2, b4) + WM(a3, b3) + WM(a4, b2) + WM(a5, b1) + WM(a6, b0) + WM(a7, c8)
		+ WM(a8, c7);
	out->x[6] = W & emask; W >>= eshift;
	W = W + WM(a0, b7) + WM(a1, b6) + WM(a2, b5) + WM(a3, b4) + WM(a4, b3) + WM(a5, b2) + WM(a6, b1) + WM(a7, b0)
		+ WM(a8, c8);
	out->x[7] = W & emask; W >>= eshift;
	W = W + WM(a0, b8) + WM(a1, b7) + WM(a2, b6) + WM(a3, b5) + WM(a4, b4) + WM(a5, b3) + WM(a6, b2) + WM(a7, b1)
		+ WM(a8, b0);
	out->x[8] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask;
	W >>= eshift;
	out->x[1] += W;
}


void fe5211_sqr(struct fe5211* out, const struct fe5211* in)
{
	__uint128_t W=0;

	uint64_t a0 = in->x[0], a1 = in->x[1], a2 = in->x[2], a3 = in->x[3], a4 = in->x[4], a5 = in->x[5],
		a6 = in->x[6], a7 = in->x[7], a8 = in->x[8];
	uint64_t c1 = (a1 << 1), c2 = (a2 << 1), c3 = (a3 << 1), c4 = (a4 << 1), c5 = (a5 << 1), c6 = (a6 << 1),
		c7 = (a7 << 1), c8 = (a8 << 1);
	uint64_t e8 = a8 << 2, e7 = a7 << 2, e6 = a6 << 2, e5 = a5 << 2;

	W = W + WM(a0, a0) + WM(a1, e8) + WM(a2, e7) + WM(a3, e6) + WM(a4, e5);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(a0, c1) + WM(a2, e8) + WM(a3, e7) + WM(a4, e6) + WM(a5, c5);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(a0, c2) + WM(a1, a1) + WM(a3, e8) + WM(a4, e7) + WM(a5, e6);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(a0, c3) + WM(a1, c2) + WM(a4, e8) + WM(a5, e7) + WM(a6, c6);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(a0, c4) + WM(a1, c3) + WM(a2, a2) + WM(a5, e8) + WM(a6, e7);
	out->x[4] = W & emask; W >>= eshift;
	W = W + WM(a0, c5) + WM(a1, c4) + WM(a2, c3) + WM(a6, e8) + WM(a7, c7);
	out->x[5] = W & emask; W >>= eshift;
	W = W + WM(a0, c6) + WM(a1, c5) + WM(a2, c4) + WM(a3, a3) + WM(a7, e8);
	out->x[6] = W & emask; W >>= eshift;
	W = W + WM(a0, c7) + WM(a1, c6) + WM(a2, c5) + WM(a3, c4) + WM(a8, c8);
	out->x[7] = W & emask; W >>= eshift;
	W = W + WM(a0, c8) + WM(a1, c7) + WM(a2, c6) + WM(a3, c5) + WM(a4, a4);
	out->x[8] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask;
	W >>= eshift;
	out->x[1] += W;
}

void fe5211_load_cond(struct fe5211* out, const struct fe5211* in, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 9; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe5211_select(struct fe5211* out, const struct fe5211* in1, const struct fe5211* in0, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 9; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe5211_cswap(struct fe5211* x0, struct fe5211* x1, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	for(unsigned i = 0; i < 9; i++) {
		uint64_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif
