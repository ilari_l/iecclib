#include "38921.h"
#include "iecclib.h"
#ifdef USE_64BIT_MATH

static const uint64_t emask = (1ULL << 56) - 1;
static const unsigned eshift = 56;
static const uint64_t fconst = 168;
static const uint64_t rfconst = 21;

//The modulus as words
static const uint64_t modulus64[7] = {
	0xFFFFFFFFFFFFEB, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF,
	0x1FFFFFFFFFFFFF
};

void fe38921_load(struct fe38921* out, const uint8_t* num)
{
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 7; i++) {
		while(shift < eshift) {
			if(idx == 49) break;
			uint64_t byte = num[idx];
			tmp = tmp + (byte << shift);
			idx++;
			shift += 8;
		}
		out->x[i] = tmp & emask;
		shift -= eshift;
		tmp >>= eshift;
	}
	out->x[6] &= (emask >> 3);
}

static void reduce(uint64_t out[7], const uint64_t in[7])
{
	uint64_t carry = rfconst * (in[6] >> (eshift - 3));
	for(unsigned i = 0; i < 7; i++) out[i] = in[i];
	out[6] = in[6] & (emask >> 3);
	for(unsigned i = 0; i < 7; i++) {
		carry = out[i] + carry;
		out[i] = carry & emask;
		carry >>= eshift;
	}
	out[6] = out[6] + (carry << eshift);
	uint64_t gt = 0;
	uint64_t lt = 0;
	uint64_t s[7] = {0};
	for(unsigned i = 6; i < 7; i--) {
		uint64_t neutral = (gt ^ 1) & (lt ^ 1);
		gt |= neutral & (out[i] > modulus64[i]);
		lt |= neutral & (out[i] < modulus64[i]);
	}
	uint64_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 7; i++) s[i] = modulus64[i] & mask;
	uint64_t borrow = 0;
	for(unsigned i = 0; i < 7; i++) {
		out[i] = out[i] - s[i] - borrow;
		borrow = out[i] >> 63;
		out[i] &= emask;
	}
}

void fe38921_store(uint8_t* out, const struct fe38921* elem)
{
	uint64_t y[7];
	reduce(y, elem->x);
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 7; i++) {
		tmp = tmp + (y[i] << shift);
		shift = shift + eshift;
		while(shift >= 8) {
			if(idx >= 49) return;
			out[idx] = tmp;
			idx++;
			tmp >>= 8;
			shift -= 8;
		}
	}
}

void fe38921_zero(struct fe38921* out)
{
	for(unsigned i = 0; i < 7; i++) out->x[i] = 0;
}

void fe38921_load_small(struct fe38921* out, uint32_t num)
{
	for(unsigned i = 0; i < 7; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe38921_neg(struct fe38921* out, const struct fe38921* in)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) - in->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 7; i++) {
		uint64_t r = 4 * emask - in->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe38921_sub(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in2)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) + in1->x[0] - in2->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 7; i++) {
		uint64_t r = 4 * emask + in1->x[i] - in2->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe38921_add(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in2)
{
	uint64_t carry = 0;
	for(unsigned i = 0; i < 7; i++) {
		carry = in1->x[i] + in2->x[i] + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	out->x[0] += fconst * carry;
}

#define WM(X, Y) ((__uint128_t)(X) * (__uint128_t)(Y))

void fe38921_smul(struct fe38921* out, const struct fe38921* in1, uint32_t in2)
{
	__uint128_t carry = 0, carry2 = 0;
	for(unsigned i = 0; i < 7; i++) {
		carry = WM(in1->x[i], in2) + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	carry2 = out->x[0] + fconst * carry;
	out->x[0] = carry2 & emask;
	carry2 >>= eshift;
	out->x[1] += carry2;
}

void fe38921_mul(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in2)
{
	__uint128_t W=0;
	uint64_t a0 = in1->x[0], a1 = in1->x[1], a2 = in1->x[2], a3 = in1->x[3], a4 = in1->x[4], a5 = in1->x[5],
		a6 = in1->x[6];
	uint64_t b0 = in2->x[0], b1 = in2->x[1], b2 = in2->x[2], b3 = in2->x[3], b4 = in2->x[4], b5 = in2->x[5],
		b6 = in2->x[6];
	uint64_t c1 = fconst * b1, c2 = fconst * b2,  c3 = fconst * b3, c4 = fconst * b4, c5 = fconst * b5,
		c6 = fconst * b6;

	W = W + WM(a0, b0) + WM(a1, c6) + WM(a2, c5) + WM(a3, c4) + WM(a4, c3) + WM(a5, c2) + WM(a6, c1);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(a0, b1) + WM(a1, b0) + WM(a2, c6) + WM(a3, c5) + WM(a4, c4) + WM(a5, c3) + WM(a6, c2);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(a0, b2) + WM(a1, b1) + WM(a2, b0) + WM(a3, c6) + WM(a4, c5) + WM(a5, c4) + WM(a6, c3);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(a0, b3) + WM(a1, b2) + WM(a2, b1) + WM(a3, b0) + WM(a4, c6) + WM(a5, c5) + WM(a6, c4);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(a0, b4) + WM(a1, b3) + WM(a2, b2) + WM(a3, b1) + WM(a4, b0) + WM(a5, c6) + WM(a6, c5);
	out->x[4] = W & emask; W >>= eshift;
	W = W + WM(a0, b5) + WM(a1, b4) + WM(a2, b3) + WM(a3, b2) + WM(a4, b1) + WM(a5, b0) + WM(a6, c6);
	out->x[5] = W & emask; W >>= eshift;
	W = W + WM(a0, b6) + WM(a1, b5) + WM(a2, b4) + WM(a3, b3) + WM(a4, b2) + WM(a5, b1) + WM(a6, b0);
	out->x[6] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask;
	W >>= eshift;
	out->x[1] += W;
}

void fe38921_sqr(struct fe38921* out, const struct fe38921* in)
{
	__uint128_t W=0;
	uint64_t a0 = in->x[0], a1 = in->x[1], a2 = in->x[2], a3 = in->x[3], a4 = in->x[4], a5 = in->x[5],
		a6 = in->x[6];
	uint64_t b1 = a1 * fconst, b2 = a2 * fconst, b3 = a3 * fconst, b4 = a4 * fconst, b5 = a5 * fconst,
		b6 = a6 * fconst;
	uint64_t c1 = a1 << 1, c2 = a2 << 1, c3 = a3 << 1, c4 = a4 << 1, c5 = a5 << 1, c6 = a6 << 1;

	W = W + WM(a0, a0) + WM(b1, c6) + WM(b2, c5) + WM(b3, c4);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(a0, c1) + WM(b2, c6) + WM(b3, c5) + WM(b4, a4);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(a0, c2) + WM(a1, a1) + WM(b3, c6) + WM(b4, c5);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(a0, c3) + WM(a1, c2) + WM(b4, c6) + WM(b5, a5);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(a0, c4) + WM(a1, c3) + WM(a2, a2) + WM(b5, c6);
	out->x[4] = W & emask; W >>= eshift;
	W = W + WM(a0, c5) + WM(a1, c4) + WM(a2, c3) + WM(b6, a6);
	out->x[5] = W & emask; W >>= eshift;
	W = W + WM(a0, c6) + WM(a1, c5) + WM(a2, c4) + WM(a3, a3);
	out->x[6] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask;
	W >>= eshift;
	out->x[1] += W;
}

void fe38921_load_cond(struct fe38921* out, const struct fe38921* in, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 7; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe38921_select(struct fe38921* out, const struct fe38921* in1, const struct fe38921* in0, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 7; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe38921_cswap(struct fe38921* x0, struct fe38921* x1, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	for(unsigned i = 0; i < 7; i++) {
		uint64_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif
