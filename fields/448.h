#ifndef _ecclib_field_448_h_included_
#define _ecclib_field_448_h_included_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include "utils2.h"
#include "libpfx.h"

/**
 * Element of field GF(2^448-2^224-1)
 */
struct fe448
{
#ifdef USE_64BIT_MATH
	uint64_t x[8];
#else
#define DEFAULT_F448
	uint32_t x[15];	//+1 Extra word for intermediates too large.
#endif
};

#define NAME_field_448 "2^448-2^224-1"
#define FE448_BITS 448
#define FE448_STORE_SIZE BITS_TO_OCTETS(FE448_BITS)
#define FE448_ALIGN_POWER 4

#define field_448 FAKE_NOEXPORT(field_448)
#define fe448_add FAKE_NOEXPORT(fe448_add)
#define fe448_check FAKE_NOEXPORT(fe448_check)
#define fe448_cswap FAKE_NOEXPORT(fe448_cswap)
#define fe448_inv FAKE_NOEXPORT(fe448_inv)
#define fe448_load FAKE_NOEXPORT(fe448_load)
#define fe448_load_cond FAKE_NOEXPORT(fe448_load_cond)
#define fe448_load_small FAKE_NOEXPORT(fe448_load_small)
#define fe448_mul FAKE_NOEXPORT(fe448_mul)
#define fe448_neg FAKE_NOEXPORT(fe448_neg)
#define fe448_select FAKE_NOEXPORT(fe448_select)
#define fe448_smul FAKE_NOEXPORT(fe448_smul)
#define fe448_sqr FAKE_NOEXPORT(fe448_sqr)
#define fe448_sqrt FAKE_NOEXPORT(fe448_sqrt)
#define fe448_store FAKE_NOEXPORT(fe448_store)
#define fe448_sub FAKE_NOEXPORT(fe448_sub)
#define fe448_zero FAKE_NOEXPORT(fe448_zero)

/**
 * Initialize field element to zero.
 *
 * \param out The element.
 */
void fe448_zero(struct fe448* out);

/**
 * Initialize field element to a small value.
 *
 * \param out The element.
 * \param num The value.
 */
void fe448_load_small(struct fe448* out, uint32_t num);

/**
 * Initialize field element to given value.
 *
 * \param out The element.
 * \param num The value. Given as base-256-little-endian, 56 octets.
 */
void fe448_load(struct fe448* out, const uint8_t* num);

/**
 * Check if value is smaller than 2^448-2^224-1.
 *
 * \param num The number to check. Given as base-256-littlen-endian, 56 octets.
 * \returns 0 if smaller, -1 if not.
 */
int fe448_check(const uint8_t* num);

/**
 * Store field element into octet array.
 *
 * \param out The buffer to store to, 56 octets.
 * \param elem The element to store.
 */
void fe448_store(uint8_t* out, const struct fe448* elem);

/**
 * Add two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to add.
 * \param in2 The second element to add.
 */
void fe448_add(struct fe448* out, const struct fe448* in1, const struct fe448* in2);

/**
 * Substract two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to substract.
 * \param in2 The second element to substract.
 */
void fe448_sub(struct fe448* out, const struct fe448* in1, const struct fe448* in2);

/**
 * Multiply two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to multiply.
 * \param in2 The second element to multiply.
 */
void fe448_mul(struct fe448* out, const struct fe448* in1, const struct fe448* in2);

/**
 * Multiply field element by small constant.
 *
 * \param out The element to write the result to.
 * \param in1 The element to multiply.
 * \param in2 The multiplier.
 */
void fe448_smul(struct fe448* out, const struct fe448* in1, uint32_t in2);

/**
 * Negate a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to negate.
 */
void fe448_neg(struct fe448* out, const struct fe448* in);

/**
 * Square a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to square.
 * \note This is the same as multiplying element by itself, but faster.
 */
void fe448_sqr(struct fe448* out, const struct fe448* in);

/**
 * Square root of a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to take square root of.
 * \returns 0 on success, -1 if no square root exists.
 */
int fe448_sqrt(struct fe448* out, const struct fe448* in);

/**
 * Invert a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to invert.
 */
void fe448_inv(struct fe448* out, const struct fe448* in);

/**
 * Conditionally load a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to read.
 * \param flag If 1, the copy is done, if 0 nothing is done.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe448_load_cond(struct fe448* out, const struct fe448* in, uint32_t flag);

/**
 * Select field element.
 *
 * \param out The element to write the result to.
 * \param in1 The element to read if flag is 1
 * \param in0 The element to read if flag is 0
 * \param flag The elment to read.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe448_select(struct fe448* out, const struct fe448* in1, const struct fe448* in0, uint32_t flag);

/**
 * Conditional swap.
 *
 * \param x0 The elements to swap
 * \param x1 The elements to swap
 * \param flag If 1, swap, if 0 do nothing.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe448_cswap(struct fe448* x0, struct fe448* x1, uint32_t flag);

extern const struct ecclib_field field_448;

#ifdef __cplusplus
}
#endif

#endif
