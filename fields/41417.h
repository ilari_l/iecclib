#ifndef _ecclib_field_41417_h_included_
#define _ecclib_field_41417_h_included_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include "utils2.h"
#include "libpfx.h"
#include "libpfx.h"

struct fe41417
{
#ifdef USE_64BIT_MATH
	uint64_t x[8];
#else
#define DEFAULT_F41417
	uint32_t x[14];
#endif
};

#define NAME_field_41417 "2^414-17"
#define FE41417_BITS 414
#define FE41417_STORE_SIZE BITS_TO_OCTETS(FE41417_BITS)
#define FE41417_ALIGN_POWER 4

#define field_41417 FAKE_NOEXPORT(field_41417)
#define fe41417_add FAKE_NOEXPORT(fe41417_add)
#define fe41417_check FAKE_NOEXPORT(fe41417_check)
#define fe41417_cswap FAKE_NOEXPORT(fe41417_cswap)
#define fe41417_inv FAKE_NOEXPORT(fe41417_inv)
#define fe41417_load FAKE_NOEXPORT(fe41417_load)
#define fe41417_load_cond FAKE_NOEXPORT(fe41417_load_cond)
#define fe41417_load_small FAKE_NOEXPORT(fe41417_load_small)
#define fe41417_mul FAKE_NOEXPORT(fe41417_mul)
#define fe41417_neg FAKE_NOEXPORT(fe41417_neg)
#define fe41417_select FAKE_NOEXPORT(fe41417_select)
#define fe41417_smul FAKE_NOEXPORT(fe41417_smul)
#define fe41417_sqr FAKE_NOEXPORT(fe41417_sqr)
#define fe41417_sqrt FAKE_NOEXPORT(fe41417_sqrt)
#define fe41417_store FAKE_NOEXPORT(fe41417_store)
#define fe41417_sub FAKE_NOEXPORT(fe41417_sub)
#define fe41417_zero FAKE_NOEXPORT(fe41417_zero)

/**
 * Initialize field element to zero.
 *
 * \param out The element.
 */
void fe41417_zero(struct fe41417* out);

/**
 * Initialize field element to a small value.
 *
 * \param out The element.
 * \param num The value.
 */
void fe41417_load_small(struct fe41417* out, uint32_t num);

/**
 * Initialize field element to given value.
 *
 * \param out The element.
 * \param num The value. Given as base-256-little-endian, 32 octets.
 */
void fe41417_load(struct fe41417* out, const uint8_t* num);

/**
 * Check if value is smaller than 2^255-19.
 *
 * \param num The number to check. Given as base-256-littlen-endian, 32 octets.
 * \returns 0 if smaller, -1 if not.
 */
int fe41417_check(const uint8_t* num);						//-1 if outside range.

/**
 * Store field element into octet array.
 *
 * \param out The buffer to store to, 32 octets.
 * \param elem The element to store.
 */
void fe41417_store(uint8_t* out, const struct fe41417* elem);

/**
 * Add two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to add.
 * \param in2 The second element to add.
 */
void fe41417_add(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in2);

/**
 * Substract two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to substract.
 * \param in2 The second element to substract.
 */
void fe41417_sub(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in2);

/**
 * Multiply two field elements.
 *
 * \param out The element to write the result to.
 * \param in1 The first element to multiply.
 * \param in2 The second element to multiply.
 */
void fe41417_mul(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in2);

/**
 * Multiply field element by small constant.
 *
 * \param out The element to write the result to.
 * \param in1 The element to multiply.
 * \param in2 The multiplier.
 */
void fe41417_smul(struct fe41417* out, const struct fe41417* in1, uint32_t in2);

/**
 * Negate a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to negate.
 */
void fe41417_neg(struct fe41417* out, const struct fe41417* in);

/**
 * Square a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to square.
 * \note This is the same as multiplying element by itself, but faster.
 */
void fe41417_sqr(struct fe41417* out, const struct fe41417* in);

/**
 * Square root of a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to take square root of.
 * \returns 0 on success, -1 if no square root exists.
 */
int fe41417_sqrt(struct fe41417* out, const struct fe41417* in);		//-1 on failure.

/**
 * Invert a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to invert.
 */
void fe41417_inv(struct fe41417* out, const struct fe41417* in);

/**
 * Conditionally load a field element.
 *
 * \param out The element to write the result to.
 * \param in The element to read.
 * \param flag If 1, the copy is done, if 0 nothing is done.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe41417_load_cond(struct fe41417* out, const struct fe41417* in, uint32_t flag);

/**
 * Select field element.
 *
 * \param out The element to write the result to.
 * \param in1 The element to read if flag is 1
 * \param in0 The element to read if flag is 0
 * \param flag The elment to read.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe41417_select(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in0, uint32_t flag);

/**
 * Conditional swap.
 *
 * \param x0 The elements to swap
 * \param x1 The elements to swap
 * \param flag If 1, swap, if 0 do nothing.
 * \note If flag isn't 0 nor 1, behaviour is undefined.
 */
void fe41417_cswap(struct fe41417* x0, struct fe41417* x1, uint32_t flag);

extern const struct ecclib_field field_41417;

#ifdef __cplusplus
}
#endif


#endif
