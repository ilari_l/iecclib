#include "448.h"
#include "utils2.h"
typedef struct fe448 field_t;
#define FIELD_NAME field_448
#define FIELD_REAL_NAME NAME_field_448
#define FIELD_BITS FE448_BITS
#define FIELD_DEGREE 1
#define STORAGE_OCTETS FE448_STORE_SIZE
#define FIELD_DEGREE_STRIDE STORAGE_OCTETS
#define FIELD_DEGREE_BITS FIELD_BITS
#define FIELD_ALIGN_POWER FE448_ALIGN_POWER
#define ELEMENT_SIZE (sizeof(struct fe448) + (1 << FIELD_ALIGN_POWER))
#define FIELD_ZERO(X) fe448_zero(X)
#define FIELD_CHECK(X) fe448_check(X)
#define FIELD_LOAD(X, Y) fe448_load(X, Y)
#define FIELD_STORE(X, Y) fe448_store(X, Y)
#define FIELD_SQR(X, Y) fe448_sqr(X, Y)
#define FIELD_INV(X, Y) fe448_inv(X, Y)
#define FIELD_NEG(X, Y) fe448_neg(X, Y)
#define FIELD_SQRT(X, Y) fe448_sqrt(X, Y)
#define FIELD_LOAD_SMALL(X, Y) fe448_load_small(X, Y)
#define FIELD_ADD(X, Y, Z) fe448_add(X, Y, Z)
#define FIELD_SUB(X, Y, Z) fe448_sub(X, Y, Z)
#define FIELD_MUL(X, Y, Z) fe448_mul(X, Y, Z)
#define FIELD_SMUL(X, Y, Z) fe448_smul(X, Y, Z)
#define FIELD_CSWAP(X, Y, Z) fe448_cswap(X, Y, Z)
#define FIELD_LOAD_COND(X, Y, Z) fe448_load_cond(X, Y, Z)
#define FIELD_SELECT(X, Y, Z, W) fe448_select(X, Y, Z, W)
#define FIELD_STORE_SIZE BITS_TO_OCTETS(FIELD_BITS)
