#include "25519.h"
#include "utils2.h"
typedef struct fe25519 field_t;
#define FIELD_NAME field_25519
#define FIELD_REAL_NAME NAME_field_25519
#define FIELD_BITS FE25519_BITS
#define FIELD_DEGREE 1
#define STORAGE_OCTETS FE25519_STORE_SIZE
#define FIELD_DEGREE_STRIDE STORAGE_OCTETS
#define FIELD_DEGREE_BITS FIELD_BITS
#define FIELD_ALIGN_POWER FE25519_ALIGN_POWER
#define ELEMENT_SIZE (sizeof(struct fe25519) + (1 << FIELD_ALIGN_POWER))
#define FIELD_ZERO(X) fe25519_zero(X)
#define FIELD_CHECK(X) fe25519_check(X)
#define FIELD_LOAD(X, Y) fe25519_load(X, Y)
#define FIELD_STORE(X, Y) fe25519_store(X, Y)
#define FIELD_SQR(X, Y) fe25519_sqr(X, Y)
#define FIELD_INV(X, Y) fe25519_inv(X, Y)
#define FIELD_NEG(X, Y) fe25519_neg(X, Y)
#define FIELD_SQRT(X, Y) fe25519_sqrt(X, Y)
#define FIELD_LOAD_SMALL(X, Y) fe25519_load_small(X, Y)
#define FIELD_ADD(X, Y, Z) fe25519_add(X, Y, Z)
#define FIELD_SUB(X, Y, Z) fe25519_sub(X, Y, Z)
#define FIELD_MUL(X, Y, Z) fe25519_mul(X, Y, Z)
#define FIELD_SMUL(X, Y, Z) fe25519_smul(X, Y, Z)
#define FIELD_CSWAP(X, Y, Z) fe25519_cswap(X, Y, Z)
#define FIELD_LOAD_COND(X, Y, Z) fe25519_load_cond(X, Y, Z)
#define FIELD_SELECT(X, Y, Z, W) fe25519_select(X, Y, Z, W)
#define FIELD_STORE_SIZE BITS_TO_OCTETS(FIELD_BITS)
