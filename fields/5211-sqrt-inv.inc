//The modulus as bytes
static const uint8_t modulus[66] = {
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0x01
};

int fe5211_check(const uint8_t* num)
{
	uint8_t lt = 0, gt = 0;
	for(unsigned i = 65; i < 66; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (num[i] < modulus[i]);
		gt |= neutral & (num[i] > modulus[i]);
	}
	return (int)lt;
}

static void fe5211_sqrn(struct fe5211* out, const struct fe5211* in, unsigned count)
{
	struct fe5211 tmp1, tmp2;
	if(count % 2 == 0) {
		fe5211_sqr(&tmp1, in);
		for(unsigned i = 0; i < (count-2)/2; i++) {
			fe5211_sqr(&tmp2, &tmp1);
			fe5211_sqr(&tmp1, &tmp2);
		}
		fe5211_sqr(out, &tmp1);
	} else {
		fe5211_sqr(&tmp1, in);
		fe5211_sqr(&tmp2, &tmp1);
		for(unsigned i = 0; i < (count-3)/2; i++) {
			fe5211_sqr(&tmp1, &tmp2);
			fe5211_sqr(&tmp2, &tmp1);
		}
		fe5211_sqr(out, &tmp2);
	}
}

int fe5211_sqrt(struct fe5211* out, const struct fe5211* in)
{
	//Raise to power of 2^519 (g)
	struct fe5211 r, g;
	fe5211_sqrn(&g, in, 519);	//candidate root.
	uint8_t check1[66];
	uint8_t check2[66];
	fe5211_sqr(&r, &g);		//check value.
	fe5211_store(check1, in);
	fe5211_store(check2, &r);
	uint8_t syndrome = 0;
	for(unsigned i = 0; i < 52; i++)
		syndrome |= (check1[i] ^ check2[i]);
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	fe5211_load_cond(out, &g, syndrome ^ 1);
	return 1-(int)syndrome;
}

void fe5211_inv(struct fe5211* out, const struct fe5211* in)
{
	//2^521-3=512*(2^512-1)+509
	struct fe5211 r, y, g, b;
	fe5211_sqr(&y, in);
	fe5211_mul(&r, &y, in);
	fe5211_sqr(&y, &r);
	fe5211_mul(&r, &y, in);
	fe5211_sqr(&g, &r);
	fe5211_mul(&b, &g, in);
	fe5211_sqrn(&y, &b, 3);
	fe5211_mul(&g, &y, &r);
	fe5211_sqrn(&y, &b, 4);
	fe5211_mul(&r, &y, &b);
	fe5211_sqr(&y, &g);
	fe5211_mul(&g, &r, &y);
	fe5211_sqrn(&b, &r, 8);
	fe5211_mul(&y, &b, &r);
	fe5211_sqrn(&r, &y, 16);
	fe5211_mul(&b, &r, &y);
	fe5211_sqrn(&r, &b, 32);
	fe5211_mul(&y, &r, &b);
	fe5211_sqrn(&r, &y, 64);
	fe5211_mul(&b, &r, &y);
	fe5211_sqrn(&r, &b, 128);
	fe5211_mul(&y, &r, &b);
	fe5211_sqrn(&r, &y, 256);
	fe5211_mul(&b, &r, &y);
	fe5211_sqrn(&r, &b, 9);
	fe5211_mul(out, &r, &g);
}
