//The modulus as bytes
static const uint8_t modulus[56] = {
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
};

int fe448_check(const uint8_t* num)
{
	uint8_t lt = 0, gt = 0;
	for(unsigned i = 55; i < 56; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (num[i] < modulus[i]);
		gt |= neutral & (num[i] > modulus[i]);
	}
	return (int)lt;
}

static void fe448_sqrn(struct fe448* out, const struct fe448* in, unsigned count)
{
	struct fe448 tmp1, tmp2;
	if(count % 2 == 0) {
		fe448_sqr(&tmp1, in);
		for(unsigned i = 0; i < (count-2)/2; i++) {
			fe448_sqr(&tmp2, &tmp1);
			fe448_sqr(&tmp1, &tmp2);
		}
		fe448_sqr(out, &tmp1);
	} else {
		fe448_sqr(&tmp1, in);
		fe448_sqr(&tmp2, &tmp1);
		for(unsigned i = 0; i < (count-3)/2; i++) {
			fe448_sqr(&tmp1, &tmp2);
			fe448_sqr(&tmp2, &tmp1);
		}
		fe448_sqr(out, &tmp2);
	}
}

int fe448_sqrt(struct fe448* out, const struct fe448* in)
{
	//Raise to power of 2^446-2^222
	struct fe448 x0, x1, x2;
	uint8_t check1[56];
	uint8_t check2[56];
	fe448_sqr(&x1, in);		//2^2-2=2
	fe448_mul(&x0, &x1, in);	//2^2-1
	fe448_sqr(&x1, &x0);		//2^3-2
	fe448_mul(&x2, &x1, in);	//2^3-1
	fe448_sqrn(&x1, &x2, 3);	//2^6-2^3
	fe448_mul(&x0, &x1, &x2);	//2^6-1
	fe448_sqr(&x1, &x0);		//2^7-2
	fe448_mul(&x0, &x1, in);	//2^7-1
	fe448_sqrn(&x2, &x0, 7);	//2^14-2^7
	fe448_mul(&x1, &x2, &x0);	//2^14-1
	fe448_sqrn(&x2, &x1, 14);	//2^28-2^14
	fe448_mul(&x0, &x2, &x1);	//2^28-1
	fe448_sqrn(&x2, &x0, 28);	//2^56-2^28
	fe448_mul(&x1, &x2, &x0);	//2^56-1
	fe448_sqrn(&x0, &x1, 56);	//2^112-2^56
	fe448_mul(&x2, &x0, &x1);	//2^112-1
	fe448_sqrn(&x0, &x2, 112);	//2^224-2^112
	fe448_mul(&x1, &x0, &x2);	//2^224-1
	fe448_sqrn(&x0, &x1, 222);	//2^446-2^222
	fe448_sqr(&x1, &x0);		//Check.
	fe448_store(check1, in);
	fe448_store(check2, &x1);
	uint8_t syndrome = 0;
	for(unsigned i = 0; i < 56; i++)
		syndrome |= (check1[i] ^ check2[i]);
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	fe448_load_cond(out, &x0, syndrome ^ 1);
	return 1-(int)syndrome;
}

void fe448_inv(struct fe448* out, const struct fe448* in)
{
	//Raise to power of 2^448-2^224-3.
	struct fe448 x0, x1, x2;
	fe448_sqr(&x1, in);		//2^2-2=2
	fe448_mul(&x0, &x1, in);	//2^2-1
	fe448_sqr(&x1, &x0);		//2^3-2
	fe448_mul(&x0, &x1, in);	//2^3-1
	fe448_sqrn(&x1, &x0, 3);	//2^6-2^3
	fe448_mul(&x2, &x1, &x0);	//2^6-1
	fe448_sqrn(&x0, &x2, 6);	//2^12-2^6
	fe448_mul(&x1, &x0, &x2);	//2^12-1
	fe448_sqr(&x0, &x1);		//2^13-2
	fe448_mul(&x1, &x0, in);	//2^13-1
	fe448_sqrn(&x2, &x1, 13);	//2^26-2^13
	fe448_mul(&x0, &x2, &x1);	//2^26-1
	fe448_sqr(&x1, &x0);		//2^27-2
	fe448_mul(&x0, &x1, in);	//2^27-1
	fe448_sqrn(&x2, &x0, 27);	//2^54-2^27
	fe448_mul(&x1, &x2, &x0);	//2^54-1
	fe448_sqr(&x0, &x1);		//2^55-2
	fe448_mul(&x2, &x0, in);	//2^55-1
	fe448_sqrn(&x0, &x2, 55);	//2^110-2^55
	fe448_mul(&x1, &x0, &x2);	//2^110-1
	fe448_sqr(&x0, &x1);		//2^111-2
	fe448_mul(&x2, &x0, in);	//2^111-1
	fe448_sqrn(&x1, &x2, 111);	//2^222-2^111
	fe448_mul(&x0, &x1, &x2);	//2^222-1
	fe448_sqr(&x2, &x0);		//2^223-2
	fe448_mul(&x1, &x2, in);	//2^223-1
	fe448_sqrn(&x2, &x1, 223);	//2^446-2^223
	fe448_mul(&x1, &x2, &x0);	//2^446-2^222-1
	fe448_sqrn(&x0, &x1, 2);	//2^448-2^224-4
	fe448_mul(out, &x0, in);	//2^448-2^224-3
}
