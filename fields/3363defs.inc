#include "3363.h"
#include "utils2.h"
typedef struct fe3363 field_t;
#define FIELD_NAME field_3363
#define FIELD_REAL_NAME NAME_field_3363
#define FIELD_BITS FE3363_BITS
#define FIELD_DEGREE 1
#define STORAGE_OCTETS FE3363_STORE_SIZE
#define FIELD_DEGREE_STRIDE STORAGE_OCTETS
#define FIELD_DEGREE_BITS FIELD_BITS
#define FIELD_ALIGN_POWER FE3363_ALIGN_POWER
#define ELEMENT_SIZE (sizeof(struct fe3363) + (1 << FIELD_ALIGN_POWER))
#define FIELD_ZERO(X) fe3363_zero(X)
#define FIELD_CHECK(X) fe3363_check(X)
#define FIELD_LOAD(X, Y) fe3363_load(X, Y)
#define FIELD_STORE(X, Y) fe3363_store(X, Y)
#define FIELD_SQR(X, Y) fe3363_sqr(X, Y)
#define FIELD_INV(X, Y) fe3363_inv(X, Y)
#define FIELD_NEG(X, Y) fe3363_neg(X, Y)
#define FIELD_SQRT(X, Y) fe3363_sqrt(X, Y)
#define FIELD_LOAD_SMALL(X, Y) fe3363_load_small(X, Y)
#define FIELD_ADD(X, Y, Z) fe3363_add(X, Y, Z)
#define FIELD_SUB(X, Y, Z) fe3363_sub(X, Y, Z)
#define FIELD_MUL(X, Y, Z) fe3363_mul(X, Y, Z)
#define FIELD_SMUL(X, Y, Z) fe3363_smul(X, Y, Z)
#define FIELD_CSWAP(X, Y, Z) fe3363_cswap(X, Y, Z)
#define FIELD_LOAD_COND(X, Y, Z) fe3363_load_cond(X, Y, Z)
#define FIELD_SELECT(X, Y, Z, W) fe3363_select(X, Y, Z, W)
#define FIELD_STORE_SIZE BITS_TO_OCTETS(FIELD_BITS)
