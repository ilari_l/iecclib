#include "41417.h"
#include "utils2.h"
typedef struct fe41417 field_t;
#define FIELD_NAME field_41417
#define FIELD_REAL_NAME NAME_field_41417
#define FIELD_BITS FE41417_BITS
#define FIELD_DEGREE 1
#define STORAGE_OCTETS FE41417_STORE_SIZE
#define FIELD_DEGREE_STRIDE STORAGE_OCTETS
#define FIELD_DEGREE_BITS FIELD_BITS
#define FIELD_ALIGN_POWER FE41417_ALIGN_POWER
#define ELEMENT_SIZE (sizeof(struct fe41417) + (1 << FIELD_ALIGN_POWER))
#define FIELD_ZERO(X) fe41417_zero(X)
#define FIELD_CHECK(X) fe41417_check(X)
#define FIELD_LOAD(X, Y) fe41417_load(X, Y)
#define FIELD_STORE(X, Y) fe41417_store(X, Y)
#define FIELD_SQR(X, Y) fe41417_sqr(X, Y)
#define FIELD_INV(X, Y) fe41417_inv(X, Y)
#define FIELD_NEG(X, Y) fe41417_neg(X, Y)
#define FIELD_SQRT(X, Y) fe41417_sqrt(X, Y)
#define FIELD_LOAD_SMALL(X, Y) fe41417_load_small(X, Y)
#define FIELD_ADD(X, Y, Z) fe41417_add(X, Y, Z)
#define FIELD_SUB(X, Y, Z) fe41417_sub(X, Y, Z)
#define FIELD_MUL(X, Y, Z) fe41417_mul(X, Y, Z)
#define FIELD_SMUL(X, Y, Z) fe41417_smul(X, Y, Z)
#define FIELD_CSWAP(X, Y, Z) fe41417_cswap(X, Y, Z)
#define FIELD_LOAD_COND(X, Y, Z) fe41417_load_cond(X, Y, Z)
#define FIELD_SELECT(X, Y, Z, W) fe41417_select(X, Y, Z, W)
#define FIELD_STORE_SIZE BITS_TO_OCTETS(FIELD_BITS)
