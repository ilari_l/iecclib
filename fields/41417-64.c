#include "41417.h"
#include "iecclib.h"
#ifdef USE_64BIT_MATH

static const uint64_t emask = (1ULL << 52) - 1;
static const unsigned eshift = 52;
static const uint64_t fconst = 68;
static const uint64_t rfconst = 17;

//The modulus as words
static const uint64_t modulus64[8] = {
	0xFFFFFFFFFFFEF, 0xFFFFFFFFFFFFF, 0xFFFFFFFFFFFFF, 0xFFFFFFFFFFFFF,
	0xFFFFFFFFFFFFF, 0xFFFFFFFFFFFFF, 0xFFFFFFFFFFFFF, 0x3FFFFFFFFFFFF
};

void fe41417_load(struct fe41417* out, const uint8_t* num)
{
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 8; i++) {
		while(shift < eshift) {
			if(idx == 52) break;
			uint64_t byte = num[idx];
			tmp = tmp + (byte << shift);
			idx++;
			shift += 8;
		}
		out->x[i] = tmp & emask;
		shift -= eshift;
		tmp >>= eshift;
	}
	out->x[7] &= (emask >> 2);
}

static void reduce(uint64_t out[8], const uint64_t in[8])
{
	uint64_t carry = rfconst * (in[7] >> (eshift - 2));
	for(unsigned i = 0; i < 8; i++) out[i] = in[i];
	out[7] = in[7] & (emask >> 2);
	for(unsigned i = 0; i < 8; i++) {
		carry = out[i] + carry;
		out[i] = carry & emask;
		carry >>= eshift;
	}
	out[7] = out[7] + (carry << eshift);
	uint64_t gt = 0;
	uint64_t lt = 0;
	uint64_t s[8] = {0};
	for(unsigned i = 7; i < 8; i--) {
		uint64_t neutral = (gt ^ 1) & (lt ^ 1);
		gt |= neutral & (out[i] > modulus64[i]);
		lt |= neutral & (out[i] < modulus64[i]);
	}
	uint64_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 8; i++) s[i] = modulus64[i] & mask;
	uint64_t borrow = 0;
	for(unsigned i = 0; i < 8; i++) {
		out[i] = out[i] - s[i] - borrow;
		borrow = out[i] >> 63;
		out[i] &= emask;
	}
}

void fe41417_store(uint8_t* out, const struct fe41417* elem)
{
	uint64_t y[8];
	reduce(y, elem->x);
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 8; i++) {
		tmp = tmp + (y[i] << shift);
		shift = shift + eshift;
		while(shift >= 8) {
			if(idx >= 52) return;
			out[idx] = tmp;
			idx++;
			tmp >>= 8;
			shift -= 8;
		}
	}
}

void fe41417_zero(struct fe41417* out)
{
	for(unsigned i = 0; i < 8; i++) out->x[i] = 0;
}

void fe41417_load_small(struct fe41417* out, uint32_t num)
{
	for(unsigned i = 0; i < 8; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe41417_neg(struct fe41417* out, const struct fe41417* in)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) - in->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 8; i++) {
		uint64_t r = 4 * emask - in->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe41417_sub(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in2)
{
	uint64_t carry;
	{
		uint64_t r = (4 * emask - 4 * fconst + 4) + in1->x[0] - in2->x[0];
		out->x[0] = r & emask;
		carry = r >> eshift;
	}
	for(unsigned i = 1; i < 8; i++) {
		uint64_t r = 4 * emask + in1->x[i] - in2->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += fconst * carry;
}

void fe41417_add(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in2)
{
	uint64_t carry = 0;
	for(unsigned i = 0; i < 8; i++) {
		carry = in1->x[i] + in2->x[i] + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	out->x[0] += fconst * carry;
}

#define WM(X, Y) ((__uint128_t)(X) * (__uint128_t)(Y))

void fe41417_smul(struct fe41417* out, const struct fe41417* in1, uint32_t in2)
{
	__uint128_t carry = 0, carry2 = 0;
	for(unsigned i = 0; i < 8; i++) {
		carry = WM(in1->x[i], in2) + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	carry2 = out->x[0] + fconst * carry;
	out->x[0] = carry2 & emask;
	carry2 >>= eshift;
	out->x[1] += carry2;
}

void fe41417_mul(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in2)
{
	__uint128_t W=0;
	uint64_t a0 = in1->x[0], a1 = in1->x[1], a2 = in1->x[2], a3 = in1->x[3], a4 = in1->x[4], a5 = in1->x[5],
		a6 = in1->x[6], a7 = in1->x[7];
	uint64_t b0 = in2->x[0], b1 = in2->x[1], b2 = in2->x[2], b3 = in2->x[3], b4 = in2->x[4], b5 = in2->x[5],
		b6 = in2->x[6], b7 = in2->x[7];
	uint64_t c1 = (b1 << 6) + (b1 << 2), c2 = (b2 << 6) + (b2 << 2), c3 = (b3 << 6) + (b3 << 2),
		c4 = (b4 << 6) + (b4 << 2), c5 = (b5 << 6) + (b5 << 2), c6 = (b6 << 6) + (b6 << 2),
		c7 = (b7 << 6) + (b7 << 2);

	W = W + WM(a0, b0) + WM(a1, c7) + WM(a2, c6) + WM(a3, c5) + WM(a4, c4) + WM(a5, c3) + WM(a6, c2) + WM(a7, c1);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(a0, b1) + WM(a1, b0) + WM(a2, c7) + WM(a3, c6) + WM(a4, c5) + WM(a5, c4) + WM(a6, c3) + WM(a7, c2);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(a0, b2) + WM(a1, b1) + WM(a2, b0) + WM(a3, c7) + WM(a4, c6) + WM(a5, c5) + WM(a6, c4) + WM(a7, c3);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(a0, b3) + WM(a1, b2) + WM(a2, b1) + WM(a3, b0) + WM(a4, c7) + WM(a5, c6) + WM(a6, c5) + WM(a7, c4);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(a0, b4) + WM(a1, b3) + WM(a2, b2) + WM(a3, b1) + WM(a4, b0) + WM(a5, c7) + WM(a6, c6) + WM(a7, c5);
	out->x[4] = W & emask; W >>= eshift;
	W = W + WM(a0, b5) + WM(a1, b4) + WM(a2, b3) + WM(a3, b2) + WM(a4, b1) + WM(a5, b0) + WM(a6, c7) + WM(a7, c6);
	out->x[5] = W & emask; W >>= eshift;
	W = W + WM(a0, b6) + WM(a1, b5) + WM(a2, b4) + WM(a3, b3) + WM(a4, b2) + WM(a5, b1) + WM(a6, b0) + WM(a7, c7);
	out->x[6] = W & emask; W >>= eshift;
	W = W + WM(a0, b7) + WM(a1, b6) + WM(a2, b5) + WM(a3, b4) + WM(a4, b3) + WM(a5, b2) + WM(a6, b1) + WM(a7, b0);
	out->x[7] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask;
	W >>= eshift;
	out->x[1] += W;
}

void fe41417_sqr(struct fe41417* out, const struct fe41417* in)
{
	__uint128_t W=0;

	uint64_t a0 = in->x[0], a1 = in->x[1], a2 = in->x[2], a3 = in->x[3], a4 = in->x[4], a5 = in->x[5],
		a6 = in->x[6], a7 = in->x[7];
	uint64_t b4 = a4 * fconst, b5 = a5 * fconst, b6 = a6 * fconst, b7 = a7 * fconst;
	uint64_t c0 = a0 << 1, c1 = a1 << 1, c2 = a2 << 1, c3 = a3 << 1, c4 = a4 << 1, c5 = a5 << 1,
		c6 = a6 << 1;

	W = W + WM(a0, a0) + WM(c1, b7) + WM(c2, b6) + WM(c3, b5) + WM(a4, b4);
	out->x[0] = W & emask; W >>= eshift;
	W = W + WM(c0, a1) + WM(c2, b7) + WM(c3, b6) + WM(c4, b5);
	out->x[1] = W & emask; W >>= eshift;
	W = W + WM(c0, a2) + WM(a1, a1) + WM(c3, b7) + WM(c4, b6) + WM(a5, b5);
	out->x[2] = W & emask; W >>= eshift;
	W = W + WM(c0, a3) + WM(c1, a2) + WM(c4, b7) + WM(c5, b6);
	out->x[3] = W & emask; W >>= eshift;
	W = W + WM(c0, a4) + WM(c1, a3) + WM(a2, a2) + WM(c5, b7) + WM(a6, b6);
	out->x[4] = W & emask; W >>= eshift;
	W = W + WM(c0, a5) + WM(c1, a4) + WM(c2, a3) + WM(c6, b7);
	out->x[5] = W & emask; W >>= eshift;
	W = W + WM(c0, a6) + WM(c1, a5) + WM(c2, a4) + WM(a3, a3) + WM(a7, b7);
	out->x[6] = W & emask; W >>= eshift;
	W = W + WM(c0, a7) + WM(c1, a6) + WM(c2, a5) + WM(c3, a4);
	out->x[7] = W & emask; W >>= eshift;

	W = out->x[0] + fconst * W;
	out->x[0] = W & emask;
	W >>= eshift;
	out->x[1] += W;
}

void fe41417_load_cond(struct fe41417* out, const struct fe41417* in, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 8; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe41417_select(struct fe41417* out, const struct fe41417* in1, const struct fe41417* in0, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 8; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe41417_cswap(struct fe41417* x0, struct fe41417* x1, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	for(unsigned i = 0; i < 8; i++) {
		uint64_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif
