#include "448.h"
#include "iecclib.h"
#ifdef USE_64BIT_MATH

static const uint64_t emask = (1ULL << 56) - 1;
static const unsigned eshift = 56;

//The modulus as words
static const uint64_t modulus64[8] = {
	0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF,
	0xFFFFFFFFFFFFFE, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFF
};

void fe448_load(struct fe448* out, const uint8_t* num)
{
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 8; i++) {
		while(shift < eshift) {
			if(idx == 56) break;
			uint64_t byte = num[idx];
			tmp = tmp + (byte << shift);
			idx++;
			shift += 8;
		}
		out->x[i] = tmp & emask;
		shift -= eshift;
		tmp >>= eshift;
	}
}

static void reduce(uint64_t out[8], const uint64_t in[8])
{
	uint64_t carry = (in[7] >> eshift);
	for(unsigned i = 0; i < 8; i++) out[i] = in[i];
	out[7] = in[7] & emask;
	for(unsigned i = 0; i < 8; i++) {
		carry = out[i] + carry;
		out[i] = carry & emask;
		carry >>= eshift;
	}
	out[7] = out[7] + (carry << eshift);
	uint64_t gt = 0;
	uint64_t lt = 0;
	uint64_t s[8] = {0};
	for(unsigned i = 7; i < 8; i--) {
		uint64_t neutral = (gt ^ 1) & (lt ^ 1);
		gt |= neutral & (out[i] > modulus64[i]);
		lt |= neutral & (out[i] < modulus64[i]);
	}
	uint64_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 8; i++) s[i] = modulus64[i] & mask;
	uint64_t borrow = 0;
	for(unsigned i = 0; i < 8; i++) {
		out[i] = out[i] - s[i] - borrow;
		borrow = out[i] >> 63;
		out[i] &= emask;
	}
}

void fe448_store(uint8_t* out, const struct fe448* elem)
{
	uint64_t y[8];
	reduce(y, elem->x);
	uint64_t tmp = 0;
	unsigned shift = 0;
	unsigned idx = 0;
	for(unsigned i = 0; i < 8; i++) {
		tmp = tmp + (y[i] << shift);
		shift = shift + eshift;
		while(shift >= 8) {
			if(idx >= 56) return;
			out[idx] = tmp;
			idx++;
			tmp >>= 8;
			shift -= 8;
		}
	}
}

void fe448_zero(struct fe448* out)
{
	for(unsigned i = 0; i < 8; i++) out->x[i] = 0;
}

void fe448_load_small(struct fe448* out, uint32_t num)
{
	for(unsigned i = 0; i < 8; i++) out->x[i] = 0;
	out->x[0] = num;
}

void fe448_neg(struct fe448* out, const struct fe448* in)
{
	uint64_t carry = 0;
	for(unsigned i = 0; i < 8; i++) {
		uint64_t base = emask - (i == 4);
		uint64_t r = 4 * base - in->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += carry;
	out->x[4] += carry;
}

void fe448_sub(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint64_t carry = 0;
	for(unsigned i = 0; i < 8; i++) {
		uint64_t base = emask - (i == 4);
		uint64_t r = 4 * base + in1->x[i] - in2->x[i] + carry;
		out->x[i] = r & emask;
		carry = r >> eshift;
	}
	out->x[0] += carry;
	out->x[4] += carry;
}

void fe448_add(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	uint64_t carry = 0;
	for(unsigned i = 0; i < 8; i++) {
		carry = in1->x[i] + in2->x[i] + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	out->x[0] += carry;
	out->x[4] += carry;
}

#define WM(X, Y) ((__uint128_t)(X) * (__uint128_t)(Y))

void fe448_smul(struct fe448* out, const struct fe448* in1, uint32_t in2)
{
	__uint128_t carry = 0, carry2 = 0;
	for(unsigned i = 0; i < 8; i++) {
		carry = WM(in1->x[i], in2) + carry;
		out->x[i] = carry & emask;
		carry >>= eshift;
	}
	carry2 = out->x[0] + carry;
	out->x[0] = carry2 & emask;
	carry2 >>= eshift;
	out->x[1] += carry2;
	carry2 = out->x[4] + carry;
	out->x[4] = carry2 & emask;
	carry2 >>= eshift;
	out->x[5] += carry2;
}

void fe448_mul(struct fe448* out, const struct fe448* in1, const struct fe448* in2)
{
	//FIXME: Use Karatsuba
	__uint128_t w = 0;
	uint64_t a0 = in1->x[0], a1 = in1->x[1], a2 = in1->x[2], a3 = in1->x[3], a4 = in1->x[4], a5 = in1->x[5],
		a6 = in1->x[6], a7 = in1->x[7];
	uint64_t b0 = in2->x[0], b1 = in2->x[1], b2 = in2->x[2], b3 = in2->x[3], b4 = in2->x[4], b5 = in2->x[5],
		b6 = in2->x[6], b7 = in2->x[7];
	uint64_t c0 = b0 + b4, c1 = b1 + b5, c2 = b2 + b6, c3 = b3 + b7;
	uint64_t d1 = b1 + (b5 << 1), d2 = b2 + (b6 << 1), d3 = b3 + (b7 << 1);

	w = w + WM(a0, b0) + WM(a1, b7) + WM(a2, b6) + WM(a3, b5) + WM(a4, b4) + WM(a5, c3) + WM(a6, c2) + WM(a7, c1);
	out->x[0]=w & emask; w = w >> eshift;
	w = w + WM(a0, b1) + WM(a1, b0) + WM(a2, b7) + WM(a3, b6) + WM(a4, b5) + WM(a5, b4) + WM(a6, c3) + WM(a7, c2);
	out->x[1]=w & emask; w = w >> eshift;
	w = w + WM(a0, b2) + WM(a1, b1) + WM(a2, b0) + WM(a3, b7) + WM(a4, b6) + WM(a5, b5) + WM(a6, b4) + WM(a7, c3);
	out->x[2]=w & emask; w = w >> eshift;
	w = w + WM(a0, b3) + WM(a1, b2) + WM(a2, b1) + WM(a3, b0) + WM(a4, b7) + WM(a5, b6) + WM(a6, b5) + WM(a7, b4);
	out->x[3]=w & emask; w = w >> eshift;
	w = w + WM(a0, b4) + WM(a1, c3) + WM(a2, c2) + WM(a3, c1) + WM(a4, c0) + WM(a5, d3) + WM(a6, d2) + WM(a7, d1);
	out->x[4]=w & emask; w = w >> eshift;
	w = w + WM(a0, b5) + WM(a1, b4) + WM(a2, c3) + WM(a3, c2) + WM(a4, c1) + WM(a5, c0) + WM(a6, d3) + WM(a7, d2);
	out->x[5]=w & emask; w = w >> eshift;
	w = w + WM(a0, b6) + WM(a1, b5) + WM(a2, b4) + WM(a3, c3) + WM(a4, c2) + WM(a5, c1) + WM(a6, c0) + WM(a7, d3);
	out->x[6]=w & emask; w = w >> eshift;
	w = w + WM(a0, b7) + WM(a1, b6) + WM(a2, b5) + WM(a3, b4) + WM(a4, c3) + WM(a5, c2) + WM(a6, c1) + WM(a7, c0);
	out->x[7]=w & emask; w = w >> eshift;

	uint64_t carry2;
	out->x[0] = out->x[0] + w;
	carry2 = out->x[0] >> eshift;
	out->x[0] = out->x[0] & emask;
	out->x[1] = out->x[1] + carry2;
	out->x[4] = out->x[4] + w;
	carry2 = out->x[4] >> eshift;
	out->x[4] = out->x[4] & emask;
	out->x[5] = out->x[5] + carry2;
}

void fe448_sqr(struct fe448* out, const struct fe448* in)
{
	__uint128_t w = 0;
	uint64_t a0 = in->x[0], a1 = in->x[1], a2 = in->x[2], a3 = in->x[3], a4 = in->x[4], a5 = in->x[5],
		a6 = in->x[6], a7 = in->x[7];
	uint64_t b1 = a1 + a5, b2 = a2 + a6, b3 = a3 + a7;
	uint64_t c1 = a1 + (a5 << 1), c2 = a2 + (a6 << 1), c3 = a3 + (a7 << 1);
	uint64_t d0 = (a0 << 1) + a4, d1 = (a1 << 1) + a5, d2 = (a2 << 1) + a6, d3 = (a3 << 1) + a7;
	uint64_t e0 = (a0 << 1), e1 = (a1 << 1), e4 = (a4 << 1);

	w=w + WM(a0, a0) + WM(d1, a7) + WM(d2, a6) + WM(d3, a5) + WM(a4, a4);
	out->x[0]=w & emask; w = w >> eshift;
	w=w + WM(e0, a1) + WM(d2, a7) + WM(d3, a6) + WM(e4, a5);
	out->x[1]=w & emask; w = w >> eshift;
	w=w + WM(e0, a2) + WM(a1, a1) + WM(d3, a7) + WM(e4, a6) + WM(a5, a5);
	out->x[2]=w & emask; w = w >> eshift;
	w=w + WM(e0, a3) + WM(e1, a2) + WM(e4, a7) + WM(a5, a6) + WM(a6, a5);
	out->x[3]=w & emask; w = w >> eshift;
	w=w + WM(d0, a4) + WM(a1, b3) + WM(a2, b2) + WM(a3, b1) + WM(a5, c3) + WM(a6, c2) + WM(a7, c1);
	out->x[4]=w & emask; w = w >> eshift;
	w=w + WM(d0, a5) + WM(d1, a4) + WM(a2, b3) + WM(a3, b2) + WM(a6, c3) + WM(a7, c2);
	out->x[5]=w & emask; w = w >> eshift;
	w=w + WM(d0, a6) + WM(d1, a5) +WM(d2, a4) + WM(a3, b3) + WM(a7, c3);
	out->x[6]=w & emask; w = w >> eshift;
	w=w + WM(d0, a7) + WM(d1, a6) +WM(d2, a5) + WM(d3, a4);
	out->x[7]=w & emask; w = w >> eshift;

	uint64_t carry2;
	out->x[0] = out->x[0] + w;
	carry2 = out->x[0] >> eshift;
	out->x[0] = out->x[0] & emask;
	out->x[1] = out->x[1] + carry2;
	out->x[4] = out->x[4] + w;
	carry2 = out->x[4] >> eshift;
	out->x[4] = out->x[4] & emask;
	out->x[5] = out->x[5] + carry2;
}

void fe448_load_cond(struct fe448* out, const struct fe448* in, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 8; i++)
		out->x[i] = (out->x[i] & iflag) | (in->x[i] & rflag);
}

void fe448_select(struct fe448* out, const struct fe448* in1, const struct fe448* in0, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	uint64_t iflag = ~rflag;
	for(unsigned i = 0; i < 8; i++)
		out->x[i] = (in0->x[i] & iflag) | (in1->x[i] & rflag);
}

void fe448_cswap(struct fe448* x0, struct fe448* x1, uint32_t flag)
{
	uint64_t rflag = -(uint64_t)flag;
	for(unsigned i = 0; i < 8; i++) {
		uint64_t t = rflag & (x0->x[i] ^ x1->x[i]);
		x0->x[i] ^= t;
		x1->x[i] ^= t;
	}
}
#endif
