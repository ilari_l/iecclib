//The modulus as bytes
static const uint8_t modulus[42] = {
	0xFD, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
};

int fe3363_check(const uint8_t* num)
{
	uint8_t lt = 0, gt = 0;
	for(unsigned i = 41; i < 42; i--) {
		uint8_t neutral = (1^gt)&(1^lt);
		lt |= neutral & (num[i] < modulus[i]);
		gt |= neutral & (num[i] > modulus[i]);
	}
	return (int)lt;
}

static void fe3363_sqrn(struct fe3363* out, const struct fe3363* in, unsigned count)
{
	struct fe3363 tmp1, tmp2;
	if(count % 2 == 0) {
		fe3363_sqr(&tmp1, in);
		for(unsigned i = 0; i < (count-2)/2; i++) {
			fe3363_sqr(&tmp2, &tmp1);
			fe3363_sqr(&tmp1, &tmp2);
		}
		fe3363_sqr(out, &tmp1);
	} else {
		fe3363_sqr(&tmp1, in);
		fe3363_sqr(&tmp2, &tmp1);
		for(unsigned i = 0; i < (count-3)/2; i++) {
			fe3363_sqr(&tmp1, &tmp2);
			fe3363_sqr(&tmp2, &tmp1);
		}
		fe3363_sqr(out, &tmp2);
	}
}

int fe3363_sqrt(struct fe3363* out, const struct fe3363* in)
{
	//v=(2a)^(2^333-1)
	//i=(2a)v^2
	//r=av(i-1)
	//2^252-3=2^252-4+1=4*(2^250-1)+1
	struct fe3363 r, y, g, b, c, d, o;
	uint8_t check1[42];
	uint8_t check2[42];
	fe3363_load_small(&o, 1);
	fe3363_add(&d, in, in);	//2a

	fe3363_sqr(&r, &d);		//r=2
	fe3363_mul(&g, &r, &d);		//g=3
	fe3363_sqr(&r, &g);		//r=6
	fe3363_mul(&y, &r, &d);		//y=7
	fe3363_sqrn(&r, &y, 2);		//r=28
	fe3363_mul(&b, &r, &g);		//b=2^5-1
	fe3363_sqrn(&r, &b, 5);		//r=2^10-2^5
	fe3363_mul(&y, &r, &b);		//y=2^10-1
	fe3363_sqrn(&r, &y, 10);	//r=2^20-2^10
	fe3363_mul(&b, &r, &y);		//b=2^20-1
	fe3363_sqrn(&r, &b, 20);	//r=2^40-2^20
	fe3363_mul(&y, &r, &b);		//y=2^40-1
	fe3363_sqr(&r, &y);		//r=2^41-2
	fe3363_mul(&b, &r, &d);		//b=2^41-1
	fe3363_sqrn(&r, &b, 41);	//r=2^82-2^41
	fe3363_mul(&y, &r, &b);		//y=2^82-1
	fe3363_sqr(&r, &y);		//r=2^83-2
	fe3363_mul(&b, &r, &d);		//b=2^83-1
	fe3363_sqrn(&y, &b, 83);	//y=2^166-2^83
	fe3363_mul(&r, &y, &b);		//r=2^166-1
	fe3363_sqrn(&b, &r, 166);	//b=2^332-2^166
	fe3363_mul(&y, &b, &r);		//y=2^332-1
	fe3363_sqr(&r, &y);		//r=2^333-2
	fe3363_mul(&y, &r, &d);		//y=2^333-1

	fe3363_sqr(&g, &y);		//v^2
	fe3363_mul(&c, &d, &g);		//(2a)v^2 => c is i
	fe3363_mul(&b, in, &y);		//av
	fe3363_sub(&r, &c, &o);		//i-1
	fe3363_mul(&g, &b, &r);		//candidate root.
	fe3363_sqr(&r, &g);		//check value.
	fe3363_store(check1, in);
	fe3363_store(check2, &r);
	uint8_t syndrome = 0;
	for(unsigned i = 0; i < 42; i++)
		syndrome |= (check1[i] ^ check2[i]);
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	fe3363_load_cond(out, &g, syndrome ^ 1);
	return 1-(int)syndrome;
}

void fe3363_inv(struct fe3363* out, const struct fe3363* in)
{
	//Raise to power of 2^336-5
	struct fe3363 tmp1, tmp2, tmp3, tmp4;
	fe3363_sqr(&tmp1, in);			//tmp1=2
	fe3363_mul(&tmp2, &tmp1, in);		//tmp2=3
	fe3363_sqr(&tmp1, &tmp2);		//tmp1=6
	fe3363_mul(&tmp3, &tmp1, in);		//tmp3=7
	fe3363_sqrn(&tmp1, &tmp3, 2);		//tmp1=28
	fe3363_mul(&tmp4, &tmp1, &tmp2);	//tmp4=2^5-1
	fe3363_sqrn(&tmp1, &tmp4, 5);		//tmp1=2^10-2^5
	fe3363_mul(&tmp3, &tmp1, &tmp4);	//tmp3=2^10-1
	fe3363_sqrn(&tmp1, &tmp3, 10);		//tmp1=2^20-2^10
	fe3363_mul(&tmp4, &tmp1, &tmp3);	//tmp4=2^20-1
	fe3363_sqrn(&tmp1, &tmp4, 20);		//tmp1=2^40-2^20
	fe3363_mul(&tmp3, &tmp1, &tmp4);	//tmp3=2^40-1
	fe3363_sqr(&tmp1, &tmp3);		//tmp1=2^41-2
	fe3363_mul(&tmp4, &tmp1, in);		//tmp4=2^41-1
	fe3363_sqrn(&tmp1, &tmp4, 41);		//tmp1=2^82-2^41
	fe3363_mul(&tmp3, &tmp1, &tmp4);	//tmp3=2^82-1
	fe3363_sqr(&tmp1, &tmp3);		//tmp1=2^83-2
	fe3363_mul(&tmp4, &tmp1, in);		//tmp4=2^83-1
	fe3363_sqrn(&tmp3, &tmp4, 83);		//tmp3=2^166-2^83
	fe3363_mul(&tmp1, &tmp3, &tmp4);	//tmp1=2^166-1
	fe3363_sqrn(&tmp4, &tmp1, 166);		//tmp4=2^332-2^166
	fe3363_mul(&tmp3, &tmp4, &tmp1);	//tmp3=2^332-1
	fe3363_sqr(&tmp1, &tmp3);		//tmp1=2^333-2
	fe3363_mul(&tmp3, &tmp1, in);		//tmp3=2^333-1
	fe3363_sqrn(&tmp1, &tmp3, 3);		//tmp1=2^336-8
	fe3363_mul(out, &tmp1, &tmp2);		//out=2^336-5
}
