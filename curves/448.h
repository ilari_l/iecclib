#ifndef _ecclib_curve448_h_included_
#define _ecclib_curve448_h_included_

#include "iecclib.h"
#include "libpfx.h"

#define NAME_curve448 "curve448"

#define curve448 FAKE_NOEXPORT(curve448)

/**
 * The handle of curve448 function.
 */
extern const struct ecclib_curve curve448;

#endif
