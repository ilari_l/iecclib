#include "iecclib.h"
#include "41417.h"
#include "fields/41417defs.inc"
#include "utils.h"

#define CURVE_NAME NAME_curve41417
#define CLOG2L 411
#define RLOG2L 411
#define CURVE_FIELD field_41417
#define PARTIAL_MAX_BITS 832
#define COFACTOR 8
#define SCALAR_WORDS 13
#define POINT_ALIGN_POWER 4
#define FIELD_BITS FE41417_BITS
#define EDWARDS_CONST 3617
#define MONTGOMERY_CONSTANT -3616
#define POINT_STORE_SIZE (FIELD_BITS + 8) / 8

static field_t base_x;
static field_t base_y;
static field_t const_one;
static volatile unsigned global_init;

static const uint8_t montgomery_base[FIELD_STORE_SIZE] = {
	0x45, 0x70, 0x35, 0xE5, 0x7D, 0x98, 0x70, 0xAC, 0x52, 0x59, 0x4F, 0xDE, 0x87, 0x09, 0xC7, 0x2A,
	0x95, 0xF5, 0xE4, 0x7D, 0x98, 0x70, 0xAC, 0x52, 0x59, 0x4F, 0xDE, 0x87, 0x09, 0xC7, 0x2A, 0x95,
	0xF5, 0xE4, 0x7D, 0x98, 0x70, 0xAC, 0x52, 0x59, 0x4F, 0xDE, 0x87, 0x09, 0xC7, 0x2A, 0x95, 0xF5,
	0xE4, 0x7D, 0x98, 0x30
};

static void do_global_init()
{
	if(!global_init) {
		call_thread_lock();
		static uint8_t _x[FIELD_STORE_SIZE] = {
			0x95, 0xc5, 0xcb, 0xf3, 0x12, 0x38, 0xfd, 0xc4, 0x64, 0x7c, 0x53, 0xa8, 0xfa, 0x73,
			0x1a, 0x30, 0x11, 0xa1, 0x6b, 0x6d, 0x4d, 0xab, 0xa4, 0x98, 0x54, 0xf3, 0x7f, 0xf5,
			0xc7, 0x3e, 0xc0, 0x44, 0x9f, 0x36, 0x46, 0xcd, 0x5f, 0x6e, 0x32, 0x1c, 0x63, 0xc0,
			0x18, 0x02, 0x30, 0x43, 0x14, 0x14, 0x05, 0x49, 0x33, 0x1a
		};
		static uint8_t _y[FIELD_STORE_SIZE] = {
			0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
		};
		FIELD_LOAD(&base_x, _x);
		FIELD_LOAD(&base_y, _y);
		FIELD_LOAD_SMALL(&const_one, 1);
		global_init = 1;
		call_thread_unlock();
	}
}

typedef struct point41417
{
	field_t x;
	field_t y;
	field_t z;
} point_t;

#include "curve-edwards-common.inc"
#include "curve-edwards-normal.inc"

static inline void _point_add(point_t* out, const point_t* in1, const point_t* in2)
{
	field_t A, B, C, D, E, F, G, H;
	FIELD_MUL(&A, &in1->z, &in2->z);
	FIELD_SQR(&B, &A);
	FIELD_MUL(&C, &in1->x, &in2->x);
	FIELD_MUL(&D, &in1->y, &in2->y);
	FIELD_MUL(&F, &C, &D);
	FIELD_SMUL(&E, &F, EDWARDS_CONST);	//Really +3617, no sign flip sign for E.
	FIELD_SUB(&F, &B, &E);
	FIELD_ADD(&G, &B, &E);
	FIELD_ADD(&B, &in1->x, &in1->y);
	FIELD_ADD(&E, &in2->x, &in2->y);
	FIELD_MUL(&H, &B, &E);
	FIELD_SUB(&B, &H, &C);
	FIELD_SUB(&E, &B, &D);
	FIELD_MUL(&B, &F, &E);
	FIELD_MUL(&out->x, &A, &B);
	FIELD_SUB(&B, &D, &C);
	FIELD_MUL(&C, &G, &B);
	FIELD_MUL(&out->y, &A, &C);
	FIELD_MUL(&out->z, &F, &G);
}

static int _point_set(point_t* point, const uint8_t* data)
{
	do_global_init();
	uint8_t _y[FIELD_STORE_SIZE];
	field_t y, A, B, C;
	for(unsigned i = 0; i < FIELD_STORE_SIZE; i++) _y[i] = data[i];
	_y[FIELD_STORE_SIZE - 1] &= 127;

	//Check y in range.
	uint8_t syndrome = 0;
	syndrome = 1 - FIELD_CHECK(_y);

	FIELD_LOAD(&y, _y);
	FIELD_SQR(&A, &y);			//y^2
	FIELD_SUB(&B, &const_one, &A);		//1 - y^2
	FIELD_SMUL(&C, &A, EDWARDS_CONST);	//d*y^2
	FIELD_SUB(&A, &const_one, &C);		//1 - d*y^2
	FIELD_INV(&C, &A);			//1/(1 - d*y^2)
	FIELD_MUL(&A, &B, &C);			//(1 - y^2)/(1 - d*y^2)
	uint32_t flag = FIELD_SQRT(&B, &A);	//sqrt((1 - y^2)/(1 - d*y^2))
	//Now (B,y) is correct up to sign of x if flag is 1.
	FIELD_STORE(_y, &B);
	uint32_t needs_flip = (data[POINT_STORE_SIZE - 1] >> 7) ^ (_y[0] & 1);

	//Check for x=0 and needs_flip, that is illegal.
	for(size_t i = 0; i < FIELD_STORE_SIZE; i++) syndrome |= _y[i];
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	flag &= 1 - ((1 - syndrome) & needs_flip);

	FIELD_NEG(&A, &B);
	//If needs_flip is 1, x coordinate is A, otherwise B.
	FIELD_SELECT(&C, &A, &B, needs_flip);
	FIELD_LOAD_COND(&point->x, &C, flag);
	FIELD_LOAD_COND(&point->y, &y, flag);
	FIELD_LOAD_COND(&point->z, &const_one, flag);
	return flag;
}

static inline int _point_montgomery(field_t* output, const point_t* point)
{
	field_t A, B;
	FIELD_INV(&A, &point->x);
	FIELD_MUL(&B, &point->y, &A);
	FIELD_SQR(output, &B);
	return 1;
}

//7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEB3CC92414CF706022B36F1C0338AD63CF181B0E71A5E106AF79

static const uint32_t high_word_mul[7] = {
	0xDF2A10E0, 0x9E31CB43, 0x53861CFC, 0x1C7F98EA, 0xF3FBA992, 0xDB7D6611, 0x00029866
};

static const uint32_t high_bit_mul[7] = {
	0x1EF95087, 0xE4F18E5A, 0x529C30E7, 0x90E3FCC7, 0x8F9FDD4C, 0x36DBEB30, 0x000014C3
};

static const uint32_t curve_order[13] = {
	0xE106AF79, 0x1B0E71A5, 0xAD63CF18, 0x6F1C0338, 0x706022B3, 0xC92414CF, 0xFFFFEB3C,
	0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x07FFFFFF
};

static void reduce_partial(uint32_t* output, const uint32_t* input)
{
	uint32_t tmp1[20];
	uint32_t tmp2[20];
	uint32_t tmp3[20];
	//First, split to two 416-bit parts, multiply upper and accumulate. The largest possible result for this
	//is 2^626 (20 words). We need to pad the lower words due to addition routine workings.
	ecclib_bigint_mul(tmp1, input + 13, 13, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	for(unsigned i = 0; i < 13; i++) tmp2[i] = input[i];
	for(unsigned i = 13; i < 20; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp3, tmp1, tmp2, 20);
	//Then split the lower 416-bit and upper 210-bit (7 words) parts, multiply upper and accumulate. The largest
	//possible result for this is 2^420 (14 words). Again, lower words need to be padded.
	ecclib_bigint_mul(tmp1, tmp3 + 13, 7, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	for(unsigned i = 0; i < 13; i++) tmp2[i] = tmp3[i];
	for(unsigned i = 13; i < 14; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp3, tmp1, tmp2, 14);
	//Then split the lower 411-bit and upper 9-bit parts, multiply upper and accumulate. The result of this is
	//less than 2^413. Pad multiply result, it is too small otherwise.
	uint32_t xh = (tmp3[12] >> 27) | (tmp3[13] << 5);
	tmp3[12] &= (1UL << 27) - 1;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 13; i++) tmp1[i] = 0;
	ecclib_bigint_add(output, tmp1, tmp3, 13);
}

static void reduce_full(uint8_t* output, const uint32_t* input)
{
	uint32_t tmp1[14];
	uint32_t tmp2[14];
	uint32_t tmp3[14];
	//Split the lower and upper parts for one final multiplicative reduction. The multiply result is padded as it
	//is too small otherwise.
	for(unsigned i = 0; i < 13; i++) tmp3[i] = input[i];
	uint32_t xh = (tmp3[12] >> 27);
	tmp3[12] &= (1UL << 27) - 1;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 14; i++) tmp1[i] = 0;
	ecclib_bigint_add(tmp2, tmp1, tmp3, 13);
	//Now tmp2 is at most twice order. Conditionally substract order.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 12; i < 13; i--) {
		uint32_t neutral = (1 ^ gt) & (1 ^ lt);
		lt |= neutral & (tmp2[i] < curve_order[i]);
		gt |= neutral & (tmp2[i] > curve_order[i]);
	}
	uint32_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 13; i++) tmp3[i] = curve_order[i] & mask;
	ecclib_bigint_sub(tmp1, tmp2, tmp3, 13);
	//Now tmp1 is final reduced value. Convert it to octets.
	ecclib_bigint_store(output, curve41417.scalar_octets, tmp1);
}

const static uint8_t _order[] = {
	0x79, 0xAF, 0x06, 0xE1, 0xA5, 0x71, 0x0E, 0x1B, 0x18, 0xCF, 0x63, 0xAD, 0x38, 0x03, 0x1C, 0x6F,
	0xB3, 0x22, 0x60, 0x70, 0xCF, 0x14, 0x24, 0xC9, 0x3C, 0xEB, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0x07
};

const struct ecclib_curve curve41417 = {
#include "curve-fns.inc"
};
