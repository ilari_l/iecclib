#ifndef _ecclib_curve3363_h_included_
#define _ecclib_curve3363_h_included_

#include "iecclib.h"
#include "libpfx.h"

#define NAME_E3363 "E-3363"

#define E3363 FAKE_NOEXPORT(E3363)

/**
 * The handle of E-3363 function.
 */
extern const struct ecclib_curve E3363;

#endif
