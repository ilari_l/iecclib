#ifndef _ecclib_curve38921_h_included_
#define _ecclib_curve38921_h_included_

#include "iecclib.h"
#include "libpfx.h"

#define NAME_Ed38921 "Ed38921"

#define Ed38921 FAKE_NOEXPORT(Ed38921)

/**
 * The handle of Ed38921 function.
 */
extern const struct ecclib_curve Ed38921;

#endif
