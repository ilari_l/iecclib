#include "iecclib.h"
#include "25519.h"
#include "fields/25519defs.inc"
#include "utils.h"
#include <stdlib.h>

#define CURVE_NAME NAME_curve25519
#define CLOG2L 253
#define RLOG2L 252
#define CURVE_FIELD field_25519
#define PARTIAL_MAX_BITS 512
#define COFACTOR 8
#define SCALAR_WORDS 8
#define POINT_ALIGN_POWER 4
#define FIELD_BITS FE25519_BITS
#define MONTGOMERY_CONSTANT 121666
#define HAS_COORDINATE_T
#define POINT_STORE_SIZE (FIELD_BITS + 8) / 8

static field_t base_x;
static field_t base_y;
static field_t const_d;
static field_t const_k;
static field_t const_one;
static volatile unsigned global_init;

static const uint8_t montgomery_base[FIELD_STORE_SIZE] = {9};

static void do_global_init()
{
	if(!global_init) {
		call_thread_lock();
		static uint8_t _x[FIELD_STORE_SIZE] = {
			0x1A, 0xD5, 0x25, 0x8F, 0x60, 0x2D, 0x56, 0xC9,
			0xB2, 0xA7, 0x25, 0x95, 0x60, 0xC7, 0x2C, 0x69,
			0x5C, 0xDC, 0xD6, 0xFD, 0x31, 0xE2, 0xA4, 0xC0,
			0xFE, 0x53, 0x6E, 0xCD, 0xD3, 0x36, 0x69, 0x21
		};
		static uint8_t _y[FIELD_STORE_SIZE] = {
			0x58, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66,
			0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66,
			0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66,
			0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66
		};
		static uint8_t _d[FIELD_STORE_SIZE] = {
			0xA3, 0x78, 0x59, 0x13, 0xCA, 0x4D, 0xEB, 0x75,
			0xAB, 0xD8, 0x41, 0x41, 0x4D, 0x0A, 0x70, 0x00,
			0x98, 0xE8, 0x79, 0x77, 0x79, 0x40, 0xC7, 0x8C,
			0x73, 0xFE, 0x6F, 0x2B, 0xEE, 0x6C, 0x03, 0x52
		};
		FIELD_LOAD(&base_x, _x);
		FIELD_LOAD(&base_y, _y);
		FIELD_LOAD(&const_d, _d);
		FIELD_LOAD_SMALL(&const_one, 1);
		FIELD_ADD(&const_k, &const_d, &const_d);
		global_init = 1;
		call_thread_unlock();
	}
}

typedef struct point25519
{
	field_t x;
	field_t y;
	field_t z;
	field_t t;
} point_t;

#include "curve-edwards-common.inc"
#include "curve-edwards-extended.inc"

static inline void _point_add(point_t* out, const point_t* in1, const point_t* in2)
{
	do_global_init();
	field_t A, B, C, D, E, F, G;
	FIELD_SUB(&B, &in1->y, &in1->x);
	FIELD_SUB(&C, &in2->y, &in2->x);
	FIELD_MUL(&A, &B, &C);
	FIELD_ADD(&D, &in1->y, &in1->x);
	FIELD_ADD(&C, &in2->y, &in2->x);
	FIELD_MUL(&B, &D, &C);
	FIELD_MUL(&D, &const_k, &in2->t);
	FIELD_MUL(&C, &in1->t, &D);
	FIELD_ADD(&E, &in2->z, &in2->z);
	FIELD_MUL(&D, &in1->z, &E);
	FIELD_SUB(&E, &B, &A);
	FIELD_SUB(&F, &D, &C);
	FIELD_ADD(&G, &D, &C);
	FIELD_ADD(&C, &B, &A);
	FIELD_MUL(&out->x, &E, &F);
	FIELD_MUL(&out->y, &G, &C);
	FIELD_MUL(&out->t, &E, &C);
	FIELD_MUL(&out->z, &F, &G);
}

static int _point_set(point_t* point, const uint8_t* data)
{
	do_global_init();
	uint8_t _y[FIELD_STORE_SIZE];
	field_t y, A, B, C;
	for(unsigned i = 0; i < FIELD_STORE_SIZE; i++) _y[i] = data[i];
	_y[FIELD_STORE_SIZE - 1] &= 127;

	//Check y in range.
	uint8_t syndrome = 0;
	syndrome = 1 - FIELD_CHECK(_y);

	FIELD_LOAD(&y, _y);
	FIELD_SQR(&A, &y);			//y^2
	FIELD_SUB(&B, &A, &const_one);		//y^2 - 1
	FIELD_MUL(&C, &const_d, &A);		//d*y^2
	FIELD_ADD(&A, &C, &const_one);		//d*y^2 + 1
	FIELD_INV(&C, &A);			//1/(d*y^2 + 1)
	FIELD_MUL(&A, &B, &C);			//(y^2 - 1)/(d*y^2 + 1)
	uint32_t flag = FIELD_SQRT(&B, &A);	//sqrt((y^2 - 1)/(d*y^2 + 1))
	//Now (B,y) is correct up to sign of x if flag is 1.
	FIELD_STORE(_y, &B);
	uint32_t needs_flip = (data[POINT_STORE_SIZE - 1] >> 7) ^ (_y[0] & 1);

	//Check for x=0 and needs_flip, that is illegal.
	for(size_t i = 0; i < FIELD_STORE_SIZE; i++) syndrome |= _y[i];
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	flag &= 1 - ((1 - syndrome) & needs_flip);

	FIELD_NEG(&A, &B);
	//If needs_flip is 1, x coordinate is A, otherwise B.
	FIELD_SELECT(&C, &A, &B, needs_flip);
	FIELD_LOAD_COND(&point->x, &C, flag);
	FIELD_LOAD_COND(&point->y, &y, flag);
	FIELD_LOAD_COND(&point->z, &const_one, flag);
	FIELD_MUL(&B, &C, &y);
	FIELD_LOAD_COND(&point->t, &B, flag);
	return flag;
}


static inline int _point_montgomery(field_t* output, const point_t* point)
{
	field_t A, B;
	FIELD_SUB(&A, &point->z, &point->y);
	FIELD_INV(&B, &A);
	FIELD_ADD(&A, &point->z, &point->y);
	FIELD_MUL(output, &A, &B);
	return 1;
}

static const uint32_t high_word_mul[5] = {
	0xcf5d3ed0, 0x812631a5, 0x2f79cd65, 0x4def9dea, 0x00000001
};
static const uint32_t high_bit_mul[4] = {
	0x5cf5d3ed, 0x5812631a, 0xa2f79cd6, 0x14def9de
};
static const uint32_t curve_order[8] = {
	0x5cf5d3ed, 0x5812631a, 0xa2f79cd6, 0x14def9de, 0x00000000, 0x00000000, 0x00000000, 0x10000000
};
static const uint32_t bias1[13] = {
	0x449c0f01, 0xa40611e3, 0x68859347, 0xd00e1ba7, 0x17f5be65, 0xceec73d2, 0x7c309a3d,
	0x0399411b, 0xcf5d3ed0, 0x812631a5, 0x2f79cd65, 0x4def9dea, 0x00000001
};
static const uint32_t bias2[9] = {
	0x4054c4e2, 0x73044253, 0xbc9fbb2a, 0x6c94fe86, 0x00000003, 0x00000000, 0x00000000, 0xa0000000,
	0x00000002
};

static void reduce_partial(uint32_t* output, const uint32_t* input)
{
	uint32_t tmp1[13];
	uint32_t tmp2[13];
	uint32_t tmp3[13];

	//First, split to two 256-bit parts, multiply upper and accumulate. The largest possible result for this
	//is 2^385 (13 words). We need to pad the lower words due to addition routine workings.
	ecclib_bigint_mul(tmp1, input + 8, 8, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	ecclib_bigint_sub(tmp3, bias1, tmp1, 13);
	for(unsigned i = 0; i < 8; i++) tmp2[i] = input[i];
	for(unsigned i = 8; i < 13; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp1, tmp3, tmp2, 13);

	//Then split the lower 256-bit and upper 129-bit (5 words) parts, multiply upper and accumulate. The largest
	//possible result for this is 2^258 (9 words). Again, lower words need to be padded.
	ecclib_bigint_mul(tmp2, tmp1 + 8, 5, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	ecclib_bigint_sub(tmp3, bias2, tmp2, 9);
	for(unsigned i = 0; i < 8; i++) tmp2[i] = tmp1[i];
	for(unsigned i = 8; i < 9; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp1, tmp2, tmp3, 9);

	//Then split the lower 252-bit and upper 6-bit parts, multiply upper and accumulate. The result of this is
	//less than 2^254. Pad multiply result, it is too small otherwise.
	uint32_t xh = (tmp1[7] >> 28) | (tmp1[8] << 4);
	tmp1[7] &= (1UL << 28) - 1;
	ecclib_bigint_mul(tmp2, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 8; i++) tmp2[i] = 0;
	ecclib_bigint_sub(tmp3, curve_order, tmp2, 8);
	ecclib_bigint_add(output, tmp1, tmp3, 8);
}

static void reduce_full(uint8_t* output, const uint32_t* input)
{
	uint32_t tmp1[9];
	uint32_t tmp2[9];
	uint32_t tmp3[9];
	//Split the lower and upper parts for one final multiplicative reduction. The multiply result is padded as it
	//is too small otherwise.
	for(unsigned i = 0; i < 8; i++) tmp3[i] = input[i];
	uint32_t xh = (tmp3[7] >> 28);
	tmp3[7] &= (1UL << 28) - 1;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 8; i++) tmp1[i] = 0;
	ecclib_bigint_sub(tmp2, curve_order, tmp1, 8);
	ecclib_bigint_add(tmp1, tmp2, tmp3, 8);
	//Now tmp1 is at most twice order. Conditionally substract order.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 7; i < 8; i--) {
		uint32_t neutral = (1 ^ gt) & (1 ^ lt);
		lt |= neutral & (tmp1[i] < curve_order[i]);
		gt |= neutral & (tmp1[i] > curve_order[i]);
	}
	uint32_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 8; i++) tmp3[i] = curve_order[i] & mask;
	ecclib_bigint_sub(tmp1, tmp1, tmp3, 8);
	//Now tmp1 is final reduced value. Convert it to octets.
	ecclib_bigint_store(output, curve25519.scalar_octets, tmp1);
}

const static uint8_t _order[] = {
	0xed, 0xd3, 0xf5, 0x5c, 0x1a, 0x63, 0x12, 0x58, 0xd6, 0x9c, 0xf7, 0xa2, 0xde, 0xf9, 0xde, 0x14,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10
};

const struct ecclib_curve curve25519 = {
#include "curve-fns.inc"
};
