#include "iecclib.h"
#include "38921.h"
#include "fields/38921defs.inc"
#include "utils.h"

#define CURVE_NAME NAME_Ed38921
#define CLOG2L 388
#define RLOG2L 387
#define CURVE_FIELD field_38921
#define PARTIAL_MAX_BITS 800
#define COFACTOR 4
#define SCALAR_WORDS 13
#define POINT_ALIGN_POWER 4
#define FIELD_BITS FE38921_BITS
#define EDWARDS_CONST -5423
#define MONTGOMERY_CONSTANT 5424
#define POINT_STORE_SIZE (FIELD_BITS + 8) / 8

static field_t base_x;
static field_t base_y;
static field_t const_one;
static volatile unsigned global_init;

static const uint8_t montgomery_base[FIELD_STORE_SIZE] = {
	0x4B, 0xDC, 0xFB, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x1F
};

static void do_global_init()
{
	if(!global_init) {
		call_thread_lock();
		static uint8_t _x[FIELD_STORE_SIZE] = {
			0x4A, 0x41, 0x0E, 0x4F, 0x65, 0xC6, 0xCF, 0xA0, 0x5A, 0x8A, 0x1E, 0x8B, 0x4D, 0x87,
			0xC0, 0xEA, 0x72, 0xEF, 0xE2, 0xCF, 0xA6, 0x45, 0xCA, 0x5B, 0xCF, 0x64, 0x5B, 0x2A,
			0xF5, 0x3C, 0xF6, 0x25, 0x7C, 0x7D, 0xFE, 0x68, 0x88, 0x91, 0x8E, 0x89, 0x0B, 0x9C,
			0xC4, 0xB9, 0xD7, 0xE6, 0xBD, 0x10, 0x04
		};
		static uint8_t _y[FIELD_STORE_SIZE] = {
			0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
		};
		FIELD_LOAD(&base_x, _x);
		FIELD_LOAD(&base_y, _y);
		FIELD_LOAD_SMALL(&const_one, 1);
		global_init = 1;
		call_thread_unlock();
	}
}

typedef struct point38921
{
	field_t x;
	field_t y;
	field_t z;
} point_t;

#include "curve-edwards-common.inc"
#include "curve-edwards-normal.inc"

static inline void _point_add(point_t* out, const point_t* in1, const point_t* in2)
{
	field_t A, B, C, D, E, F, G, H;
	FIELD_MUL(&A, &in1->z, &in2->z);
	FIELD_SQR(&B, &A);
	FIELD_MUL(&C, &in1->x, &in2->x);
	FIELD_MUL(&D, &in1->y, &in2->y);
	FIELD_MUL(&F, &C, &D);
	FIELD_SMUL(&E, &F, -EDWARDS_CONST);	//Really -5423, flip sign of E.
	FIELD_ADD(&F, &B, &E);
	FIELD_SUB(&G, &B, &E);
	FIELD_ADD(&B, &in1->x, &in1->y);
	FIELD_ADD(&E, &in2->x, &in2->y);
	FIELD_MUL(&H, &B, &E);
	FIELD_SUB(&B, &H, &C);
	FIELD_SUB(&E, &B, &D);
	FIELD_MUL(&B, &F, &E);
	FIELD_MUL(&out->x, &A, &B);
	FIELD_SUB(&B, &D, &C);
	FIELD_MUL(&C, &G, &B);
	FIELD_MUL(&out->y, &A, &C);
	FIELD_MUL(&out->z, &F, &G);
}

static int _point_set(point_t* point, const uint8_t* data)
{
	do_global_init();
	uint8_t _y[FIELD_STORE_SIZE];
	field_t y, A, B, C;
	for(unsigned i = 0; i < FIELD_STORE_SIZE; i++) _y[i] = data[i];
	_y[FIELD_STORE_SIZE - 1] &= 127;

	//Check y in range.
	uint8_t syndrome = 0;
	syndrome = 1 - FIELD_CHECK(_y);

	FIELD_LOAD(&y, _y);
	FIELD_SQR(&A, &y);			//y^2
	FIELD_SUB(&B, &const_one, &A);		//1 - y^2
	FIELD_SMUL(&C, &A, -EDWARDS_CONST);	//-d*y^2
	FIELD_ADD(&A, &C, &const_one);		//-d*y^2 + 1
	FIELD_INV(&C, &A);			//1/(-d*y^2 + 1)
	FIELD_MUL(&A, &B, &C);			//(1 - y^2)/(-d*y^2 + 1)
	uint32_t flag = FIELD_SQRT(&B, &A);	//sqrt((1 - y^2)/(-d*y^2 + 1))
	//Now (B,y) is correct up to sign of x if flag is 1.
	FIELD_STORE(_y, &B);
	uint32_t needs_flip = (data[POINT_STORE_SIZE - 1] >> 7) ^ (_y[0] & 1);

	//Check for x=0 and needs_flip, that is illegal.
	for(size_t i = 0; i < FIELD_STORE_SIZE; i++) syndrome |= _y[i];
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	flag &= 1 - ((1 - syndrome) & needs_flip);

	FIELD_NEG(&A, &B);
	//If needs_flip is 1, x coordinate is A, otherwise B.
	FIELD_SELECT(&C, &A, &B, needs_flip);
	FIELD_LOAD_COND(&point->x, &C, flag);
	FIELD_LOAD_COND(&point->y, &y, flag);
	FIELD_LOAD_COND(&point->z, &const_one, flag);
	return flag;
}

static inline int _point_montgomery(field_t* output, const point_t* point)
{
	field_t A, B;
	FIELD_INV(&A, &point->x);
	FIELD_MUL(&B, &point->y, &A);
	FIELD_SQR(output, &B);
	return 1;
}

//8000000000000000000000000000000000000000000000000F015C9EE5991796231BCD44844D798E8CD39734D450782F5

static const uint32_t high_word_mul[7] = {
	0xA0000000, 0xA8A0F05E, 0x19A72E69, 0x089AF31D, 0x46379A89, 0xCB322F2C, 0x1E02B93D
};
static const uint32_t high_bit_mul[6] = {
	0x450782F5, 0xCD39734D, 0x44D798E8, 0x31BCD448, 0x59917962, 0xF015C9EE
};

static const uint32_t curve_order[14] = {
	0x450782F5, 0xCD39734D, 0x44D798E8, 0x31BCD448, 0x59917962, 0xF015C9EE, 0x00000000, 0x00000000,
	0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000008, 0x00000000
};
static const uint32_t bias1[19] = {
	0x97E016BA, 0xF2D32494, 0xD14B7BF8, 0xE85B1124, 0x1A2F4D2E, 0xB5CCF60F, 0xD10DD022,
	0x0562307F, 0xFDFA7F61, 0x095B0D62, 0x1C873A53, 0xE6A14E70, 0xA0000001, 0xA8A0F05E,
	0x19A72E69, 0x089AF31D, 0x46379A89, 0xCB322F2C, 0x1E02B93D,
};

static const uint32_t bias2[14] = {
	0x6DADB729, 0xDF1D8456, 0xADF51331, 0x6014FC65, 0xB3E6BB4B, 0x451AEDE6, 0x0070946E,
	0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x03C05728, 0x00000000
};

static void reduce_partial(uint32_t* output, const uint32_t* input)
{
	uint32_t tmp1[20];
	uint32_t tmp2[20];
	uint32_t tmp3[20];

	//First, split the lower 416-bit and upper 384-bit parts, multiply upper and accumulate. The largest
	//possible result for this is 2^605 (19 words). We need to pad the lower words due to addition routine
	//workings.
	ecclib_bigint_mul(tmp1, input + 13, 12, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	ecclib_bigint_sub(tmp3, bias1, tmp1, 19);
	for(unsigned i = 0; i < 13; i++) tmp2[i] = input[i];
	for(unsigned i = 13; i < 20; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp1, tmp3, tmp2, 19);

	//Then split the lower 416-bit and upper 189-bit (6 words) parts, multiply upper and accumulate. The largest
	//possible result for this is 2^417 (14 words). Again, lower words need to be padded.
	ecclib_bigint_mul(tmp2, tmp1 + 13, 6, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	tmp2[13] = 0;
	ecclib_bigint_sub(tmp3, bias2, tmp2, 14);
	for(unsigned i = 0; i < 13; i++) tmp2[i] = tmp1[i];
	for(unsigned i = 13; i < 14; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp1, tmp2, tmp3, 14);

	//Then split the lower 387-bit and upper 30-bit parts, multiply upper and accumulate. The result of this is
	//less than 2^389. Pad multiply result, it is too small otherwise.
	uint32_t xh = (tmp1[12] >> 3) | (tmp1[13] << 29);
	tmp1[12] &= (1UL << 3) - 1;
	ecclib_bigint_mul(tmp2, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 13; i++) tmp2[i] = 0;
	ecclib_bigint_sub(tmp3, curve_order, tmp2, 13);
	ecclib_bigint_add(output, tmp1, tmp3, 13);
}

static void reduce_full(uint8_t* output, const uint32_t* input)
{
	uint32_t tmp1[13];
	uint32_t tmp2[13];
	uint32_t tmp3[13];
	//Split the lower and upper parts for one final multiplicative reduction. The multiply result is padded as it
	//is too small otherwise.
	for(unsigned i = 0; i < 13; i++) tmp3[i] = input[i];
	uint32_t xh = (tmp3[12] >> 3);
	tmp3[12] &= (1UL << 3) - 1;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 13; i++) tmp1[i] = 0;
	ecclib_bigint_sub(tmp2, curve_order, tmp1, 13);
	ecclib_bigint_add(tmp1, tmp2, tmp3, 13);
	//Now tmp1 is at most twice order. Conditionally substract order.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 12; i < 13; i--) {
		uint32_t neutral = (1 ^ gt) & (1 ^ lt);
		lt |= neutral & (tmp1[i] < curve_order[i]);
		gt |= neutral & (tmp1[i] > curve_order[i]);
	}
	uint32_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 13; i++) tmp3[i] = curve_order[i] & mask;
	ecclib_bigint_sub(tmp2, tmp1, tmp3, 13);
	//Now tmp1 is final reduced value. Convert it to octets.
	ecclib_bigint_store(output, Ed38921.scalar_octets, tmp2);
}

const static uint8_t _order[] = {
	0xF5, 0x82, 0x07, 0x45, 0x4D, 0x73, 0x39, 0xCD, 0xE8, 0x98, 0xD7, 0x44, 0x48, 0xD4, 0xBC, 0x31,
	0x62, 0x79, 0x91, 0x59, 0xEE, 0xC9, 0x15, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x08
};

const struct ecclib_curve Ed38921 = {
#include "curve-fns.inc"
};
