#include <stdio.h>
#include "iecclib.h"
#include "e521.h"
#include "fields/5211defs.inc"
#include "utils.h"

#define CURVE_NAME NAME_E521
#define CLOG2L 519
#define RLOG2L 519
#define CURVE_FIELD field_5211
#define PARTIAL_MAX_BITS 1073
#define COFACTOR 4
#define SCALAR_WORDS 17
#define POINT_ALIGN_POWER 4
#define FIELD_BITS FE5211_BITS
#define EDWARDS_CONST -376014
#define MONTGOMERY_CONSTANT 376015
#define POINT_STORE_SIZE (FIELD_BITS + 8) / 8

static field_t base_x;
static field_t base_y;
static field_t const_one;
static volatile unsigned global_init;

static const uint8_t montgomery_base[FIELD_STORE_SIZE] = {
	0x28, 0x15, 0xD9, 0xEA, 0x55, 0x30, 0x4B, 0xCA, 0x01, 0x91, 0xE1, 0x5E, 0x05, 0xB3, 0xA4, 0x1C,
	0x10, 0x19, 0xEE, 0x55, 0x30, 0x4B, 0xCA, 0x01, 0x91, 0xE1, 0x5E, 0x05, 0xB3, 0xA4, 0x1C, 0x10,
	0x19, 0xEE, 0x55, 0x30, 0x4B, 0xCA, 0x01, 0x91, 0xE1, 0x5E, 0x05, 0xB3, 0xA4, 0x1C, 0x10, 0x19,
	0xEE, 0x55, 0x30, 0x4B, 0xCA, 0x01, 0x91, 0xE1, 0x5E, 0x05, 0xB3, 0xA4, 0x1C, 0x10, 0x19, 0xEE,
	0x55
};

static void do_global_init()
{
	if(!global_init) {
		call_thread_lock();
		static uint8_t _x[FIELD_STORE_SIZE] = {
			0x6C, 0xBA, 0x19, 0x2F, 0x0A, 0x94, 0x2A, 0x30, 0xAA, 0x38, 0x48, 0x36, 0x13, 0xFB,
			0xD0, 0x59, 0x60, 0x9C, 0xC9, 0x8F, 0x56, 0x9D, 0x94, 0xAE, 0xB1, 0x34, 0x24, 0xC7,
			0xCC, 0xC5, 0xEC, 0xF6, 0x13, 0x39, 0x20, 0xC6, 0xC0, 0xC9, 0xF3, 0x8B, 0xEC, 0x18,
			0xC8, 0xC6, 0x2F, 0xF4, 0xD9, 0xBF, 0xA3, 0x78, 0x28, 0x6B, 0x29, 0xB2, 0x0C, 0xF9,
			0x9D, 0x18, 0x8B, 0x64, 0x48, 0x5C, 0xB4, 0x2C, 0x75
		};
		static uint8_t _y[FIELD_STORE_SIZE] = {
			0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
		};
		FIELD_LOAD(&base_x, _x);
		FIELD_LOAD(&base_y, _y);
		FIELD_LOAD_SMALL(&const_one, 1);
		global_init = 1;
		call_thread_unlock();
	}
}

typedef struct point5211
{
	field_t x;
	field_t y;
	field_t z;
} point_t;

#include "curve-edwards-common.inc"
#include "curve-edwards-normal.inc"

static inline void _point_add(point_t* out, const point_t* in1, const point_t* in2)
{
	field_t A, B, C, D, E, F, G, H;
	FIELD_MUL(&A, &in1->z, &in2->z);
	FIELD_SQR(&B, &A);
	FIELD_MUL(&C, &in1->x, &in2->x);
	FIELD_MUL(&D, &in1->y, &in2->y);
	FIELD_MUL(&F, &C, &D);
	FIELD_SMUL(&E, &F, -EDWARDS_CONST);	//Really -376014, flip sign of E.
	FIELD_ADD(&F, &B, &E);
	FIELD_SUB(&G, &B, &E);
	FIELD_ADD(&B, &in1->x, &in1->y);
	FIELD_ADD(&E, &in2->x, &in2->y);
	FIELD_MUL(&H, &B, &E);
	FIELD_SUB(&B, &H, &C);
	FIELD_SUB(&E, &B, &D);
	FIELD_MUL(&B, &F, &E);
	FIELD_MUL(&out->x, &A, &B);
	FIELD_SUB(&B, &D, &C);
	FIELD_MUL(&C, &G, &B);
	FIELD_MUL(&out->y, &A, &C);
	FIELD_MUL(&out->z, &F, &G);
}

static int _point_set(point_t* point, const uint8_t* data)
{
	do_global_init();
	uint8_t _y[FIELD_STORE_SIZE];
	field_t y, A, B, C;
	for(unsigned i = 0; i < FIELD_STORE_SIZE; i++) _y[i] = data[i];
	_y[FIELD_STORE_SIZE - 1] &= 127;

	//Check y in range.
	uint8_t syndrome = 0;
	syndrome = 1 - FIELD_CHECK(_y);

	FIELD_LOAD(&y, _y);
	FIELD_SQR(&A, &y);			//y^2
	FIELD_SUB(&B, &const_one, &A);		//1 - y^2
	FIELD_SMUL(&C, &A, -EDWARDS_CONST);	//-d*y^2
	FIELD_ADD(&A, &C, &const_one);		//-d*y^2 + 1
	FIELD_INV(&C, &A);			//1/(-d*y^2 + 1)
	FIELD_MUL(&A, &B, &C);			//(1 - y^2)/(-d*y^2 + 1)
	uint32_t flag = FIELD_SQRT(&B, &A);	//sqrt((1 - y^2)/(-d*y^2 + 1))
	//Now (B,y) is correct up to sign of x if flag is 1.
	FIELD_STORE(_y, &B);
	uint32_t needs_flip = (data[POINT_STORE_SIZE - 1] >> 7) ^ (_y[0] & 1);

	//Check for x=0 and needs_flip, that is illegal.
	for(size_t i = 0; i < FIELD_STORE_SIZE; i++) syndrome |= _y[i];
	syndrome = (syndrome | (syndrome >> 4)) & 15;
	syndrome = (syndrome | (syndrome >> 2)) & 3;
	syndrome = (syndrome | (syndrome >> 1)) & 1;
	flag &= 1 - ((1 - syndrome) & needs_flip);

	FIELD_NEG(&A, &B);
	//If needs_flip is 1, x coordinate is A, otherwise B.
	FIELD_SELECT(&C, &A, &B, needs_flip);
	FIELD_LOAD_COND(&point->x, &C, flag);
	FIELD_LOAD_COND(&point->y, &y, flag);
	FIELD_LOAD_COND(&point->z, &const_one, flag);
	return flag;
}

static inline int _point_montgomery(field_t* output, const point_t* point)
{
	field_t A, B;
	FIELD_INV(&A, &point->x);
	FIELD_MUL(&B, &point->y, &A);
	FIELD_SQR(output, &B);
	return 1;
}

//7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd15b6c64746fc85f736b8af5e7ec53f04fbd8c4569a8f1f4540ea2435f5180d6b
//2EA4939B8B9037A08C94750A1813AC0FB04273BA96570E0BABF15DBCA0AE7F295


static const uint32_t high_word_mul[9] = {
	0x2A000000, 0x9415CFE5, 0x757E2BB7, 0x52CAE1C1, 0xF6084E77, 0x43027581, 0x11928EA1, 0x717206F4, 0x05D49273
};

static const uint32_t high_bit_mul[9] = {
	0x0AE7F295, 0xBF15DBCA, 0x6570E0BA, 0x04273BA9, 0x813AC0FB, 0xC94750A1, 0xB9037A08, 0xEA4939B8, 0x00000002
};

static const uint32_t curve_order[17] = {
	0xF5180D6B, 0x40EA2435, 0x9A8F1F45, 0xFBD8C456, 0x7EC53F04, 0x36B8AF5E, 0x46FC85F7,
	0x15B6C647, 0xFFFFFFFD, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
	0xFFFFFFFF, 0xFFFFFFFF, 0x0000007F
};

static void reduce_partial(uint32_t* output, const uint32_t* input)
{
	uint32_t tmp1[26];
	uint32_t tmp2[26];
	uint32_t tmp3[26];
	//First, split to lower 544-bit and upper 529-bit parts, multiply upper and accumulate. The largest possible
	//result for this is 2^812 (26 words). We need to pad the lower words due to addition routine workings.
	ecclib_bigint_mul(tmp1, input + 17, 17, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	for(unsigned i = 0; i < 17; i++) tmp2[i] = input[i];
	for(unsigned i = 17; i < 26; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp3, tmp1, tmp2, 26);
	//Then split the lower 544-bit and upper 268-bit (9 words) parts, multiply upper and accumulate. The largest
	//possible result for this is 2^551 (18 words). Again, lower words need to be padded.
	ecclib_bigint_mul(tmp1, tmp3 + 17, 9, high_word_mul, sizeof(high_word_mul) / sizeof(high_word_mul[0]));
	for(unsigned i = 0; i < 17; i++) tmp2[i] = tmp3[i];
	for(unsigned i = 17; i < 18; i++) tmp2[i] = 0;
	ecclib_bigint_add(tmp3, tmp1, tmp2, 18);
	//Then split the lower 519-bit and upper 32-bit parts, multiply upper and accumulate. The result of this is
	//less than 2^520. Pad multiply result, it is too small otherwise.
	uint32_t xh = (tmp3[16] >> 7) | (tmp3[17] << 25);
	tmp3[16] &= (1UL << 7) - 1;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 17; i++) tmp1[i] = 0;
	ecclib_bigint_add(output, tmp1, tmp3, 17);
}

static void reduce_full(uint8_t* output, const uint32_t* input)
{
	uint32_t tmp1[18];
	uint32_t tmp2[18];
	uint32_t tmp3[18];
	//Split the lower and upper parts for one final multiplicative reduction. The multiply result is padded as it
	//is too small otherwise.
	for(unsigned i = 0; i < 17; i++) tmp3[i] = input[i];
	uint32_t xh = (tmp3[16] >> 7);
	tmp3[16] &= (1UL << 7) - 1;
	ecclib_bigint_mul(tmp1, high_bit_mul, sizeof(high_bit_mul) / sizeof(high_bit_mul[0]), &xh, 1);
	for(unsigned i = 1 + sizeof(high_bit_mul) / sizeof(high_bit_mul[0]); i < 18; i++) tmp1[i] = 0;
	ecclib_bigint_add(tmp2, tmp1, tmp3, 17);
	//Now tmp2 is at most twice order. Conditionally substract order.
	uint32_t lt = 0, gt = 0;
	for(unsigned i = 16; i < 17; i--) {
		uint32_t neutral = (1 ^ gt) & (1 ^ lt);
		lt |= neutral & (tmp2[i] < curve_order[i]);
		gt |= neutral & (tmp2[i] > curve_order[i]);
	}
	uint32_t mask = -(1 ^ lt);
	for(unsigned i = 0; i < 17; i++) tmp3[i] = curve_order[i] & mask;
	ecclib_bigint_sub(tmp1, tmp2, tmp3, 17);
	//Now tmp1 is final reduced value. Convert it to octets.
	ecclib_bigint_store(output, E521.scalar_octets, tmp1);
}

const static uint8_t _order[] = {
	0x6B, 0x0D, 0x18, 0xF5, 0x35, 0x24, 0xEA, 0x40, 0x45, 0x1F, 0x8F, 0x9A, 0x56, 0xC4, 0xD8, 0xFB,
	0x04, 0x3F, 0xC5, 0x7E, 0x5E, 0xAF, 0xB8, 0x36, 0xF7, 0x85, 0xFC, 0x46, 0x47, 0xC6, 0xB6, 0x15,
	0xFD, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x7F
};

const struct ecclib_curve E521 = {
#include "curve-fns.inc"
};
