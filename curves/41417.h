#ifndef _ecclib_curve41417_h_included_
#define _ecclib_curve41417_h_included_

#include "iecclib.h"
#include "libpfx.h"

#define NAME_curve41417 "curve41417"

#define curve41417 FAKE_NOEXPORT(curve41417)

/**
 * The handle of curve41417 function.
 */
extern const struct ecclib_curve curve41417;

#endif
