#include "iecclib.h"
#define DO_CC_INCLUDES
#include "curves/list.inc"
#undef DO_CC_INCLUDES
#include "utils.h"

#define CANDIDATE(X) do { if(streq(name, NAME_##X )) return & X ; } while(0);
#define ALIAS(X, Y) do { if(streq(name, X )) name = Y ; } while(0);

const struct ecclib_curve* ecclib_lookup_curve_by_name(const char* name)
{
#include "curves/list.inc"
	return NULL;
}

#undef ALIAS
#undef CANDIDATE
#define CANDIDATE(X) NAME_##X ,
#define ALIAS(X, Y) X ,


const char** ecclib_curve_list()
{
	static const char* list[] = {
#include "curves/list.inc"
		NULL
	};
	return list;
}
