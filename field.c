#include "iecclib.h"
#define DO_CC_INCLUDES
#include "fields/list.inc"
#undef DO_CC_INCLUDES
#include "utils.h"

#define CANDIDATE(X) do { if(streq(name, NAME_##X )) return & X ; } while(0);

const struct ecclib_field* ecclib_lookup_field_by_name(const char* name)
{
#include "fields/list.inc"
	return NULL;
}

#undef CANDIDATE
#define CANDIDATE(X) NAME_##X ,

const char** ecclib_field_list()
{
	static const char* list[] = {
#include "fields/list.inc"
		NULL
	};
	return list;
}
