#include "utils.h"

static void field_zero(void* _out)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	FIELD_ZERO(out);
}

static void field_set_int(void* _out, uint32_t val)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	FIELD_LOAD_SMALL(out, val);
}

static void field_set(void* _out, const uint8_t* val)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	FIELD_LOAD(out, val);
}

static void field_get(const void* _in, uint8_t* val)
{
	const field_t* in = DO_ALIGN(const field_t, _in, FIELD_ALIGN_POWER);
	FIELD_STORE(val, in);
}

static int field_check(const uint8_t* val)
{
	return FIELD_CHECK(val);
}

static void field_add(void* _out, const void* _in1, const void* _in2)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in1 = DO_ALIGN(const field_t, _in1, FIELD_ALIGN_POWER);
	const field_t* in2 = DO_ALIGN(const field_t, _in2, FIELD_ALIGN_POWER);
	FIELD_ADD(out, in1, in2);
}

static void field_sub(void* _out, const void* _in1, const void* _in2)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in1 = DO_ALIGN(const field_t, _in1, FIELD_ALIGN_POWER);
	const field_t* in2 = DO_ALIGN(const field_t, _in2, FIELD_ALIGN_POWER);
	FIELD_SUB(out, in1, in2);
}

static void field_mul(void* _out, const void* _in1, const void* _in2)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in1 = DO_ALIGN(const field_t, _in1, FIELD_ALIGN_POWER);
	const field_t* in2 = DO_ALIGN(const field_t, _in2, FIELD_ALIGN_POWER);
	FIELD_MUL(out, in1, in2);
}

static void field_smul(void* _out, const void* _in1, uint32_t in2)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in1 = DO_ALIGN(const field_t, _in1, FIELD_ALIGN_POWER);
	FIELD_SMUL(out, in1, in2);
}

static void field_neg(void* _out, const void* _in)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in = DO_ALIGN(const field_t, _in, FIELD_ALIGN_POWER);
	FIELD_NEG(out, in);
}

static void field_sqr(void* _out, const void* _in)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in = DO_ALIGN(const field_t, _in, FIELD_ALIGN_POWER);
	FIELD_SQR(out, in);
}

static void field_inv(void* _out, const void* _in)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in = DO_ALIGN(const field_t, _in, FIELD_ALIGN_POWER);
	FIELD_INV(out, in);
}

static void field_copy(void* _out, const void* _in)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in = DO_ALIGN(const field_t, _in, FIELD_ALIGN_POWER);
	*out = *in;
}

static int field_sqrt(void* _out, const void* _in)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in = DO_ALIGN(const field_t, _in, FIELD_ALIGN_POWER);
	return FIELD_SQRT(out, in);
}

static void field_load_cond(void* _out, const void* _in, uint32_t flag)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in = DO_ALIGN(const field_t, _in, FIELD_ALIGN_POWER);
	return FIELD_LOAD_COND(out, in, flag & 1);
}

static void field_select(void* _out, const void* _in1, const void* _in0, uint32_t flag)
{
	field_t* out = DO_ALIGN(field_t, _out, FIELD_ALIGN_POWER);
	const field_t* in1 = DO_ALIGN(const field_t, _in1, FIELD_ALIGN_POWER);
	const field_t* in0 = DO_ALIGN(const field_t, _in0, FIELD_ALIGN_POWER);
	return FIELD_SELECT(out, in1, in0, flag & 1);
}

static void field_cswap(void* _x1, void* _x2, uint32_t flag)
{
	field_t* x1 = DO_ALIGN(field_t, _x1, FIELD_ALIGN_POWER);
	field_t* x2 = DO_ALIGN(field_t, _x2, FIELD_ALIGN_POWER);
	return FIELD_CSWAP(x1, x2, flag & 1);
}

const struct ecclib_field FIELD_NAME = {
.name = FIELD_REAL_NAME,
.element_size = ELEMENT_SIZE,
.field_bits = FIELD_BITS,
.storage_octets = STORAGE_OCTETS,
.field_element_count = FIELD_ELEMENT_COUNT,
.field_characteristic = FIELD_CHARACTERISTIC,
.degree = FIELD_DEGREE,
.degree_stride = FIELD_DEGREE_STRIDE,
.degree_bits = FIELD_DEGREE_BITS,
.zero = field_zero,
.set_int = field_set_int,
.set = field_set,
.get = field_get,
.check = field_check,
.add = field_add,
.sub = field_sub,
.mul = field_mul,
.smul = field_smul,
.neg = field_neg,
.sqr = field_sqr,
.inv = field_inv,
.sqrt = field_sqrt,
.load_cond = field_load_cond,
.select = field_select,
.cswap = field_cswap,
.copy = field_copy,
};
