#include "iecclib.h"

void ecclib_bigint_add(uint32_t* out, const uint32_t* in1, const uint32_t* in2, size_t words)
{
	uint64_t carry = 0;
	for(size_t i = 0; i < words; i++) {
		carry = carry + in1[i] + in2[i];
		out[i] = carry;
		carry >>= 32;
	}
}

void ecclib_bigint_sub(uint32_t* out, const uint32_t* in1, const uint32_t* in2, size_t words)
{
	uint64_t borrow = 0;
	for(size_t i = 0; i < words; i++) {
		uint64_t r = (uint64_t)in1[i] - in2[i] - borrow;
		out[i] = r;
		borrow = r >> 63;
	}
}

void ecclib_bigint_mul(uint32_t* out, const uint32_t* in1, size_t words1, const uint32_t* in2, size_t words2)
{
	if(words2 == 1) {
		//Special case for 1-word multiplication.
		uint64_t carry = 0;
		uint64_t m = in2[0];
		for(size_t i = 0; i < words1; i++) {
			carry += m * in1[i];
			out[i] = carry;
			carry >>= 32;
		}
		out[words1] = carry;
	} else {
		uint64_t carry_l = 0, carry_h = 0;
		size_t words0 = words1 + words2;
		for(size_t o = 0; o < words0; o++) {
			size_t imin = (o >= words2) ? (o - words2 + 1) : 0;
			size_t imax = (words1 > o + 1) ? (o + 1) : words1;
			for(size_t i = imin; i < imax; i++) {
				uint64_t r = (uint64_t)in1[i] * in2[o - i];
				carry_l += r & ((1ULL << 32) - 1);
				carry_h += r >> 32;
			}
			out[o] = carry_l;
			carry_l = carry_h + (carry_l >> 32);
			carry_h = 0;
		}
	}
}

void ecclib_bigint_load(uint32_t* out, size_t outwords, const uint8_t* in, size_t inoctets)
{
	if(inoctets > 4 * outwords)
		inoctets = 4 * outwords;
	for(size_t i = 0; i < outwords; i++)
		out[i] = 0;
	for(size_t i = 0; i < inoctets; i++)
		out[i>>2] |= (uint32_t)in[i] << ((i&3)<<3);
}

void ecclib_bigint_store(uint8_t* out, size_t outoctets, const uint32_t* in)
{
	for(size_t i = 0; i < outoctets; i++)
		out[i] = in[i>>2] >> ((i&3)<<3);
}
