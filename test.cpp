#ifdef TEST_ECCLIB_CODE
#include <cstdio>
#include <set>
#include <cstdlib>
#include <cstdint>
#include <functional>
#include <cstring>
#include <cassert>
#include <fcntl.h>
#include <sys/time.h>
#include <vector>
#include <mutex>
#include <thread>
#include <unistd.h>
#include "iecclib.h"
#include "utils.h"
#include "fields/25519.h"
#include "fields/3363.h"
#include "fields/38921.h"
#include "fields/41417.h"
#include "fields/448.h"

uint64_t timestamp()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (uint64_t)tv.tv_sec * 1000000 + tv.tv_usec;
}

class buffer
{
public:
	buffer(size_t size)
	{
		buf = new uint8_t[bufsize = size];
		for(size_t i = 0; i < size; i++) buf[i] = 0;
	}
	buffer(std::initializer_list<uint8_t> args)
	{
		buf = new uint8_t[bufsize = args.size()];
		size_t i = 0;
		for(auto j : args)
			buf[i++] = j;
	}
	~buffer()
	{
		delete[] buf;
	}
	operator uint8_t*() { return buf; }
	operator void*() { return buf; }
	size_t size() const { return bufsize; }
	bool operator!=(const buffer& c) const { return !(*this == c); }
	bool operator==(const buffer& c) const { return bufsize == c.bufsize && !memcmp(buf, c.buf, bufsize); }
private:
	uint8_t* buf;
	size_t bufsize;
	buffer(const buffer&);
	buffer& operator=(const buffer&);
};

class buffer32
{
public:
	buffer32(size_t size)
	{
		buf = new uint32_t[bufsize = size];
		for(size_t i = 0; i < size; i++) buf[i] = 0;
	}
	buffer32(std::initializer_list<uint32_t> args)
	{
		buf = new uint32_t[bufsize = args.size()];
		size_t i = 0;
		for(auto j : args)
			buf[i++] = j;
	}
	~buffer32()
	{
		delete[] buf;
	}
	operator const uint32_t*() const { return buf; }
	operator uint32_t*() { return buf; }
	size_t size() const { return bufsize; }
	bool operator!=(const buffer32& c) const { return !(*this == c); }
	bool operator==(const buffer32& c) const {
		return bufsize == c.bufsize && !memcmp(buf, c.buf, sizeof(uint32_t) * bufsize);
	}
private:
	uint32_t* buf;
	size_t bufsize;
	buffer32(const buffer32&);
	buffer32& operator=(const buffer32&);
};

uint32_t gcd(uint32_t a, uint32_t b)
{
	if(a % b)
		return gcd(b, a % b);
	else
		return b;
}

uint32_t lcm(uint32_t a, uint32_t b)
{
	return (uint64_t)a * b / gcd(a, b);
}

void random_fill(void* _buffer, size_t bufsize)
{
	uint8_t* buffer = (uint8_t*)_buffer;
	static int fd = -1;
	while(fd < 0) fd = open("/dev/urandom", O_RDONLY);
	while(bufsize > 0) {
		ssize_t r = read(fd, buffer, bufsize);
		if(r <= 0) continue;
		buffer += r;
		bufsize -= r;
	}
}

void random_fill(buffer& _buffer)
{
	random_fill(_buffer, _buffer.size());
}

void random_fill(buffer32& _buffer)
{
	random_fill(_buffer, sizeof(uint32_t) * _buffer.size());
}

bool generate_random_point(const ecclib_curve* crv, buffer& out)
{
	buffer _in(crv->storage_octets);
	uint64_t deadline = timestamp() + 15000000;
retry:
	//Try encodings as far as one works.
	random_fill(_in);
	if(crv->point_set(out, _in) <= 0) {
		if(timestamp() > deadline) return false;
		goto retry;
	}
	return true;
}

bool loop_repeat(std::function<bool()> fn)
{
	uint64_t deadline = timestamp() + 1000000;
	unsigned i = 0;
	while(i < 100 || timestamp() < deadline) {
		if(!fn()) return false;
		i++;
	}
	return true;
}

bool do_field_mulsmul_consistency_check(const ecclib_field* fld)
{
	buffer _in1(fld->storage_octets);
	buffer _in2(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _out2(fld->storage_octets);
	uint32_t xin2;
	buffer in1(fld->element_size);
	buffer in2(fld->element_size);
	buffer out1(fld->element_size);
	buffer out2(fld->element_size);
	return loop_repeat([fld, &_in1, &_in2, &_out1, &_out2, &in1, &in2, &out1, &out2, &xin2]() -> bool {
		random_fill(_in1);
		random_fill(_in2);
		for(size_t i = 3; i < fld->storage_octets; i++) _in2[i] = 0;
		fld->set(in1, _in1);
		fld->set(in2, _in2);
		xin2 = _in2[0] + 256 * _in2[1] + 65536 * _in2[2];
		fld->mul(out1, in1, in2);
		fld->smul(out2, in1, xin2);
		fld->get(out1, _out1);
		fld->get(out2, _out2);
		return (_out1 == _out2);
	});
}

bool do_field_addneg_consistency_check(const ecclib_field* fld)
{
	buffer _in1(fld->storage_octets);
	buffer _in2(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _out2(fld->storage_octets);
	buffer in1(fld->element_size);
	buffer in2(fld->element_size);
	buffer iin2(fld->element_size);
	buffer out1(fld->element_size);
	buffer out2(fld->element_size);
	return loop_repeat([fld, &_in1, &_in2, &_out1, &_out2, &in1, &in2, &iin2, &out1, &out2]() -> bool {
		random_fill(_in1);
		random_fill(_in2);
		fld->set(in1, _in1);
		fld->set(in2, _in2);
		fld->add(out1, in1, in2);
		fld->neg(iin2, in2);
		fld->add(out2, out1, iin2);
		fld->get(in1, _out1);
		fld->get(out2, _out2);
		return (_out1 == _out2);
	});
}

bool do_field_move_checks(const ecclib_field* fld)
{
	buffer _in1(fld->storage_octets);
	buffer _in2(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _zero(fld->storage_octets);
	buffer in1(fld->element_size);
	buffer in2(fld->element_size);
	buffer out1(fld->element_size);
	return loop_repeat([fld, &_in1, &_in2, &_out1, &in1, &in2, &out1, &_zero]() -> bool {
		while(_in1 == _zero || _in2 == _zero || _in1 == _in2) {
			random_fill(_in1);
			random_fill(_in2);
		}
		fld->set(in1, _in1);
		fld->set(in2, _in2);
		fld->get(in1, _in1);	//Readback to discard extra bits.
		fld->get(in2, _in2);
		fld->select(out1, in1, in2, 1);
		fld->get(out1, _out1);
		if(_out1 != _in1) return false;
		fld->select(out1, in1, in2, 0);
		fld->get(out1, _out1);
		if(_out1 != _in2) return false;
		fld->zero(out1);
		fld->get(out1, _out1);
		if(_out1 != _zero) return false;
		fld->load_cond(out1, in1, 0);
		fld->get(out1, _out1);
		if(_out1 != _zero) return false;
		fld->load_cond(out1, in1, 1);
		fld->get(out1, _out1);
		if(_out1 != _in1) return false;
		fld->cswap(in1, in2, 0);
		fld->get(in1, _out1);
		if(_out1 != _in1) return false;
		fld->get(in2, _out1);
		if(_out1 != _in2) return false;
		fld->cswap(in1, in2, 1);
		fld->get(in1, _out1);
		if(_out1 != _in2) return false;
		fld->get(in2, _out1);
		if(_out1 != _in1) return false;
		return true;
	});
}


bool do_field_commutativity_check2(const ecclib_field* fld, void(*add)(void* t, const void* s1, const void* s2))
{
	buffer _in1(fld->storage_octets);
	buffer _in2(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _out2(fld->storage_octets);
	buffer in1(fld->element_size);
	buffer in2(fld->element_size);
	buffer out1(fld->element_size);
	buffer out2(fld->element_size);
	return loop_repeat([fld, add, &_in1, &_in2, &_out1, &_out2, &in1, &in2, &out1, &out2]() -> bool {
		random_fill(_in1);
		random_fill(_in2);
		fld->set(in1, _in1);
		fld->set(in2, _in2);
		add(out1, in1, in2);
		add(out2, in2, in1);
		fld->get(out2, _out2);
		fld->get(out1, _out1);
		return (_out1 == _out2);
	});
}

bool do_field_associvity_check2(const ecclib_field* fld, void(*add)(void* t, const void* s1, const void* s2))
{
	buffer _in1(fld->storage_octets);
	buffer _in2(fld->storage_octets);
	buffer _in3(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _out2(fld->storage_octets);
	buffer in1(fld->element_size);
	buffer in2(fld->element_size);
	buffer in3(fld->element_size);
	buffer out1(fld->element_size);
	buffer out2(fld->element_size);
	buffer tmp(fld->element_size);
	return loop_repeat([fld, add, &_in1, &_in2, &_in3, &_out1, &_out2, &in1, &in2, &in3, &tmp, &out1, &out2]()
		-> bool {
		random_fill(_in1);
		random_fill(_in2);
		random_fill(_in3);
		fld->set(in1, _in1);
		fld->set(in2, _in2);
		fld->set(in3, _in3);
		add(tmp, in1, in2);
		add(out1, tmp, in3);
		add(tmp, in2, in3);
		add(out2, in1, tmp);
		fld->get(out1, _out1);
		fld->get(out2, _out2);
		return (_out1 == _out2);
	});
}

bool do_field_addsub_check2(const ecclib_field* fld)
{
	buffer _in1(fld->storage_octets);
	buffer _in2(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _out2(fld->storage_octets);
	buffer in1(fld->element_size);
	buffer in2(fld->element_size);
	buffer tmp(fld->element_size);
	buffer out(fld->element_size);
	return loop_repeat([fld, &_in1, &_in2, &_out1, &_out2, &in1, &in2, &tmp, &out]() -> bool {
		random_fill(_in1);
		random_fill(_in2);
		fld->set(in1, _in1);
		fld->set(in2, _in2);
		fld->add(tmp, in1, in2);
		fld->sub(out, tmp, in2);
		fld->get(in1, _out1);
		fld->get(out, _out2);
		return (_out1 == _out2);
	});
}

bool do_field_mulsqr_check2(const ecclib_field* fld)
{
	buffer _in(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _out2(fld->storage_octets);
	buffer in(fld->element_size);
	buffer out1(fld->element_size);
	buffer out2(fld->element_size);
	return loop_repeat([fld, &_in, &_out1, &_out2, &in, &out1, &out2]() -> bool {
		random_fill(_in);
		fld->set(in, _in);
		fld->mul(out1, in, in);
		fld->sqr(out2, in);
		fld->get(out1, _out1);
		fld->get(out2, _out2);
		return (_out1 == _out2);
	});
}

bool do_field_mulinv_check2(const ecclib_field* fld)
{
	buffer _in1(fld->storage_octets);
	buffer _in2(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _out2(fld->storage_octets);
	buffer in1(fld->element_size);
	buffer in2(fld->element_size);
	buffer iin2(fld->element_size);
	buffer out1(fld->element_size);
	buffer out2(fld->element_size);
	return loop_repeat([fld, &_in1, &_in2, &_out1, &_out2, &in1, &in2, &iin2, &out1, &out2]() -> bool {
		random_fill(_in1);
		random_fill(_in2);
		fld->set(in1, _in1);
		fld->set(in2, _in2);
		fld->mul(out1, in1, in2);
		fld->inv(iin2, in2);
		fld->mul(out2, out1, iin2);
		fld->get(in1, _out1);
		fld->get(out2, _out2);
		return (_out1 == _out2);
	});
}

bool do_field_inverse_check2(const ecclib_field* fld)
{
	buffer _one(fld->storage_octets);
	buffer _in(fld->storage_octets);
	buffer _out(fld->storage_octets);
	buffer in(fld->element_size);
	buffer iin(fld->element_size);
	buffer out(fld->element_size);
	buffer one(fld->element_size);
	fld->set_int(one, 1);
	fld->get(one, _one);
	return loop_repeat([fld, &_in, &_out, &in, &iin, &out, &_one]() -> bool {
		random_fill(_in);
		fld->set(in, _in);
		fld->inv(iin, in);
		fld->mul(out, in, iin);
		fld->get(out, _out);
		return (_out == _one);
	});
}

bool do_field_mul1_check2(const ecclib_field* fld)
{
	buffer _in(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _out2(fld->storage_octets);
	buffer in(fld->element_size);
	buffer out(fld->element_size);
	buffer one(fld->element_size);
	fld->set_int(one, 1);
	return loop_repeat([fld, &_in, &_out1, &_out2, &in, &out, &one]() -> bool {
		random_fill(_in);
		fld->set(in, _in);
		fld->mul(out, in, one);
		fld->get(in, _out1);
		fld->get(out, _out2);
		return (_out1 == _out2);
	});
}

bool do_field_sqrt_check2(const ecclib_field* fld)
{
	buffer _in(fld->storage_octets);
	buffer _out1(fld->storage_octets);
	buffer _out2(fld->storage_octets);
	buffer in(fld->element_size);
	buffer check(fld->element_size);
	buffer tmp(fld->element_size);
	unsigned count = 0;
	unsigned tcount = 0;
	return loop_repeat([fld, &count, &tcount, &_in, &_out1, &_out2, &in, &check, &tmp]() -> bool {
		tcount++;
		random_fill(_in);
		fld->set(in, _in);
		if(fld->sqrt(tmp, in) > 0) count++; else return true;
		fld->sqr(check, tmp);
		fld->get(in, _out1);
		fld->get(check, _out2);
		return _out1 == _out2;
	}) && (count > 4 * tcount / 10 && count < 6 * tcount / 10);
}

bool do_field_multiply_scan_char2(const ecclib_field* fld)
{
	buffer _tmp1(fld->storage_octets);
	buffer _tmp2(fld->storage_octets);
	buffer _tmp3(fld->storage_octets);
	buffer ref(fld->storage_octets);
	buffer tmp1(fld->element_size);
	buffer tmp2(fld->element_size);
	buffer tmp3(fld->element_size);
	for(size_t i = 0; i < 8 * fld->storage_octets; i++) {
		for(size_t j = 0; j < 8 * fld->storage_octets; j++) {
			ecclib_zeroize(_tmp1, _tmp1.size());
			ecclib_zeroize(_tmp2, _tmp2.size());
			ecclib_zeroize(ref, _tmp2.size());
			_tmp1[i>>3] |= (1UL<<(i&7));
			_tmp2[j>>3] |= (1UL<<(j&7));
			if(i < fld->degree && j < fld->degree) {
				size_t refb = (i + j) % fld->degree;
				ref[refb>>3] |= (1UL<<(refb&7));
			}
			fld->set(tmp1, _tmp1);
			fld->set(tmp2, _tmp2);
			fld->mul(tmp3, tmp1, tmp2);
			fld->get(tmp3, _tmp3);
			if(_tmp3 != ref) return false;
		}
	}
	return true;
}

void do_cond_substract(uint32_t* inout, const uint32_t* substract, size_t words)
{
	uint32_t tmp[words];
	for(size_t i = words - 1; i < words; i--) {
		if(inout[i] > substract[i]) break;
		if(inout[i] < substract[i]) return;
	}
	ecclib_bigint_sub(tmp, inout, substract, words);
	for(size_t i = 0; i < words; i++) inout[i] = tmp[i];
}

void break_fn()
{
}

void reduce_mod_modulus(uint32_t* inout, size_t words, const uint8_t* modulus, size_t bytes)
{
	uint32_t _modulus[words];
	ecclib_zeroize((uint8_t*)_modulus, sizeof(_modulus));
	ecclib_bigint_load(_modulus, words, modulus, bytes);
	//Chainshift _modulus until the highest bit is set.
	size_t positions_shifted = 0;
	while((_modulus[words - 1] >> 31) == 0) {
		uint32_t carry = 0;
		for(size_t j = 0; j < words; j++) {
			uint32_t ncarry = _modulus[j] >> 31;
			_modulus[j] = (_modulus[j] << 1) | carry;
			carry = ncarry;
		}
		positions_shifted++;
	}
	//Then chainshift back, substracting conditionally.
	for(size_t i = 0; i < positions_shifted; i++) {
		do_cond_substract(inout, _modulus, words);
		uint32_t carry = 0;
		for(size_t j = words - 1; j < words; j--) {
			uint32_t ncarry = _modulus[j] & 1;
			_modulus[j] = (_modulus[j] >> 1) | (carry << 31);
			carry = ncarry;
		}
	}
	//Do one final substract with non-shifted value.
	do_cond_substract(inout, _modulus, words);
}

void reduce_mulmod_characteristic(uint8_t* out, const uint8_t* in1, const uint8_t* in2, const uint8_t* modulus,
	size_t bytes)
{
	size_t words = (bytes + 3) / 4;
	uint32_t in1w[words];
	uint32_t in2w[words];
	uint32_t outw[2 * words];
	ecclib_bigint_load(in1w, words, in1, bytes);
	ecclib_bigint_load(in2w, words, in2, bytes);
	ecclib_bigint_mul(outw, in1w, words, in2w, words);
	reduce_mod_modulus(outw, 2 * words, modulus, bytes);
	ecclib_bigint_store(out, bytes, outw);
}

void reduce_mod_characteristic(uint8_t* out, const uint8_t* in, const uint8_t* modulus, size_t bytes)
{
	size_t words = (bytes + 3) / 4;
	uint32_t outw[words];
	ecclib_bigint_load(outw, words, in, bytes);
	reduce_mod_modulus(outw, words, modulus, bytes);
	ecclib_bigint_store(out, bytes, outw);
}

bool do_field_multiply_scan(const ecclib_field* fld)
{
	//Char2 fields are check specially.
	if(fld->field_characteristic[0] == 2)
		return do_field_multiply_scan_char2(fld);
	buffer _tmp1(fld->storage_octets);
	buffer _tmp2(fld->storage_octets);
	buffer _tmp3(fld->storage_octets);
	buffer ref(fld->storage_octets);
	buffer tmp1(fld->element_size);
	buffer tmp2(fld->element_size);
	buffer tmp3(fld->element_size);
	for(size_t i = 0; i < 8 * fld->storage_octets; i++) {
		if((i % 16) == 0) { printf("."); fflush(stdout); }
		for(size_t j = 0; j < 8 * fld->storage_octets; j++) {
			ecclib_zeroize(_tmp1, _tmp1.size());
			ecclib_zeroize(_tmp2, _tmp2.size());
			ecclib_zeroize(ref, ref.size());
			_tmp1[i>>3] |= (1UL<<(i&7));
			_tmp2[j>>3] |= (1UL<<(j&7));
			size_t ideg = i / 8 / fld->degree_stride;
			size_t jdeg = j / 8 / fld->degree_stride;
			size_t rdeg = (ideg + jdeg) % fld->degree;
			const uint8_t* ibase = &_tmp1[ideg * fld->degree_stride];
			const uint8_t* jbase = &_tmp2[jdeg * fld->degree_stride];
			uint8_t* rbase = &ref[rdeg * fld->degree_stride];
			if(i < fld->degree_bits && j < fld->degree_bits) {
				reduce_mulmod_characteristic(rbase, ibase, jbase, fld->field_characteristic,
					fld->degree_stride);
			}
			fld->set(tmp1, _tmp1);
			fld->set(tmp2, _tmp2);
			fld->mul(tmp3, tmp1, tmp2);
			fld->get(tmp3, _tmp3);
			if(_tmp3 != ref) return false;
		}
	}
	printf("*");
	fflush(stdout);
	return true;
}

bool do_field_inout_scan_char2(const ecclib_field* fld)
{
	buffer _tmp1(fld->storage_octets);
	buffer _tmp3(fld->storage_octets);
	buffer ref(fld->storage_octets);
	buffer tmp1(fld->element_size);
	for(size_t i = 0; i < 8 * fld->storage_octets; i++) {
		ecclib_zeroize(_tmp1, _tmp1.size());
		ecclib_zeroize(_tmp3, _tmp3.size());
		ecclib_zeroize(ref, ref.size());
		_tmp1[i>>3] |= (1UL<<(i&7));
		if(i < fld->degree) {
			ref[i>>3] |= (1UL<<(i&7));
		}
		fld->set(tmp1, _tmp1);
		fld->get(tmp1, _tmp3);
		if(_tmp3 != ref) return false;
	}
	return true;
}

bool do_field_inout_scan(const ecclib_field* fld)
{
	//Char2 fields are check specially.
	if(fld->field_characteristic[0] == 2)
		return do_field_inout_scan_char2(fld);
	buffer _tmp1(fld->storage_octets);
	buffer _tmp3(fld->storage_octets);
	buffer ref(fld->storage_octets);
	buffer tmp1(fld->element_size);
	for(size_t i = 0; i < 8 * fld->storage_octets; i++) {
		ecclib_zeroize(_tmp1, _tmp1.size());
		ecclib_zeroize(ref, ref.size());
		_tmp1[i>>3] |= (1UL<<(i&7));
		size_t ideg = i / 8 / fld->degree_stride;
		const uint8_t* ibase = &_tmp1[ideg * fld->degree_stride];
		uint8_t* rbase = &ref[ideg * fld->degree_stride];
		if(i < fld->degree_bits) {
			reduce_mod_characteristic(rbase, ibase, fld->field_characteristic, fld->degree_stride);
		}
		fld->set(tmp1, _tmp1);
		fld->get(tmp1, _tmp3);
		if(_tmp3 != ref) return false;
	}
	return true;
}


bool do_field_limit_check_char2(const ecclib_field* fld)
{
	//Check 1: All 1s is in bounds.
	buffer tmp(fld->storage_octets);
	for(size_t i = 0; i < fld->degree; i++)
		tmp[i>>3] |= (1UL<<(i&7));
	if(fld->check(tmp) <= 0) return false;
	//Check 1: All 0s and 1 is out of bounds.
	if(fld->degree < 8 * fld->storage_octets) {
		ecclib_zeroize(tmp, tmp.size());
		tmp[fld->degree>>3] |= (1UL<<(fld->degree&7));
		if(fld->check(tmp) > 0) return false;
	}
	return true;
}

static void write_subfield_order_full(const ecclib_field* fld, uint8_t* buf, size_t bytes)
{
	for(size_t i = 0; i < bytes; i++)
		buf[i] = fld->field_characteristic[i];
}

static void write_subfield_order_minus1(const ecclib_field* fld, uint8_t* buf, size_t bytes)
{
	uint16_t borrow = 1;
	for(size_t i = 0; i < bytes; i++) {
		uint16_t r = fld->field_characteristic[i] - borrow;
		buf[i] = r;
		borrow = r >> 15;
	}
}

static void write_subfield_order_add1_nonff(const ecclib_field* fld, uint8_t* buf, size_t bytes)
{
	size_t pos = bytes;
	for(size_t i = 0; i < bytes; i++) if(fld->field_characteristic[i] != 255) pos = i;
	for(size_t i = 0; i < bytes; i++) {
		if(i < pos) buf[i] = 0;
		if(i == pos) buf[i] = fld->field_characteristic[i] + 1;
		if(i > pos) buf[i] = fld->field_characteristic[i];
	}
}

static void write_subfield_order_sub1_non00(const ecclib_field* fld, uint8_t* buf, size_t bytes)
{
	size_t pos = bytes;
	for(size_t i = 0; i < bytes; i++) if(fld->field_characteristic[i] != 0) pos = i;
	for(size_t i = 0; i < bytes; i++) {
		if(i < pos) buf[i] = 255;
		if(i == pos) buf[i] = fld->field_characteristic[i] - 1;
		if(i > pos) buf[i] = fld->field_characteristic[i];
	}
}

bool do_field_limit_check(const ecclib_field* fld)
{
	buffer tmp(fld->storage_octets);
	//Char2 fields are check specially.
	if(fld->field_characteristic[0] == 2)
		return do_field_limit_check_char2(fld);
	//Check 1: field_characteristic[i] is out of bounds (rest at char-1).
	ecclib_zeroize(tmp, tmp.size());
	for(size_t i = 0; i < fld->degree; i++) {
		for(size_t j = 0; i < fld->degree; i++) {
			if(i == j) write_subfield_order_full(fld, tmp + j * fld->degree_stride, fld->degree_stride);
			else write_subfield_order_minus1(fld, tmp + j * fld->degree_stride, fld->degree_stride);
		}
		if(fld->check(tmp) > 0) return false;
	}
	//Check 2: Substracting 1 from field_characteristic[i] is in bounds (rest at char-1).
	ecclib_zeroize(tmp, tmp.size());
	for(size_t i = 0; i < fld->degree; i++) {
		for(size_t j = 0; i < fld->degree; i++) {
			if(i == j) write_subfield_order_minus1(fld, tmp + j * fld->degree_stride, fld->degree_stride);
			else write_subfield_order_minus1(fld, tmp + j * fld->degree_stride, fld->degree_stride);
		}
		if(fld->check(tmp) <= 0) return false;
	}
	//Check 3: Adding 1 to last non-FF octet and setting all octets before to 0 is out of bounds (rest at char-1).
	ecclib_zeroize(tmp, tmp.size());
	for(size_t i = 0; i < fld->degree; i++) {
		for(size_t j = 0; i < fld->degree; i++) {
			if(i == j) write_subfield_order_add1_nonff(fld, tmp + j * fld->degree_stride,
				fld->degree_stride);
			else write_subfield_order_minus1(fld, tmp + j * fld->degree_stride, fld->degree_stride);
		}
		if(fld->check(tmp) > 0) return false;
	}
	//Check 4: Substracting 1 from last non-0 octet and setting all octets before to FF is in bounds (rest at
	//char-1).
	ecclib_zeroize(tmp, tmp.size());
	for(size_t i = 0; i < fld->degree; i++) {
		for(size_t j = 0; i < fld->degree; i++) {
			if(i == j) write_subfield_order_sub1_non00(fld, tmp + j * fld->degree_stride,
				fld->degree_stride);
			else write_subfield_order_minus1(fld, tmp + j * fld->degree_stride, fld->degree_stride);
		}
		if(fld->check(tmp) <= 0) return false;
	}
	return true;
}

bool do_curve_coordinates_test(const ecclib_curve* crv)
{
	if(!crv) return false;
	buffer _out1(crv->storage_octets), _out2(crv->storage_octets);
	buffer x1(crv->field->element_size), x2(crv->field->element_size), one(crv->field->element_size),
		y(crv->field->element_size);
	buffer in(crv->element_size), in2(crv->element_size);
	crv->field->set_int(one, 1);
	return loop_repeat([&crv, &_out1, &_out2, &in, &in2, &x1, &x2, &one, &y]() -> bool {
		if(!generate_random_point(crv, in)) return false;
		crv->point_get_coords(in, x1, y);
		if(crv->point_set_coords(in2, x1, y) <= 0) return false;
		crv->field->add(x2, x1, one);
		if(crv->point_set_coords(in2, x2, y) > 0) return false;
		crv->point_get(in, _out1);
		crv->point_get(in2, _out2);
		return (_out1 == _out2);
	});
	return true;
}

bool do_curve_orderp1_check(const ecclib_curve* crv)
{
	if(!crv) return false;
	buffer orderp1(crv->scalar_octets);
	//Add 1 to copied value.
	uint16_t carry = 1;
	for(size_t i = 0; i < crv->scalar_octets; i++) {
		carry += crv->order[i];
		orderp1[i] = carry;
		carry >>= 8;
	}
	buffer _out1(crv->storage_octets), _out2(crv->storage_octets);
	buffer in(crv->element_size), in2(crv->element_size), inpwr(crv->element_size), tmp(crv->element_size),
		accum(crv->element_size);

	return loop_repeat([&crv, &orderp1, &_out1, &_out2, &in, &in2, &inpwr, &tmp, &accum]() -> bool {
		if(!generate_random_point(crv, in)) return false;
		//Double thrice to clear torsion
		crv->point_double(in2, in);
		crv->point_double(in, in2);
		crv->point_double(in2, in);
		crv->point_zero(accum);
		crv->point_copy(inpwr, in2);
		for(unsigned i = 0; i < 8 * orderp1.size(); i++) {
			if((orderp1[i/8] >> (i%8)) & 1) {
				crv->point_add(tmp, accum, inpwr);
				crv->point_copy(accum, tmp);
			}
			crv->point_double(tmp, inpwr);
			crv->point_copy(inpwr, tmp);
		}
		crv->point_get(in2, _out1);
		crv->point_get(accum, _out2);
		return (_out1 == _out2);
	});
}

bool do_curve_add_dbl_consistency(const ecclib_curve* crv)
{
	if(!crv) return false;
	buffer _out1(crv->storage_octets), _out2(crv->storage_octets);
	buffer in(crv->element_size), out1(crv->element_size), out2(crv->element_size);
	return loop_repeat([&crv, &_out1, &_out2, &in, &out1, &out2]() -> bool {
		if(!generate_random_point(crv, in)) return false;
		crv->point_add(out1, in, in);
		crv->point_double(out2, in);
		crv->point_get(out1, _out1);
		crv->point_get(out2, _out2);
		return (_out1 == _out2);
	});
}

bool do_curve_add_zero(const ecclib_curve* crv)
{
	if(!crv) return false;
	buffer zero(crv->element_size), in(crv->element_size), out1(crv->element_size), out2(crv->element_size);
	buffer _in2(crv->storage_octets), _out1(crv->storage_octets), _out2(crv->storage_octets);
	crv->point_zero(zero);
	return loop_repeat([&crv, &_in2, &_out1, &_out2, &zero, &in, &out1, &out2]() -> bool {
		if(!generate_random_point(crv, in)) return false;
		crv->point_add(out1, in, zero);
		crv->point_add(out2, zero, in);
		crv->point_get(out1, _out1);
		crv->point_get(out2, _out2);
		crv->point_get(in, _in2);
		return (_in2 == _out1) && (_in2 == _out2);
	});
}

bool do_curve_order_check(const ecclib_curve* crv, bool third_double)
{
	if(!crv) return false;
	buffer orderp0(crv->scalar_octets);
	for(size_t i = 0; i < crv->scalar_octets; i++) orderp0[i] = crv->order[i];
	buffer _out1(crv->storage_octets), _out2(crv->storage_octets),
		_zero(crv->storage_octets);
	buffer in(crv->element_size), in2(crv->element_size), inpwr(crv->element_size), tmp(crv->element_size),
		accum(crv->element_size), zero(crv->element_size);

	crv->point_zero(zero);
	crv->point_get(zero, _zero);
	void (*third_fn)(void* dst, const void* src) = third_double ? crv->point_double : crv->point_copy;
	return loop_repeat([&crv, &orderp0, &_out1, &_out2, &_zero, &in, &in2, &inpwr, &tmp, &accum,
		&third_fn]() -> bool {
		if(!generate_random_point(crv, in)) return false;
		//Double twice/thrice to clear torsion
		crv->point_double(in2, in);
		crv->point_double(in, in2);
		third_fn(in2, in);
		crv->point_zero(accum);
		crv->point_copy(inpwr, in2);
		for(unsigned i = 0; i < 8 * orderp0.size(); i++) {
			if((orderp0[i/8] >> (i%8)) & 1) {
				crv->point_add(tmp, accum, inpwr);
				crv->point_copy(accum, tmp);
			}
			crv->point_double(tmp, inpwr);
			crv->point_copy(inpwr, tmp);
		}
		crv->point_get(in2, _out1);
		crv->point_get(accum, _out2);
		return (_zero == _out2) && (_out1 != _out2);
	});
}

bool do_curve_base_order_check(const ecclib_curve* crv)
{
	if(!crv) return false;
	buffer orderp0(crv->scalar_octets);
	for(size_t i = 0; i < crv->scalar_octets; i++) orderp0[i] = crv->order[i];
	buffer _out1(crv->storage_octets), _out2(crv->storage_octets),
		_zero(crv->storage_octets);
	buffer in2(crv->element_size), inpwr(crv->element_size), tmp(crv->element_size), accum(crv->element_size),
		zero(crv->element_size);

	crv->point_zero(zero);
	crv->point_get(zero, _zero);
	crv->point_base(in2);
	crv->point_zero(accum);
	crv->point_copy(inpwr, in2);
	for(unsigned i = 0; i < 8 * orderp0.size(); i++) {
		if((orderp0[i/8] >> (i%8)) & 1) {
			crv->point_add(tmp, accum, inpwr);
			crv->point_copy(accum, tmp);
		}
		crv->point_double(tmp, inpwr);
		crv->point_copy(inpwr, tmp);
	}
	crv->point_get(in2, _out1);
	crv->point_get(accum, _out2);
	return (_zero == _out2) && (_out1 != _out2);
}

bool do_curve_montgomery_order_check(const ecclib_curve* crv)
{
	if(!crv) return false;
	if(!(crv->flags & ECCLIB_FLAG_HAS_MONTGOMERY)) return true;
	buffer orderp0(crv->scalar_octets);
	for(size_t i = 0; i < crv->scalar_octets; i++) orderp0[i] = crv->order[i];
	buffer _mbase(crv->field->storage_octets), _zero(crv->field->storage_octets),
		_out(crv->field->storage_octets);
	buffer mbase(crv->field->element_size), mbase2(crv->field->element_size);
	buffer32 s(crv->scalar_elems_single);

	for(size_t i = 0; i < _mbase.size(); i++) _mbase[i] = crv->montgomery_base[i];

	crv->field->set(mbase, _mbase);
	ecclib_bigint_load(s, crv->scalar_elems_single, orderp0, crv->scalar_octets);
	crv->montgomery_mul(mbase2, s, mbase);
	crv->field->get(mbase2, _out);
	return (_zero == _out);
}

bool do_curve_montgomery_consistency(const ecclib_curve* crv)
{
	if(!crv) return false;
	if(!(crv->flags & ECCLIB_FLAG_HAS_MONTGOMERY)) return true;
	buffer _mbase(crv->field->storage_octets);
	buffer mbase(crv->field->element_size);
	buffer _s1(crv->scalar_octets), _s2(crv->scalar_octets);
	buffer32 s1(crv->scalar_elems_single), s2(crv->scalar_elems_single);
	buffer g(crv->field->element_size), pa(crv->field->element_size), pb(crv->field->element_size),
		k1(crv->field->element_size), k2(crv->field->element_size);
	buffer _k1(crv->field->storage_octets), _k2(crv->field->storage_octets);

	for(size_t i = 0; i < _mbase.size(); i++) _mbase[i] = crv->montgomery_base[i];

	crv->field->set(mbase, _mbase);
	return loop_repeat([&crv, &_k1, &_k2, &_s1, &_s2, &k1, &k2, &s1, &s2, &pb, &pa, &mbase]() -> bool {
		random_fill(_s1);
		random_fill(_s2);
		ecclib_bigint_load(s1, crv->scalar_elems_single, _s1, crv->scalar_octets);
		ecclib_bigint_load(s2, crv->scalar_elems_single, _s2, crv->scalar_octets);
		crv->montgomery_mul(pa, s1, mbase);
		crv->montgomery_mul(pb, s2, mbase);
		crv->montgomery_mul(k1, s2, pa);
		crv->montgomery_mul(k2, s1, pb);
		crv->field->get(k1, _k1);
		crv->field->get(k2, _k2);
		return (_k1 == _k2);
	});
}

bool do_curve_xcurve_consistency(const ecclib_curve* crv)
{
	if(!crv) return false;
	if(!(crv->flags & ECCLIB_FLAG_HAS_MONTGOMERY)) return true;
	buffer _mbase(crv->field->storage_octets);
	buffer mbase(crv->field->element_size);
	buffer p1(crv->field->storage_octets);
	buffer p2(crv->field->storage_octets);
	buffer p1b(crv->field->storage_octets);
	buffer p2b(crv->field->storage_octets);
	buffer s1(crv->field->storage_octets);
	buffer s2(crv->field->storage_octets);
	buffer k1(crv->field->storage_octets);
	buffer k2(crv->field->storage_octets);

	for(size_t i = 0; i < mbase.size(); i++) mbase[i] = crv->montgomery_base[i];

	return loop_repeat([&crv, &p1, &p2, &p1b, &p2b, &s1, &s2, &k1, &k2, &mbase]() -> bool {
		random_fill(s1);
		random_fill(s2);
		ecclib_xcurve_base(crv, p1, s1);
		ecclib_xcurve_base(crv, p2, s2);
		ecclib_xcurve(crv, p1b, s1, mbase);
		ecclib_xcurve(crv, p2b, s2, mbase);
		ecclib_xcurve(crv, k1, s1, p2);
		ecclib_xcurve(crv, k2, s2, p1);
		return (k1 == k2 && p1 == p1b && p2 == p2b);
	});
}

bool do_curve_scalarmul_vs_power_ladder(const ecclib_curve* crv)
{
	if(!crv) return false;
	buffer accum(crv->element_size), tmp(crv->element_size), inpwr(crv->element_size), mulres(crv->element_size),
		in(crv->element_size);
	buffer _out1(crv->storage_octets), _out2(crv->storage_octets);
	buffer32 s(crv->scalar_elems_single);
	return loop_repeat([&crv, &accum, &tmp, &inpwr, &mulres, &in, &_out1, &_out2, &s]() -> bool {
		if(!generate_random_point(crv, in)) return false;
		random_fill(s);
		//Calculate power ladder.
		crv->point_zero(accum);
		crv->point_copy(inpwr, in);
		for(unsigned i = 0; i < 32 * s.size(); i++) {
			if((s[i/32] >> (i%32)) & 1) {
				crv->point_add(tmp, accum, inpwr);
				crv->point_copy(accum, tmp);
			}
			crv->point_double(tmp, inpwr);
			crv->point_copy(inpwr, tmp);
		}
		crv->point_mul(mulres, s, in);
		crv->point_get(mulres, _out1);
		crv->point_get(accum, _out2);
		return (_out1 == _out2);
	});
}

bool do_curve_sclarmul_vs_scalarmul_base(const ecclib_curve* crv)
{
	if(!crv) return false;
	buffer _out1(crv->storage_octets), _out2(crv->storage_octets);
	buffer base(crv->element_size), mulres(crv->element_size), baseres(crv->element_size);
	buffer32 s(crv->scalar_elems_single);
	crv->point_base(base);
	return loop_repeat([&crv, &_out1, &_out2, &base, &mulres, &baseres, &s]() -> bool {
		//Try encodings as far as one works.
		random_fill(s);
		//Calculate power ladder.
		crv->point_mul(baseres, s, base);
		crv->point_mul_base(mulres, s);
		crv->point_get(baseres, _out1);
		crv->point_get(mulres, _out2);
		return (_out1 == _out2);
	});
}

bool do_curve_edwards_montgomery_consistency(const ecclib_curve* crv)
{
	if(!crv) return false;
	if(!(crv->flags & ECCLIB_FLAG_HAS_MONTGOMERY)) return true;
	buffer mbase(crv->field->element_size), i1(crv->field->element_size), i2(crv->field->element_size),
		k1(crv->field->element_size), k2(crv->field->element_size), k3(crv->field->element_size);
	buffer _k1(crv->field->storage_octets), _k2(crv->field->storage_octets), _k3(crv->field->storage_octets),
		_i1(crv->field->storage_octets), _i2(crv->field->storage_octets);
	buffer tmp(crv->element_size), tmp2(crv->element_size), ebase(crv->element_size);
	buffer32 s1(crv->scalar_elems_single), s2(crv->scalar_elems_single);

	crv->point_base(ebase);
	crv->point_montgomery(mbase, ebase);
	return loop_repeat([&crv, &mbase, &i1, &i2, &k1, &k2, &k3, &_k1, &_k2, &_k3, &_i1, &_i2, &tmp, &tmp2, &s1,
		&s2]() -> bool {
		random_fill(s1);
		random_fill(s2);
		//Montgomery + Montgomery
		crv->montgomery_mul(i1, s1, mbase);
		crv->montgomery_mul(k1, s2, i1);
		//Edwards + Montgomery
		crv->point_mul_base(tmp, s1);
		crv->point_montgomery(i2, tmp);
		crv->montgomery_mul(k2, s2, i2);
		//Edwards + Edwards
		crv->point_mul(tmp2, s2, tmp);
		crv->point_montgomery(k3, tmp2);

		crv->field->get(k1, _k1);
		crv->field->get(k2, _k2);
		crv->field->get(k3, _k3);
		crv->field->get(i1, _i1);
		crv->field->get(i2, _i2);
		return (_i1 == _i2) && (_k2 == _k3) && (_k1 == _k3);
	});
}

bool do_curve_test_negation(const ecclib_curve* crv)
{
	if(!crv) return false;
	buffer zero(crv->element_size), in(crv->element_size), out(crv->element_size), tmp(crv->element_size);
	buffer _out1(crv->storage_octets), _out2(crv->storage_octets);
	crv->point_zero(zero);
	return loop_repeat([&crv, &zero, &in, &out, &tmp, &_out1, &_out2]() -> bool {
		if(!generate_random_point(crv, in)) return false;
		crv->point_neg(tmp, in);
		crv->point_add(out, in, tmp);
		crv->point_get(out, _out1);
		crv->point_get(zero, _out2);
		return (_out1 == _out2);
	});
}

bool do_curve_basepoint_isogeny(const ecclib_curve* crv)
{
	if(!crv) return false;
	if(!(crv->flags & ECCLIB_FLAG_HAS_MONTGOMERY)) return true;
	buffer mbase(crv->field->element_size), iebase(crv->field->element_size);
	buffer _iebase(crv->field->storage_octets), _mbase2(crv->field->storage_octets),
		_mbase(crv->field->storage_octets);
	buffer ebase(crv->element_size);

	for(size_t i = 0; i < _mbase.size(); i++) _mbase[i] = crv->montgomery_base[i];
	crv->field->set(mbase, _mbase);
	crv->point_base(ebase);
	crv->point_montgomery(iebase, ebase);
	crv->field->get(iebase, _iebase);
	crv->field->get(mbase, _mbase2);
	return _iebase == _mbase2;
}

bool do_curve_schnorr_reduce_test(const ecclib_curve* crv)
{
	//s = r + h * a => sB = rB + h * (a * B)
	if(!crv) return false;
	buffer32 r_raw(crv->scalar_elems_double), h_raw(crv->scalar_elems_double), a_raw(crv->scalar_elems_double),
		tmp1(crv->scalar_elems_double);
	buffer32 r(crv->scalar_elems_single), h(crv->scalar_elems_single), a(crv->scalar_elems_single),
		tmp2(crv->scalar_elems_single), tmp3(crv->scalar_elems_single), sd(crv->scalar_elems_single);
	buffer s(crv->scalar_octets);
	buffer R(crv->element_size), A(crv->element_size), S(crv->element_size), hA(crv->element_size),
		RHA(crv->element_size);
	buffer lhs(crv->storage_octets), rhs(crv->storage_octets);
	
	return loop_repeat([&crv, &r_raw, &h_raw, &a_raw, &r, &h, &a, &lhs, &rhs, &RHA, &S, &hA, &R, &A, &s, &sd,
		&tmp1, &tmp2, &tmp3]() -> bool {
		random_fill(r_raw);
		random_fill(h_raw);
		random_fill(a_raw);
		//Mask excess bits away.
		for(unsigned i = crv->partial_max_bits; i < 32 * crv->scalar_elems_double; i++) {
			uint32_t elem = i >> 5;
			uint32_t mask = ~(1UL << (i & 31));
			r_raw[elem] &= mask;
			h_raw[elem] &= mask;
			a_raw[elem] &= mask;
		}
		crv->reduce_partial(r, r_raw);
		crv->reduce_partial(h, h_raw);
		crv->reduce_partial(a, a_raw);
		crv->point_mul_base(R, r);
		crv->point_mul_base(A, a);
		ecclib_bigint_mul(tmp1, h, crv->scalar_elems_single, a, crv->scalar_elems_single);
		crv->reduce_partial(tmp2, tmp1);
		ecclib_bigint_add(tmp3, r, tmp2, crv->scalar_elems_single);
		crv->reduce_full(s, tmp3);
		ecclib_bigint_load(sd, crv->scalar_elems_single, s, crv->scalar_octets);
		crv->point_mul_base(S, sd);
		crv->point_mul(hA, h, A);
		crv->point_add(RHA, R, hA);
		crv->point_get(S, lhs);
		crv->point_get(RHA, rhs);
		return (lhs == rhs);
	});
}


bool do_cfrg_chain_test(const struct ecclib_curve* crv, buffer& init, buffer& out1, buffer& out1k, buffer& out1m)
{
	buffer u_in(crv->field->element_size), u_out(crv->field->element_size);
	buffer u_out_res(crv->field->storage_octets), _u_in(crv->field->storage_octets);
	buffer _iscalar(crv->field->storage_octets);
	buffer32 iscalar(crv->scalar_elems_single);
	for(unsigned i = 0; i < _u_in.size(); i++) _u_in[i] = _iscalar[i] = init[i];
	for(unsigned i = 1;; i++) {
		crv->field->set(u_in, _u_in);
		ecclib_bigint_load(iscalar, crv->scalar_elems_single, _iscalar, _iscalar.size());
		//Hack: Mask some octets.
		if(crv->field->storage_octets == 56) {
			iscalar[0] &= 0xFFFFFFFC;	//Mask scalar.
			iscalar[13] |= 0x80000000;	//Mask scalar.
		} else {
			iscalar[0] &= 0xFFFFFFF8;	//Mask scalar.
			iscalar[7] &= 0x7FFFFFFF;	//Mask scalar.
			iscalar[7] |= 0x40000000;	//Mask scalar.
		}
		crv->montgomery_mul(u_out, iscalar, u_in);
		crv->field->get(u_out, u_out_res);

		for(unsigned j = 0; j < _u_in.size(); j++) _u_in[j] = _iscalar[j];		//old_k -> u
		for(unsigned j = 0; j < _iscalar.size(); j++) _iscalar[j] = u_out_res[j];	//result -> k
		if(i == 1 && _iscalar != out1) return false;
		if(i == 1000 && _iscalar != out1k) return false;
		if(i % 10000 == 0) { printf("."); fflush(stdout); }
		if(i == 1000000) return _iscalar == out1m;
	}
}

bool do_rfc7748_testvectors()
{
	const struct ecclib_curve* curve25519 = ecclib_lookup_curve_by_name("curve25519");
	const struct ecclib_curve* curve448 = ecclib_lookup_curve_by_name("curve448");
	buffer alice_priv_25519 = {
		0x77, 0x07, 0x6d, 0x0a, 0x73, 0x18, 0xa5, 0x7d, 0x3c, 0x16, 0xc1, 0x72, 0x51, 0xb2, 0x66, 0x45,
		0xdf, 0x4c, 0x2f, 0x87, 0xeb, 0xc0, 0x99, 0x2a, 0xb1, 0x77, 0xfb, 0xa5, 0x1d, 0xb9, 0x2c, 0x2a
	};
	buffer alice_pub_25519 = {
		0x85, 0x20, 0xf0, 0x09, 0x89, 0x30, 0xa7, 0x54, 0x74, 0x8b, 0x7d, 0xdc, 0xb4, 0x3e, 0xf7, 0x5a,
		0x0d, 0xbf, 0x3a, 0x0d, 0x26, 0x38, 0x1a, 0xf4, 0xeb, 0xa4, 0xa9, 0x8e, 0xaa, 0x9b, 0x4e, 0x6a
	};
	buffer bob_priv_25519 = {
		0x5d, 0xab, 0x08, 0x7e, 0x62, 0x4a, 0x8a, 0x4b, 0x79, 0xe1, 0x7f, 0x8b, 0x83, 0x80, 0x0e, 0xe6,
		0x6f, 0x3b, 0xb1, 0x29, 0x26, 0x18, 0xb6, 0xfd, 0x1c, 0x2f, 0x8b, 0x27, 0xff, 0x88, 0xe0, 0xeb
	};
	buffer bob_pub_25519 = {
		0xde, 0x9e, 0xdb, 0x7d, 0x7b, 0x7d, 0xc1, 0xb4, 0xd3, 0x5b, 0x61, 0xc2, 0xec, 0xe4, 0x35, 0x37,
		0x3f, 0x83, 0x43, 0xc8, 0x5b, 0x78, 0x67, 0x4d, 0xad, 0xfc, 0x7e, 0x14, 0x6f, 0x88, 0x2b, 0x4f
	};
	buffer shared_25519 = {
		0x4a, 0x5d, 0x9d, 0x5b, 0xa4, 0xce, 0x2d, 0xe1, 0x72, 0x8e, 0x3b, 0xf4, 0x80, 0x35, 0x0f, 0x25,
		0xe0, 0x7e, 0x21, 0xc9, 0x47, 0xd1, 0x9e, 0x33, 0x76, 0xf0, 0x9b, 0x3c, 0x1e, 0x16, 0x17, 0x42
	};
	buffer alice_priv_448 = {
		0x9a, 0x8f, 0x49, 0x25, 0xd1, 0x51, 0x9f, 0x57, 0x75, 0xcf, 0x46, 0xb0, 0x4b, 0x58, 0x00, 0xd4,
		0xee, 0x9e, 0xe8, 0xba, 0xe8, 0xbc, 0x55, 0x65, 0xd4, 0x98, 0xc2, 0x8d, 0xd9, 0xc9, 0xba, 0xf5,
		0x74, 0xa9, 0x41, 0x97, 0x44, 0x89, 0x73, 0x91, 0x00, 0x63, 0x82, 0xa6, 0xf1, 0x27, 0xab, 0x1d,
		0x9a, 0xc2, 0xd8, 0xc0, 0xa5, 0x98, 0x72, 0x6b
	};
	buffer alice_pub_448 = {
		0x9b, 0x08, 0xf7, 0xcc, 0x31, 0xb7, 0xe3, 0xe6, 0x7d, 0x22, 0xd5, 0xae, 0xa1, 0x21, 0x07, 0x4a,
		0x27, 0x3b, 0xd2, 0xb8, 0x3d, 0xe0, 0x9c, 0x63, 0xfa, 0xa7, 0x3d, 0x2c, 0x22, 0xc5, 0xd9, 0xbb,
		0xc8, 0x36, 0x64, 0x72, 0x41, 0xd9, 0x53, 0xd4, 0x0c, 0x5b, 0x12, 0xda, 0x88, 0x12, 0x0d, 0x53,
		0x17, 0x7f, 0x80, 0xe5, 0x32, 0xc4, 0x1f, 0xa0
	};
	buffer bob_priv_448 = {
		0x1c, 0x30, 0x6a, 0x7a, 0xc2, 0xa0, 0xe2, 0xe0, 0x99, 0x0b, 0x29, 0x44, 0x70, 0xcb, 0xa3, 0x39,
		0xe6, 0x45, 0x37, 0x72, 0xb0, 0x75, 0x81, 0x1d, 0x8f, 0xad, 0x0d, 0x1d, 0x69, 0x27, 0xc1, 0x20,
		0xbb, 0x5e, 0xe8, 0x97, 0x2b, 0x0d, 0x3e, 0x21, 0x37, 0x4c, 0x9c, 0x92, 0x1b, 0x09, 0xd1, 0xb0,
		0x36, 0x6f, 0x10, 0xb6, 0x51, 0x73, 0x99, 0x2d
	};
	buffer bob_pub_448 = {
		0x3e, 0xb7, 0xa8, 0x29, 0xb0, 0xcd, 0x20, 0xf5, 0xbc, 0xfc, 0x0b, 0x59, 0x9b, 0x6f, 0xec, 0xcf,
		0x6d, 0xa4, 0x62, 0x71, 0x07, 0xbd, 0xb0, 0xd4, 0xf3, 0x45, 0xb4, 0x30, 0x27, 0xd8, 0xb9, 0x72,
		0xfc, 0x3e, 0x34, 0xfb, 0x42, 0x32, 0xa1, 0x3c, 0xa7, 0x06, 0xdc, 0xb5, 0x7a, 0xec, 0x3d, 0xae,
		0x07, 0xbd, 0xc1, 0xc6, 0x7b, 0xf3, 0x36, 0x09
	};
	buffer shared_448 = {
		0x07, 0xff, 0xf4, 0x18, 0x1a, 0xc6, 0xcc, 0x95, 0xec, 0x1c, 0x16, 0xa9, 0x4a, 0x0f, 0x74, 0xd1,
		0x2d, 0xa2, 0x32, 0xce, 0x40, 0xa7, 0x75, 0x52, 0x28, 0x1d, 0x28, 0x2b, 0xb6, 0x0c, 0x0b, 0x56,
		0xfd, 0x24, 0x64, 0xc3, 0x35, 0x54, 0x39, 0x36, 0x52, 0x1c, 0x24, 0x40, 0x30, 0x85, 0xd5, 0x9a,
		0x44, 0x9a, 0x50, 0x37, 0x51, 0x4a, 0x87, 0x9d
	};

	buffer pa(32), pb(32), ka(32), kb(32);
	ecclib_xcurve_base(curve25519, pa, alice_priv_25519);
	ecclib_xcurve_base(curve25519, pb, bob_priv_25519);
	ecclib_xcurve(curve25519, ka, alice_priv_25519, pb);
	ecclib_xcurve(curve25519, kb, bob_priv_25519, pa);
	if(pa != alice_pub_25519) return false;
	if(pb != bob_pub_25519) return false;
	if(ka != shared_25519) return false;
	if(kb != shared_25519) return false;

	buffer pal(56), pbl(56), kal(56), kbl(56);
	ecclib_xcurve_base(curve448, pal, alice_priv_448);
	ecclib_xcurve_base(curve448, pbl, bob_priv_448);
	ecclib_xcurve(curve448, kal, alice_priv_448, pbl);
	ecclib_xcurve(curve448, kbl, bob_priv_448, pal);
	if(pal != alice_pub_448) return false;
	if(pbl != bob_pub_448) return false;
	if(kal != shared_448) return false;
	if(kbl != shared_448) return false;

	return true;
}

struct test
{
	const char* name;
	bool (*do_fn)();
};

bool for_each_curve(std::function<bool(const struct ecclib_curve* crv)> fn)
{
	const char* const * curves = ecclib_curve_list();
	bool ok = true;
	for(size_t i = 0; curves[i]; i++) {
		const struct ecclib_curve* crv = ecclib_lookup_curve_by_name(curves[i]);
		if(!crv || !fn(crv)) {
			printf("Failed for %s...", curves[i]);
			ok = false;
		}
	}
	return ok;
}

bool for_each_field(std::function<bool(const struct ecclib_field* fld)> fn)
{
	const char* const * fields = ecclib_field_list();
	bool ok = true;
	for(size_t i = 0; fields[i]; i++) {
		const struct ecclib_field* fld = ecclib_lookup_field_by_name(fields[i]);
		if(!fld || !fn(fld)) {
			printf("Failed for %s...", fields[i]);
			ok = false;
		}
	}
	return ok;
}

static uint32_t global_threading_test = 0;

static void test_lock(void* ptr)
{
	*(uint32_t*)ptr += 1;
}

static void test_unlock(void* ptr)
{
	*(uint32_t*)ptr += 65536;
}

static struct test tests[] = {
	{"fields in/out scan", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_inout_scan(fld);
		});
	}},
	{"fields multiply scan", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_multiply_scan(fld);
		});
	}},
	{"Threading functions", []() -> bool {
		ecclib_set_thread_fns(test_lock, test_unlock, &global_threading_test);
		for_each_curve([](const struct ecclib_curve* crv) -> bool {
			buffer mulres(crv->element_size);
			buffer32 s(crv->scalar_elems_single);
			crv->point_base(mulres);
			crv->point_mul_base(mulres, s);
			return true;
		});
		return (global_threading_test >> 16) == (global_threading_test & 65535) && global_threading_test;
	}},
	{"Zeroize buffer", []() -> bool {
		uint8_t buf[65];
		for(size_t i = 0; i < sizeof(buf); i++) buf[i] = 255;
		ecclib_zeroize(buf, sizeof(buf));
		uint8_t syndrome = 0;
		for(size_t i = 0; i < sizeof(buf); i++) syndrome |= buf[i];
		return (syndrome == 0);
	}},{"Nonexistent curve", []() -> bool {
		const struct ecclib_curve* crv = ecclib_lookup_curve_by_name("DOES-NOT-EXIST");
		return !crv;
	}},{"Nonexistent field", []() -> bool {
		const struct ecclib_field* fld = ecclib_lookup_field_by_name("DOES-NOT-EXIST");
		return !fld;
	}},{"Obtain curve handles", []() -> bool {
		int count = 0;
		for_each_curve([&count](const struct ecclib_curve* crv) -> bool { count++; return true; });
		if(count < 2) return false;	//Should be at least 2.
		return true;
	}},
	{"bigint imports", []() -> bool {
		buffer in(512);
		buffer32 out1(128);
		buffer32 out2(128);
		for(unsigned i = 0; i < 128; i++) out1[i] = out2[i] = 0;
		for(unsigned i = 0; i < in.size(); i++) in[i] = 255;
		for(unsigned i = 0; i < out1.size(); i++) out1[i] = 0;
		for(unsigned i = 0; i < out2.size(); i++) out1[i] = 0;
		ecclib_bigint_load(out1, 128, in, 256);
		ecclib_bigint_load(out2, 64, in, 512);
		uint32_t syndrome = 0, syndrome2 = ~(uint32_t)0;
		for(unsigned i = 0; i < 64; i++) syndrome2 &= out2[i];
		for(unsigned i = 64; i < 128; i++) syndrome |= out2[i];
		return !syndrome && !~syndrome2 && (out1 == out2);
	}},{"1-word bigint add", []() -> bool {
		const buffer32 in1 = {123};
		const buffer32 in2 = {456};
		const buffer32 expct = {579};
		buffer32 out(1);
		ecclib_bigint_add(out, in1, in2, out.size());
		return (out == expct);
	}},{"2-word no-carry bigint add", []() -> bool {
		const buffer32 in1 = {0x6439fa8e, 0x489e7413};
		const buffer32 in2 = {0x9bc60571, 0xb7618beb};
		const buffer32 expct = {0xFFFFFFFF, 0xFFFFFFFE};
		buffer32 out(2);
		ecclib_bigint_add(out, in1, in2, out.size());
		return (out == expct);
	}},{"2-word carry bigint add", []() -> bool {
		const buffer32 in1 = {0x6439fa8e, 0x489e7413};
		const buffer32 in2 = {0x9bc60572, 0xb7618beb};
		const buffer32 expct = {0x00000000, 0xFFFFFFFF};
		buffer32 out(2);
		ecclib_bigint_add(out, in1, in2, out.size());
		return (out == expct);
	}},{"2-word carry overboard bigint add", []() -> bool {
		const buffer32 in1 = {0x6439fa8e, 0x489e7413};
		const buffer32 in2 = {0x9bc60572, 0xb7618bec};
		const buffer32 expct = {0x00000000, 0x00000000};
		buffer32 out(2);
		ecclib_bigint_add(out, in1, in2, out.size());
		return (out == expct);
	}},{"bigint add with carry and full addends", []() -> bool {
		const buffer32 in1 = {0x6439fa8e, 0xffffffff, 0};
		const buffer32 in2 = {0x9bc60572, 0xffffffff, 0};
		const buffer32 expct = {0x00000000, 0xffffffff, 1};
		buffer32 out(3);
		ecclib_bigint_add(out, in1, in2, out.size());
		return (out == expct);
	}},{"1-word bigint sub", []() -> bool {
		const buffer32 in1 = {579};
		const buffer32 in2 = {456};
		const buffer32 expct = {123};
		buffer32 out(1);
		ecclib_bigint_sub(out, in1, in2, out.size());
		return (out == expct);
	}},{"2-word no-borrow bigint sub", []() -> bool {
		const buffer32 in1 = {0xFFFFFFFF, 0xFFFFFFFE};
		const buffer32 in2 = {0x6439fa8e, 0x489e7413};
		const buffer32 expct = {0x9bc60571, 0xb7618beb};
		buffer32 out(2);
		ecclib_bigint_sub(out, in1, in2, out.size());
		return (out == expct);
	}},{"2-word borrow bigint sub", []() -> bool {
		const buffer32 in1 = {0x00000000, 0xFFFFFFFF};
		const buffer32 in2 = {0x6439fa8e, 0x489e7413};
		const buffer32 expct = {0x9bc60572, 0xb7618beb};
		buffer32 out(2);
		ecclib_bigint_sub(out, in1, in2, out.size());
		return (out == expct);
	}},{"2-word borrow overboard bigint sub", []() -> bool {
		const buffer32 in1 = {0x00000000, 0x00000000};
		const buffer32 in2 = {0x6439fa8e, 0x489e7413};
		const buffer32 expct = {0x9bc60572, 0xb7618bec};
		buffer32 out(2);
		ecclib_bigint_sub(out, in1, in2, out.size());
		return (out == expct);
	}},{"bigint sub with borrow and full terms", []() -> bool {
		const buffer32 in1 = {0x00000000, 0xffffffff, 1};
		const buffer32 in2 = {0x6439fa8e, 0xffffffff, 0};
		const buffer32 expct = {0x9bc60572, 0xffffffff, 0};
		buffer32 out(3);
		ecclib_bigint_sub(out, in1, in2, out.size());
		return (out == expct);
	}},{"bigint add commutativity", []() -> bool {
		buffer32 in1(16);
		buffer32 in2(16);
		buffer32 out1(16);
		buffer32 out2(16);
		return loop_repeat([&in1, &in2, &out1, &out2]() -> bool  {
			random_fill(in1);
			random_fill(in2);
			ecclib_bigint_add(out1, in1, in2, out1.size());
			ecclib_bigint_add(out2, in2, in1, out2.size());
			return (out1 == out2);
		});
	}},{"bigint add-sub consistency", []() -> bool {
		buffer32 in1(16);
		buffer32 in2(16);
		buffer32 tmp(in1.size());
		buffer32 out(in1.size());
		return loop_repeat([&in1, &in2, &tmp, &out]() -> bool {
			random_fill(in1);
			random_fill(in2);
			ecclib_bigint_add(tmp, in1, in2, tmp.size());
			ecclib_bigint_sub(out, tmp, in2, out.size());
			return (out == in1);
		});
	}},{"bigint mul 64k", []() -> bool {
		const buffer32 in1 = {65536, 0};
		const buffer32 in2 = {65536};
		const buffer32 expct = {0, 1, 0};
		buffer32 out(3);
		ecclib_bigint_mul(out, in1, in1.size(), in2, in2.size());
		if(out != expct) return false;
		ecclib_bigint_mul(out, in2, in2.size(), in1, in1.size());
		if(out != expct) return false;
		return true;
	}},{"bigint mul moduli test", []() -> bool {
		buffer32 primes(200);
		buffer32 in1(16);
		buffer32 in2(16);
		buffer32 out(in1.size() + in2.size());
		uint32_t nprime = 3;
		primes[0] = 2;
		for(unsigned i = 1; i < primes.size(); i++) {
next:
			for(unsigned j = 0; j < i; j++)
				if(nprime % primes[j] == 0) {
					nprime += 2;
					goto next;
				}
			primes[i] = nprime;
			nprime += 2;
		}
		return loop_repeat([&in1, &in2, &out, &primes]() -> bool {
			random_fill(in1);
			random_fill(in2);
			ecclib_bigint_mul(out, in1, in1.size(), in2, in2.size());
			for(unsigned j = 0; j < primes.size(); j++) {
				uint32_t prime = primes[j];
				uint32_t prime_m = (0xFFFFFFFF % primes[j]) + 1;
				uint32_t multiplier = 1;
				uint32_t mod_in1 = 0, mod_in2 = 0, mod_out = 0;
				for(unsigned k = 0; k < out.size(); k++) {
					if(k < in1.size()) mod_in1 = (mod_in1 + multiplier * (in1[k] % prime)) %
						prime;
					if(k < in2.size()) mod_in2 = (mod_in2 + multiplier * (in2[k] % prime)) %
						prime;
					mod_out = (mod_out + multiplier * (out[k] % prime)) % prime;
					multiplier = (multiplier * prime_m) % prime;
				}
				if((mod_in1 * mod_in2) % prime != mod_out % prime) return false;
			}
			return true;
		});
	}},{"bigint mul at max inputs", []() -> bool {
		const buffer32 in = {
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
		};
		const buffer32 expct = {
			0x00000001, 0x00000000, 0x00000000, 0x00000000,
			0x00000000, 0x00000000, 0x00000000, 0x00000000,
			0x00000000, 0x00000000, 0x00000000, 0x00000000,
			0x00000000, 0x00000000, 0x00000000, 0x00000000,
			0xfffffffe, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
		};
		buffer32 out(2 * in.size());
		ecclib_bigint_mul(out, in, in.size(), in, in.size());
		return (out == expct);
	}},{"bigint mul commutativity in smul special", []() -> bool {
		buffer32 in1(16);
		buffer32 in2(1);
		buffer32 out1(in1.size() + in2.size());
		buffer32 out2(in1.size() + in2.size());
		return loop_repeat([&in1, &in2, &out1, &out2]() -> bool {
			random_fill(in1);
			random_fill(in2);
			ecclib_bigint_mul(out1, in1, in1.size(), in2, in2.size());
			ecclib_bigint_mul(out2, in2, in2.size(), in1, in1.size());
			return (out1 == out2);
		});
	}},{"bigint mul at max inputs in smul special", []() -> bool {
		const buffer32 in1 = {
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
		};
		const buffer32 in2 = {0xffffffff};
		const buffer32 expct = {
			0x00000001, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
			0xfffffffe
		};
		buffer32 out(in1.size() + in2.size());
		ecclib_bigint_mul(out, in1, in1.size(), in2, in2.size());
		return (out == expct);
	}},{"bigint mul commutativity", []() -> bool {
		buffer32 in1(16);
		buffer32 in2(15);
		buffer32 out1(in1.size() + in2.size());
		buffer32 out2(in1.size() + in2.size());
		return loop_repeat([&in1, &in2, &out1, &out2]() -> bool {
			random_fill(in1);
			random_fill(in2);
			ecclib_bigint_mul(out1, in1, in1.size(), in2, in2.size());
			ecclib_bigint_mul(out2, in2, in2.size(), in1, in1.size());
			return (out1 == out2);
		});
	}},
	{"fields add commutativity", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_commutativity_check2(fld, fld->add);
		});
	}},{"fields add associativity", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_associvity_check2(fld, fld->add);
		});
	}},{"fields add/sub consistency", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_addsub_check2(fld);
		});
	}},{"fields mul commutativity", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_commutativity_check2(fld, fld->mul);
		});
	}},{"fields mul associativity", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_associvity_check2(fld, fld->mul);
		});
	}},{"fields mul/sqr consistency", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_mulsqr_check2(fld);
		});
	}},{"fields mul/inv consistency", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_mulinv_check2(fld);
		});
	}},{"fields inverse", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_inverse_check2(fld);
		});
	}},{"fields multiply by one", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_mul1_check2(fld);
		});
	}},{"fields sqrt", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_sqrt_check2(fld);
		});
	}},{"fields conditional moves", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_move_checks(fld);
		});
	}},{"fields add/neg consistency", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_addneg_consistency_check(fld);
		});
	}},{"fields mul/smul consistency", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_mulsmul_consistency_check(fld);
		});
	}},{"fields limit check", []() -> bool {
		return for_each_field([](const struct ecclib_field* fld) -> bool {
			return do_field_limit_check(fld);
		});
	}},
	//TODO: More tests.
	//TODO: Multiply bit scan.
	//TODO: Input/Output bit scan.f
	{"Curves add/dbl consistency", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_add_dbl_consistency(crv);
		});
	}},{"Curves add zero", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_add_zero(crv);
		});
	}},{"Curves order+1 power", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_orderp1_check(crv);
		});
	}},{"Curves order check", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_order_check(crv, true);
		});
	}},{"Curves base order check", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_base_order_check(crv);
		});
	}},{"Curves montgomery order check", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_montgomery_order_check(crv);
		});
	}},{"Curves montgomery consistency", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_montgomery_consistency(crv);
		});
	}},{"Curves scalarmul vs power ladder", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_scalarmul_vs_power_ladder(crv);
		});
	}},{"Curves scalarmul vs scalarmul base", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_sclarmul_vs_scalarmul_base(crv);
		});
	}},{"Curves Edwards-Montgomery consistency", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_edwards_montgomery_consistency(crv);
		});
	}},{"Curves xcurve consistency", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_xcurve_consistency(crv);
		});
	}},{"Curves RFC7748 test vectors", []() -> bool {
		return do_rfc7748_testvectors();
	}},{"Curves Negation", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_test_negation(crv);
		});
	}},{"Curves get/set coordinates", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_coordinates_test(crv);
		});
	}},{"Curves basepoint isogeny", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_basepoint_isogeny(crv);
		});
	}},{"Curves schnorr reduce test", []() -> bool {
		return for_each_curve([](const struct ecclib_curve* crv) -> bool {
			return do_curve_schnorr_reduce_test(crv);
		});
	}},
	//TODO: More curves tests.
	{"CFRG-Curves X25519 chained test vector", []() -> bool {
		buffer init = {
			0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		};
		buffer out1 = {
			0x42, 0x2c, 0x8e, 0x7a, 0x62, 0x27, 0xd7, 0xbc,
			0xa1, 0x35, 0x0b, 0x3e, 0x2b, 0xb7, 0x27, 0x9f,
			0x78, 0x97, 0xb8, 0x7b, 0xb6, 0x85, 0x4b, 0x78,
			0x3c, 0x60, 0xe8, 0x03, 0x11, 0xae, 0x30, 0x79,
		};
		buffer out1k = {
			0x68, 0x4c, 0xf5, 0x9b, 0xa8, 0x33, 0x09, 0x55,
			0x28, 0x00, 0xef, 0x56, 0x6f, 0x2f, 0x4d, 0x3c,
			0x1c, 0x38, 0x87, 0xc4, 0x93, 0x60, 0xe3, 0x87,
			0x5f, 0x2e, 0xb9, 0x4d, 0x99, 0x53, 0x2c, 0x51,
		};
		buffer out1m = {
			0x7c, 0x39, 0x11, 0xe0, 0xab, 0x25, 0x86, 0xfd,
			0x86, 0x44, 0x97, 0x29, 0x7e, 0x57, 0x5e, 0x6f,
			0x3b, 0xc6, 0x01, 0xc0, 0x88, 0x3c, 0x30, 0xdf,
			0x5f, 0x4d, 0xd2, 0xd2, 0x4f, 0x66, 0x54, 0x24,
		};
		const struct ecclib_curve* crv = ecclib_lookup_curve_by_name("curve25519");
		return do_cfrg_chain_test(crv, init, out1, out1k, out1m);
	}},
	{"CFRG-Curves X448 chained test vector", []() -> bool {
		buffer init = {
			0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		};
		buffer out1 = {
			0x3f, 0x48, 0x2c, 0x8a, 0x9f, 0x19, 0xb0, 0x1e,
			0x6c, 0x46, 0xee, 0x97, 0x11, 0xd9, 0xdc, 0x14,
			0xfd, 0x4b, 0xf6, 0x7a, 0xf3, 0x07, 0x65, 0xc2,
			0xae, 0x2b, 0x84, 0x6a, 0x4d, 0x23, 0xa8, 0xcd,
			0x0d, 0xb8, 0x97, 0x08, 0x62, 0x39, 0x49, 0x2c,
			0xaf, 0x35, 0x0b, 0x51, 0xf8, 0x33, 0x86, 0x8b,
			0x9b, 0xc2, 0xb3, 0xbc, 0xa9, 0xcf, 0x41, 0x13,
		};
		buffer out1k = {
			0xaa, 0x3b, 0x47, 0x49, 0xd5, 0x5b, 0x9d, 0xaf,
			0x1e, 0x5b, 0x00, 0x28, 0x88, 0x26, 0xc4, 0x67,
			0x27, 0x4c, 0xe3, 0xeb, 0xbd, 0xd5, 0xc1, 0x7b,
			0x97, 0x5e, 0x09, 0xd4, 0xaf, 0x6c, 0x67, 0xcf,
			0x10, 0xd0, 0x87, 0x20, 0x2d, 0xb8, 0x82, 0x86,
			0xe2, 0xb7, 0x9f, 0xce, 0xea, 0x3e, 0xc3, 0x53,
			0xef, 0x54, 0xfa, 0xa2, 0x6e, 0x21, 0x9f, 0x38,
		};
		buffer out1m = {
			0x07, 0x7f, 0x45, 0x36, 0x81, 0xca, 0xca, 0x36,
			0x93, 0x19, 0x84, 0x20, 0xbb, 0xe5, 0x15, 0xca,
			0xe0, 0x00, 0x24, 0x72, 0x51, 0x9b, 0x3e, 0x67,
			0x66, 0x1a, 0x7e, 0x89, 0xca, 0xb9, 0x46, 0x95,
			0xc8, 0xf4, 0xbc, 0xd6, 0x6e, 0x61, 0xb9, 0xb9,
			0xc9, 0x46, 0xda, 0x8d, 0x52, 0x4d, 0xe3, 0xd6,
			0x9b, 0xd9, 0xd9, 0xd6, 0x6b, 0x99, 0x7e, 0x37,
		};
		const struct ecclib_curve* crv = ecclib_lookup_curve_by_name("curve448");
		return do_cfrg_chain_test(crv, init, out1, out1k, out1m);
	}},
	{NULL, NULL}
};

void do_one_test(struct test* _test, int& failed, int& passed)
{
	printf("[%p] %s...", _test->do_fn, _test->name);
	fflush(stdout);
	auto ts1 = timestamp();
	int ret = _test->do_fn();
	auto ts2 = timestamp();
	auto dts = ts2 - ts1;
	if(ret <= 0) {
		printf("\e[31mFAILED\e[0m (in %u.%06us)\n", (unsigned)(dts / 1000000),
			(unsigned)(dts % 1000000));
		failed++;
	} else {
		printf("\e[32mPASS\e[0m (in %u.%06us)\n", (unsigned)(dts / 1000000),
			(unsigned)(dts % 1000000));
		passed++;
	}
}

int do_tests(struct test* testlist)
{
	std::set<size_t> addrs;
	int failed = 0, passed = 0;
	for(unsigned i = 0; testlist[i].do_fn; i++) {
		size_t addr = (size_t)testlist[i].do_fn;
		if(addrs.count(addr)) {
			fprintf(stderr, "ERROR: Duplicate routine (%s)!\n", testlist[i].name);
			abort();
		}
		do_one_test(testlist + i, failed, passed);
		addrs.insert(addr);
	}
	if(passed && failed)
		printf("%i tests \e[32mpassed\e[0m, %i tests \e[31mfailed\e[0m.\n", passed, failed);
	else if(failed)
		printf("%i tests \e[31mfailed\e[0m.\n", failed);
	else
		printf("%i tests \e[32mpassed\e[0m.\n", passed);
	return failed;
}

int main()
{
	return (do_tests(tests) > 0);
}

#endif
